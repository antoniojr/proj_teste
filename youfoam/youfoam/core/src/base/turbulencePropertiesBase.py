"""
Documentation for turbulence properties base class
To call help, turbulencePropertiesBase --help

Set info flag -i/--info for print output. You can check for error reasons

Return a value that can be:
         1: OK -> turbulenceProperties created

         0: NOK -> turbulenceProperties create error!!

To call from command line:
python3 turbulencePropertiesBase.py -w workdir -c casename -st RAS -i "creation success"
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class TurbulencePropertiesBase(CommonPropertiesBase):

    tu_file_name = 'turbulenceProperties'
    tu_file_location = 'constant'
    tu_file_class = 'dictionary'

    simulationTypeAttrs = {'RASModel': 'kEpsilon',
                           'turbulence': 'on',
                           'printCoeffs': 'on',
                           'delta': 'cubeRootVol'}

    simulationTypeAttrs2 = {'RASModel': 'kEpsilon',
                            'turbulence': 'on',
                            'printCoeffs': 'on'}                           

    tuattrs = {'simulationType': 'RAS',
               'simulationTypeAttrs': simulationTypeAttrs}

    tuattrs2 = {'simulationType': 'RAS',
                'simulationTypeAttrs': simulationTypeAttrs2}               

    def __init__(self, st, sta, wd, c, info):
        if st:
            if st.strip().upper() == 'RAS':
                self.tuattrs2['simulationType'] = st
                self.tuattrs2['simulationTypeAttrs'] = \
                    self.update_simulation_type_attrs_by_model(st)             
            else:
                self.tuattrs['simulationType'] = st
                self.tuattrs['simulationTypeAttrs'] = \
                    self.update_simulation_type_attrs_by_model(st)

        if st.strip().upper() == 'RAS':
            self.tuattrs2['simulationTypeAttrs'] = sta if sta else self.tuattrs2['simulationTypeAttrs']
        else:
            self.tuattrs['simulationTypeAttrs'] = sta if sta else self.tuattrs['simulationTypeAttrs']

        self.work_dir = wd
        self.case_name = c
        self.simtype = st
        self.info = True if info else False

    def write_turbulance_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.tu_file_location + '/' + self.tu_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.tu_file_class, self.tu_file_location, self.tu_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            if self.simtype.strip().upper() == 'RAS':
                for k, v in self.tuattrs2.items():
                    if type(v) == dict:
                        fo.write(self.tuattrs2['simulationType'] + '\n')
                        fo.write('{' + '\n')
                        self.write_dict(fo, v, '    ')
                        fo.write('}' + '\n\n')
                    elif v.find('table') is not -1:
                        fo.write('' + str(k) + ' ' + 'table\n')
                        fo.write(self.write_table(v[6:], ''))
                    else:
                        fo.write(str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                for k, v in self.tuattrs.items():
                    if type(v) == dict:
                        fo.write(self.tuattrs['simulationType'] + '\n')
                        fo.write('{' + '\n')
                        self.write_dict(fo, v, '    ')
                        fo.write('}' + '\n\n')
                    elif v.find('table') is not -1:
                        fo.write('' + str(k) + ' ' + 'table\n')
                        fo.write(self.write_table(v[6:], ''))
                    else:
                        fo.write(str(k) + ' ' + str(v) + ';' + '\n\n')                                
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}
        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def get_dict_attrs(self, simulation_type):
        if simulation_type.strip().upper() == 'RAS':
            return self.tuattrs2
        else:
            return self.tuattrs        

    def get_simulation_type(self, simulation_type):
        if simulation_type.strip().upper() == 'RAS':
            return self.tuattrs2['simulationType']
        else:
            return self.tuattrs['simulationType']

    def updated_dict_attrs(self, new_attrs, simulation_type):
        if simulation_type.strip().upper() == 'RAS':
            self.tuattrs2 = new_attrs
        else:
            self.tuattrs = new_attrs

    def get_simulation_type_attrs(self, simulation_type):
        if simulation_type.strip().upper() == 'RAS':
            return self.tuattrs2['simulationTypeAttrs']
        else:
            return self.tuattrs['simulationTypeAttrs']

    def get_model_name(self, simulation_type):
        if simulation_type.strip().upper() == 'RAS':
            sim_type = self.tuattrs2['simulationType']
        else:
            sim_type = self.tuattrs['simulationType']
            
        if sim_type == 'laminar':
            return None

        if simulation_type.strip().upper() == 'RAS':
            for k, v in self.tuattrs2['simulationTypeAttrs'].items():
                if k.startswith(sim_type):
                    return self.tuattrs2['simulationTypeAttrs'][k]
        else:
            for k, v in self.tuattrs['simulationTypeAttrs'].items():
                if k.startswith(sim_type):
                    return self.tuattrs['simulationTypeAttrs'][k]            

    def update_simulation_type_dict_attrs(self, new_attrs, simulation_type):
        if simulation_type.strip().upper() == 'RAS':
            self.tuattrs2['simulationTypeAttrs'] = new_attrs
        else:
            self.tuattrs['simulationTypeAttrs'] = new_attrs

    def update_simulation_type_attrs_by_model(self, model_name):
        new_simulation_type_attrs = {}
        if model_name.strip().upper() == 'RAS':
            old_simulation_type_attrs = self.tuattrs2['simulationTypeAttrs']
        else:
            old_simulation_type_attrs = self.tuattrs['simulationTypeAttrs']
        old_model_key = next(iter(old_simulation_type_attrs))

        new_simulation_type_attrs[model_name + 'Model'] = \
            old_simulation_type_attrs[old_model_key]
        del old_simulation_type_attrs[old_model_key]
        for k, v in old_simulation_type_attrs.items():
            new_simulation_type_attrs[k] = v

        return new_simulation_type_attrs

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str:
                if v.find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table


"""Main function to call turbulence_dict_base method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='turbulence_dict_base help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-st", "--simulationType", dest="simulationType", help="simulationType")
    parser.add_argument("-sta", "--simulationTypeAttrs", dest="simulationTypeAttrs", help="simulationTypeAttrs")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    turbulenceProperties = TurbulencePropertiesBase(args.simulationType, args.simulationTypeAttrs,
                                                    args.workdir, args.case, args.info)

    turbulenceProperties.write_turbulance_dict_file()
