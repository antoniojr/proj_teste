from unittest import TestCase
from applications.utilities.cloneCase import clone_case
import shutil
import os


class TestClone_case(TestCase):
    WORKDIR = "workdir"
    CASENAME1 = "casename"
    CASENAME2 = "teste"
    ALLFOLDERS = 0
    FIRSTTIME = 1
    INFO = True

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    SOURCECASE1 = 'solvername'
    SOURCECASE2 = 'pitzDaily'

    SOURCECASE1 = templates_path + '/' + SOURCECASE1
    SOURCECASE2 = templates_path + '/' + SOURCECASE2

    case_path = WORKDIR + '/' + CASENAME2
    if os.path.exists(case_path):
        shutil.rmtree(case_path, ignore_errors=True)

    def test_clone_case_nok(self):
        print('***************** test_clone_case_with Wrong source_case_folder ************************\n')
        r = clone_case(self.SOURCECASE1, self.WORKDIR, self.CASENAME1, self.ALLFOLDERS, self.FIRSTTIME, self.INFO)
        self.assertEqual(r['type'], 0)
        print('**************************************************************')

    def test_clone_case_ok(self):
        print('***************** test_clone_case_OK ************************\n')
        r = clone_case(self.SOURCECASE2, self.WORKDIR, self.CASENAME2, self.ALLFOLDERS, self.FIRSTTIME, self.INFO)
        self.assertEqual(r['type'], 1)

        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************')
