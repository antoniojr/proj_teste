from django.apps import AppConfig


class FvschemesConfig(AppConfig):
    name = 'fvschemes'
