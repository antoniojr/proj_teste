from unittest import TestCase
from applications.utilities.createCase import create_case
import shutil
import os


class TestCreate_case(TestCase):

    WORKDIR = "workdir"
    CASENAME = "casename"
    INFO = False

    case_path = WORKDIR + '/' + CASENAME
    if os.path.exists(case_path):
        shutil.rmtree(case_path, ignore_errors=True)

    def test_create_case(self):
        print('********************test_create_case OK************************************\n')
        r = create_case(self.WORKDIR, self.CASENAME, self.INFO)
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.case_path, ignore_errors=True)
        print('*******************************************************************************')
