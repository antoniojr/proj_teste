from django.urls import path
from . import views

urlpatterns = [
    path('controldict/create/', views.ControlDictCreateView.as_view()),
    path('controldict/delete/', views.ControlDictDeleteView.as_view()),
    path('controldict/index/<workdir>/<casename>/', views.ControlDict.index)
]
