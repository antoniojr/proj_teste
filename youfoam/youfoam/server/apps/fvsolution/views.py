import sys
import os

from django.http import HttpResponse, HttpRequest

sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from fvSolutionBase import FvSolutionBase
import shutil
import copy
import json

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render, redirect


class FvSolution():
	def index(request, workdir, casename, simulation_type, simulation_type_model):

		fvsolution = FvSolutionBase(None, None, None, workdir, casename, simulation_type, simulation_type_model, True)

		fvsolution_data = fvsolution.get_dict_attrs(simulation_type, simulation_type_model)

		tp_data = {}
		tp_data['workdir'] = workdir
		tp_data['casename'] = casename
		tp_data['simulationType'] = simulation_type
		tp_data['simulationTypeModel'] = simulation_type_model

		return render(request, 'fvsolution.html', {'fvsolution_data': fvsolution_data, 'tp_data': tp_data})

class FvSolutionCreateView(views.APIView):
	
	def get(self, request):

		"""
		get:
		Return a list of all fvSolution attributes.
		"""

		pAttrs = {'solver': 'GAMG',
              		  'tolerance': '1e-06',
              		  'reltol': '0.1',
              		  'smoother': 'GaussSeidel'}

		UAttrs = {'solver': 'smoothSolver',
              		  'smoother': 'symGaussSeidel',
              		  'tolerance': '1e-05',
              		  'relTol': '0.1'}

		kAttrs = {'solver': 'smoothSolver',
              		  'smoother': 'symGaussSeidel',
              		  'tolerance': '1e-05',
              		  'relTol': '0.1'}

		epsilonAttrs = {'solver': 'smoothSolver',
                    	 	'smoother': 'symGaussSeidel',
                   	 	'tolerance': '1e-05',
                    		'relTol': '0.1'}

		omegaAttrs = {'solver': 'smoothSolver',
                  	      'smoother': 'symGaussSeidel',
                  	      'tolerance': '1e-05',
                              'relTol': '0.1'}

		fAttrs = {'solver': 'smoothSolver',
              		  'smoother': 'symGaussSeidel',
              		  'tolerance': '1e-05',
              		  'relTol': '0.1'}

		v2Attrs = {'solver': 'smoothSolver',
               		   'smoother': 'symGaussSeidel',
               		   'tolerance': '1e-05',
               		   'relTol': '0.1'}

		solversAttrs = {'p': pAttrs,
                   		'U': UAttrs,
                  		'k': kAttrs,
                  		'epsilon': epsilonAttrs,
                  		'omega': omegaAttrs,
                 		'f': fAttrs,
                 		'v2': v2Attrs}

		residualControlAttrs = {'p': '1e-2',
                	                'U': '1e-3',
                            		'k': '1e-3',
                            		'epsilon': '1e-3',
                            		'omega': '1e-3',
                            		'f': '1e-3',
                            		'v2': '1e-3'}

		simpleAttrs = {'nNonOrthogonalCorrectors': '0',
                   	       'consistent': 'yes',
		               'residualControl': residualControlAttrs}

		equationsAttrs = {'U': '0.9',
                	          'k': '0.9',
                                  'epsilon': '0.9',
                                  'omega': '0.9',
                                  'f': '0.9',
                                  'v2': '0.9'}

		fieldsAttrs = {'p': '0.9'}

		relaxFAttrs = {'equations': equationsAttrs,
                   	       'fields': fieldsAttrs}

		fvsolution_attrs = {'solvers': solversAttrs,
                 	     'SIMPLE': simpleAttrs,
                             'relaxationFactors': relaxFAttrs, 
			     'workdir': 'workdir', 
			     'casename': 'pitzDaily', 
			     'show_output': 'true'}


		return Response(fvsolution_attrs)


	def post(self, request):

		"""
		post:
		Create fvsolution file in workdir/casename/system folder
		"""		

		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		simulation_type = request.data.get('simulationType')
		simulation_type_model = request.data.get('simulationTypeModel')
		
		solversForm = {}
		simpleForm = {}
		relaxFForm = {}

		solversAttrs = {}
		simpleAttrs = {}
		relaxFAttrs = {}

		for k,v in request.data.items():
			if 'SOLVERS' in k:
				varkey = k.replace('SOLVERS', '')
				solversForm[varkey] = v
			elif 'SIMPLE' in k:
				varkey = k.replace('SIMPLE', '')
				simpleForm[varkey] = v
			elif 'RELAXATIONFACTORS' in k:
				varkey = k.replace('RELAXATIONFACTORS', '')
				relaxFForm[varkey] = v

		solverlist = []
		for k,v in solversForm.items():
			if 'solver' in k:
				varkey = k.replace('solver', '')
				solverlist.append(varkey)

		for k in solverlist:
			attrs = {}
			attrs['solver'] = solversForm.get(k+'solver')
			attrs['tolerance'] = solversForm.get(k+'tolerance')
			attrs['relTol'] = solversForm.get(k+'relTol')
			attrs['smoother'] = solversForm.get(k+'smoother')

			solversAttrs[k] = attrs
			print(f'Solvers: {solversAttrs[k]}')

		simpleAttrs['nNonOrthogonalCorrectors'] = simpleForm['nNonOrthogonalCorrectors']
		simpleAttrs['consistent'] = simpleForm['consistent']
		residualControlAttrs = {}
		for k,v in simpleForm.items():
			if 'residualControl' in k:
				varkey = k.replace('residualControl', '')
				residualControlAttrs[varkey] = v
		simpleAttrs['residualControl'] = residualControlAttrs

		simpleAttrs['pRefCell'] = '0'
		simpleAttrs['pRefValue'] = '0'

		equationsAttrs = {}
		fieldsAttrs = {}
		for k,v in relaxFForm.items():
			if 'equations' in k:
				varkey = k.replace('equations', '')
				equationsAttrs[varkey] = v
			else:
				varkey = k.replace('fields', '')
				fieldsAttrs[varkey] = v

		relaxFAttrs['equations'] = equationsAttrs
		relaxFAttrs['fields'] = fieldsAttrs

		fvsolution = FvSolutionBase(solversAttrs, simpleAttrs, relaxFAttrs, workdir, casename, simulation_type, simulation_type_model, True)
		r = fvsolution.write_fvsolution_dict_file()
		
		#return Response({"Write fvsolution File Status": r['desc']}, status=status.HTTP_200_OK)
		#fvsolution_data = fvsolution.get_dict_attrs()
		#return render(request, 'fvsolution.html', {'fvsolution_data': fvsolution_data, 'workdir': workdir, 'casename': casename})
		return redirect("/api/controldict/index/{}/{}/".format(workdir, casename))


class FvSolutionDeleteView(views.APIView):

	def get(self, request):

		""" 
		get:
		Return a json example to erase case folder.
		"""

		attrs = {
			'workdir': 'workdir', 
                        'casename': 'pitzDaily'}

		return Response(attrs)

	def post(self, request):

		"""
		post:
		Delete folder workdir/casename folder
		"""

		if not request.data:
			return Response({"Delete case Status": 'NOT Deleted', 'desc': 'empty request body'}, status=status.HTTP_200_OK)

		templates_path = os.environ['YouFOAM_TEMPLATES_PATH']
		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		case_dir = workdir + '/' + casename

		if not os.path.exists(workdir):
			return Response({"Delete case Status": 'NOT Deleted', 'desc': 'Work Directory does not exist!'}, status=status.HTTP_200_OK)
		
		shutil.rmtree(case_dir)
		return Response({"Delete case Status": 'Deleted'}, status=status.HTTP_200_OK)

		
