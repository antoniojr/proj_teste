from django.urls import path
from . import views

urlpatterns = [
    path('simplefoam/create/', views.SimpleFoamCreateView.as_view()),
    path('simplefoam/delete/', views.SimpleFoamDeleteView.as_view()),
    path('simplefoam/execute/', views.SimpleFoamExecuteSolverView.as_view()),
]
