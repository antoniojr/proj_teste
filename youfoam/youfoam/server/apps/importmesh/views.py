import sys
import os
sys.path.append(os.environ['YouFOAM_APPLICATIONS_PATH'] + '/utilities')
from importMesh import import_mesh
import shutil

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status

from django.shortcuts import render, redirect

class ImportMesh():

	def index(request, workdir, casename):
		file = 'rotatingCavity.cas'
		#templates_path = os.environ['YouFOAM_TEMPLATES_PATH']
		#file = templates_path + '/mesh/' + file
		return render(request, 'importmesh.html', {'workdir': workdir, 'casename': casename, 'meshfile': file})


class ImportMeshView(views.APIView):

	def get(self, request):

		""" 
		get:
		Return a json example with all importmesh parameters.
		"""

		file = 'rotatingCavity.cas'
		templates_path = os.environ['YouFOAM_TEMPLATES_PATH']
		file = templates_path + '/mesh/' + file
		import_mesh_attrs = {'type': 1, 'meshfile': file, 'workdir': 'workdir', 'casename': 'pitzDaily'}

		return Response(import_mesh_attrs)

	def post(self, request):

		"""
		post:
		Converte mesh file using: 1 -> fluent3DMeshToFoam / 0 - > fluentMeshToFoam
		"""

		#file = 'rotatingCavity.cas'
		#workdir = 'workdir'
		#casename = 'pitzDaily'
		print(request.data)
		file = request.data.get('meshfile')
		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		templates_path = os.environ['YouFOAM_TEMPLATES_PATH']
		file_path_orig = templates_path + '/mesh/' + file
		source_dir = templates_path + '/' + 'pitzDaily'
		dest_dir = workdir + '/' + casename
		conv_type = 1
		file_path_dest = dest_dir + '/' + file

		if 'convertmesh' in request.POST:
			#shutil.copytree(source_dir, dest_dir)
			shutil.copyfile(source_dir + '/system/controlDict', dest_dir + '/system/controlDict')
			shutil.copyfile(file_path_orig, file_path_dest)
			r = import_mesh(conv_type, file_path_dest, workdir, casename, True)

			#return Response({"Convert Mesh Status": r['desc']}, status=status.HTTP_200_OK)
			#return render(request, 'importmesh.html', {'workdir': workdir, 'casename': casename, 'meshfile': file, 'status': r['desc']})
			return redirect("/api/boundary/index/{}/{}/".format(workdir, casename))
		else:
			try:
				shutil.rmtree(dest_dir)
				return render(request, 'importmesh.html', {'workdir': workdir, 'casename': casename, 'meshfile': file, 'status': 'Case Directory Deleted'})
			except shutil.Error as err:
				return render(request, 'importmesh.html', {'workdir': workdir, 'casename': casename, 'meshfile': file, 'status': err})


class ImportMeshDeleteView(views.APIView):
	def get(self, request):
		""" 
		get:
		Return a json example to erase case folder.
		"""

		attrs = {
			'workdir': 'workdir', 
                        'casename': 'pitzDaily'}

		return Response(attrs)

	def post(self, request):

		"""
		post:
		Delete folder workdir/casename folder
		"""

		if not request.data:
			return Response({"Delete case Status": 'NOT Deleted', 'desc': 'empty request body'}, status=status.HTTP_200_OK)

		templates_path = os.environ['YouFOAM_TEMPLATES_PATH']
		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		case_dir = workdir + '/' + casename

		if not os.path.exists(workdir):
			return Response({"Delete case Status": 'NOT Deleted', 'desc': 'Work Directory does not exist!'}, status=status.HTTP_200_OK)
		
		shutil.rmtree(case_dir)
		return Response({"Delete case Status": 'Deleted'}, status=status.HTTP_200_OK)