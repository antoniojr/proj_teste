from django.test import TestCase, Client
from rest_framework import status
import shutil
import os

class FvSchemesTests(TestCase):
    def test_create_fvschemes(self):
        client = Client()
        workdir = 'workdir'
        casename = 'pitzDaily'
        file = 'rotatingCavity.cas'
        case_path = workdir + '/' + casename
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)
        data = {'workdir': workdir, 'casename': casename, 'createCase': ''}
        response = client.post('/api/createcase/create/', data)
        if response.status_code == status.HTTP_302_FOUND:
            data['ddt'] = 'steadyState'
            data['grad'] = 'Gauss linear'
            data['divSchemesdefault'] = 'none'
            data['laplacian'] = 'Gauss linear corrected'
            data['inter'] = 'linear'
            data['sngrad'] = 'corrected'
            data['wallDist'] = 'meshWave'
            response = client.post('/api/fvschemes/create/', data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)