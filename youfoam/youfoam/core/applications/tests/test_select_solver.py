from unittest import TestCase
from applications.utilities.selectSolver import select_solver


class TestSelect_solver(TestCase):

    FLOW_TYPE = 0
    FLOW_REGIME = 1
    INFO = False

    def test_select_solver(self):
        print('*****************test_select_solver OK************************ \n')
        r = select_solver(self.FLOW_TYPE, self.FLOW_REGIME, self.INFO)
        self.assertEqual(r, 'pimpleFoam')
        print('*****************************************************************')

    def test_select_solver_miss_arg(self):
        print('*****************test_select_solver_miss_arg OK************************ \n')
        r = select_solver(2, 2, self.INFO)
        self.assertEqual(r['type'], 0)
        print('*****************************************************************')

    def test_select_solver_extra_args(self):
        print('*****************test_select_solver_extra_args OK************************ \n')
        r = select_solver(self.FLOW_TYPE, self.FLOW_REGIME, self.INFO, ext=self.FLOW_TYPE)
        self.assertEqual(r, 'pimpleFoam')
        print('*****************************************************************')


