"""
Documentation for choose_solver function
To call help, chooseSolver --help

Set info flag -i/--info for print output. You can check for error reasons

Return a solver name based on 2 arguments:
         FLOW_TYPE: 0 - Stationary
                    1 - Transient

         FLOW_REGIME: 0 - Incompressive
                      1 - Compressive

To call from command line:
python chooseSolver.py -t 0 -r 1 return pimpleFoam
"""
import argparse


class GenericSolver:

    solvers_list = ['simpleFoam', 'pimpleFoam', 'rhoSimpleFoam', 'rhoPimpleFoam']

    def __init__(self, flow_regime, flow_type):
        self.flow_regime = flow_regime
        self.flow_type = flow_type

    def GetName(self):
        if self.flow_regime == 0:
            return IncompSolver(self.flow_type).GetName()
        if self.flow_regime == 1:
            return CompSolver(self.flow_type).GetName()
        if self.flow_regime not in [0, 1]:
            return ','.join(self.solvers_list)


class IncompSolver(GenericSolver):

    solvers_list = ['simpleFoam', 'pimpleFoam']

    def __init__(self, flow_type):
        self.flow_type = flow_type

    def GetName(self):
        if self.flow_type == 0:
            return 'simpleFoam'
        if self.flow_type == 1:
            return 'pimpleFoam'
        return ','.join(self.solvers_list)


class CompSolver(GenericSolver):

    solvers_list = ['rhoSimpleFoam', 'rhoPimpleFoam']

    def __init__(self, flow_type):
        self.flow_type = flow_type

    def GetName(self):
        if self.flow_type == 0:
            return 'rhoSimpleFoam'
        if self.flow_type == 1:
            return 'rhoPimpleFoam'
        return ','.join(self.solvers_list)


"""Main function to call select solver method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='select solver help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-t", "--type", dest="type", help="flow_type")
    parser.add_argument("-r", "--regime", dest="regime", help="flow_regime")
    args = parser.parse_args()
    solver_name = GenericSolver(int(args.regime), int(args.type))
    if args.info:
        print(solver_name.GetName())
