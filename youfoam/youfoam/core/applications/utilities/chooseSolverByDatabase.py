"""
Documentation for choose_solver_by_database function
To call help, chooseSolverByDatabase --help

Set info flag -i/--info for print output. You can check for error reasons

Return a solver name based on 2 arguments:
         FLOW_TYPE: 0 - Stationary
                    1 - Transient

         FLOW_REGIME: 0 - Incompressive
                      1 - Compressive

To call from command line:
python chooseSolverByDatabase.py -t 0 -r 1 -> return pimpleFoam
"""

from pymongo import MongoClient
import argparse
import datetime


def choose_solver_by_database(db_name, flow_type, flow_regime, info):
    client = MongoClient()
    db = client[db_name]
    posts = db.posts

    solver_data = posts.find_one({"type": flow_type, "regime": flow_regime})
    if not solver_data:
        if info:
            print('Solver cannot be found')
        return 'Solver cannot be found'

    if info:
        print(solver_data['name'])
    return solver_data['name']


def init_database(db_name):
    client = MongoClient()
    db = client[db_name]
    posts = db.posts

    new_posts = [{
        "type": 0,
        "regime": 0,
        "name": "simpleFoam",
        "date": datetime.datetime(2018, 10, 30, 16, 42)

    }, {
        "type": 0,
        "regime": 1,
        "name": "pimpleFoam",
        "date": datetime.datetime(2018, 10, 30, 16, 42)
    }, {
        "type": 1,
        "regime": 0,
        "name": "rhoSimpleFoam",
        "date": datetime.datetime(2018, 10, 30, 16, 42)
    }, {
        "type": 1,
        "regime": 1,
        "name": "rhoPimpleFoam",
        "date": datetime.datetime(2018, 10, 30, 16, 42)
    }]

    return posts.insert_many(new_posts)


def insert_new_solver(db_name, flow_type, flow_regime, solver_name):
    client = MongoClient()
    db = client[db_name]
    posts = db.posts

    post = {
        "type": flow_type,
        "regime": flow_regime,
        "name": solver_name,
        "date": datetime.datetime.now()
    }

    return posts.insert_one(post).inserted_id


"""Main function to call select solver method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='select solver help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-t", "--type", dest="type", help="flow_type")
    parser.add_argument("-r", "--regime", dest="regime", help="flow_regime")
    args = parser.parse_args()
    db_name = 'foam_database'
    data = init_database(db_name)
    if data:
        choose_solver_by_database(db_name, int(args.type), int(args.regime), args.info)
