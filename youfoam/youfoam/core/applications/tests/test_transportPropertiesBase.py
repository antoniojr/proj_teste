from unittest import TestCase
from src.base.transportPropertiesBase import TransportPropertiesBase
from applications.utilities.generateMesh import generate_mesh
from applications.utilities.executeSolver import execute_solver
import os
import shutil


class TestTransportPropertiesBase(TestCase):
    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    COEFFS = '1e-07'
    LOG = '1'
    SOLVERNAME = 'simpleFoam'
    TRANSPORT_MODEL = 'CrossPowerLaw'

    crossPowerLawCoeffs = {'nu0 [0 2 -1 0 0 0 0]': '0.01',
                           'nuInf [0 2 -1 0 0 0 0]': '10',
                           'm [0 0 1 0 0 0 0]': '0.4',
                           'n [0 0 0 0 0 0 0]': '3'}

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASENAME
    source_dir = templates_path + '/' + CASENAME

    def test_write_dict_file_ok(self):
        print('******************transportProperties: test_write_dict_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        transport_properties = TransportPropertiesBase(self.TRANSPORT_MODEL, self.crossPowerLawCoeffs, self.WORKDIR,
                                                       self.CASENAME, self.INFO)
        transport_properties.of_version = '4.1'
        r = transport_properties.write_transport_dict_file()
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')

    def test_call_solver_ok(self):
        print('*****************transportProperties: test_call_solver_ok**************************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)
        transport_properties = TransportPropertiesBase(None, None, self.WORKDIR, self.CASENAME, self.INFO)
        tp = transport_properties.write_transport_dict_file()
        print(transport_properties.tpattrs)
        if tp['type'] == 1 and m['type'] == 1:
            print('executing solver!!!')
            r = execute_solver(self.WORKDIR, self.CASENAME, self.SOLVERNAME, self.LOG, self.INFO)
            self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('***************************************************************************** \n')

