from django.test import TestCase, Client
from django.urls import reverse
from rest_framework import status


class SimpleFoamTests(TestCase):
	def test_create_simplefoamcase(self):
		client = Client()

		CONTROLDICTATTRS = {
			  'application': 'simpleFoam', 
			  'startFrom': 'startTime',
		          'startTime': '0',
              		  'stopAt': 'endTime',
		          'endTime': '2000',
                          'deltaT': '1',
                          'writeControl': 'timeStep',
                          'writeInterval': '100',
                          'purgeWrite': '0',
              		  'writeFormat': 'ascii',
                          'writeCompression': 'off',
                          'writePrecision': '6',
                          'timeFormat': 'general',
                          'timePrecision': '6',
                          'runTimeModifiable': 'true', 
			  'workdir': 'workdir', 
			  'casename': 'pitzDaily', 
                          'show_output': 'true'}

		crossPowerLawCoeffs = {'nu0 [0 2 -1 0 0 0 0]': '0.01',
                           'nuInf [0 2 -1 0 0 0 0]': '10',
                           'm [0 0 1 0 0 0 0]': '0.4',
                           'n [0 0 0 0 0 0 0]': '3'}

		newtonian = {'nu [0 2 -1 0 0 0 0]': '1e-05'}

		TRANSPORTATTRS = {'transportModel': 'Newtonian', 
    		              'coeffs': crossPowerLawCoeffs, 
    		              'newtonian': newtonian}


		SIMULATIONTYPEATTRS = {'RASModel': 'kEpsilon',
                           'turbulence': 'off',
                           'printCoeffs': 'off'}

		TURBULENCEATTRS = {'simulationType': 'RAS', 
                           'simulationTypeAttrs': SIMULATIONTYPEATTRS}

		ddtSchemesAttrs = {'default': 'steadyState'}
		gradSchemesAttrs = {'default': 'Gauss linear'}
		divSchemesAttrs = {'default': 'none',
                       'div(phi,U)': 'bounded Gauss linearUpwind grad(U)',
                       'div(phi,k)': 'bounded Gauss limitedLinear 1',
                       'div(phi,epsilon)': 'bounded Gauss limitedLinear 1',
                       'div(phi,omega)': 'bounded Gauss limitedLinear 1',
                       'div(phi,v2)': 'bounded Gauss limitedLinear 1',
                       'div((nuEff*dev2(T(grad(U)))))': 'Gauss linear',
                       'div(nonlinearStress)': 'Gauss linear'}
		laplacianSchemesAttrs = {'default': 'Gauss linear corrected'}
		interpolationSchemesAttrs = {'default': 'linear'}
		snGradSchemesAttrs = {'default': 'corrected'}
		wallDistAttrs = {'method': 'meshWave'}


		FVSCHEMESATTRS = {'ddtSchemes': ddtSchemesAttrs, 
                          'gradSchemes': gradSchemesAttrs, 
                          'divSchemes': divSchemesAttrs, 
                          'laplacianSchemes': laplacianSchemesAttrs, 
                          'interpolationSchemes': interpolationSchemesAttrs, 
                          'snGradSchemes': snGradSchemesAttrs, 
                          'wallDist': wallDistAttrs}

		pAttrs = {'solver': 'GAMG',
                  'tolerance': '1e-08',
                  'reltol': '0.1',
                  'smoother': 'GaussSeidel'}

		UAttrs = {'solver': 'smoothSolver',
                  'smoother': 'symGaussSeidel',
                  'tolerance': '1e-05',
                  'relTol': '0.1'}

		kAttrs = {'solver': 'smoothSolver',
                  'smoother': 'symGaussSeidel',
                  'tolerance': '1e-05',
                  'relTol': '0.1'}

		epsilonAttrs = {'solver': 'smoothSolver',
                        'smoother': 'symGaussSeidel',
                        'tolerance': '1e-05',
                        'relTol': '0.1'}

		omegaAttrs = {'solver': 'smoothSolver',
                      'smoother': 'symGaussSeidel',
                      'tolerance': '1e-05',
                      'relTol': '0.1'}

		fAttrs = {'solver': 'smoothSolver',
                  'smoother': 'symGaussSeidel',
                  'tolerance': '1e-05',
                  'relTol': '0.1'}

		v2Attrs = {'solver': 'smoothSolver',
                   'smoother': 'symGaussSeidel',
                   'tolerance': '1e-05',
                   'relTol': '0.1'}

		solversAttrs = {'p': pAttrs,
                        'U': UAttrs,
                        'k': kAttrs,
                        'epsilon': epsilonAttrs,
                        'omega': omegaAttrs,
                        'f': fAttrs,
                        'v2': v2Attrs}

		residualControlAttrs = {'p': '1e-2',
                                'U': '1e-3',
                                'k': '1e-3',
                                'epsilon': '1e-3',
                                'omega': '1e-3',
                                'f': '1e-3',
                                'v2': '1e-3'}

		simpleAttrs = {'nNonOrthogonalCorrectors': '0',
                       'consistent': 'yes',
                       'residualControl': residualControlAttrs}

		equationsAttrs = {'U': '0.9',
                          'k': '0.9',
                          'epsilon': '0.9',
                          'omega': '0.9',
                          'f': '0.9',
                          'v2': '0.9'}

		fieldsAttrs = {'p': '0.9'}

		relaxFAttrs = {'equations': equationsAttrs,
                   'fields': fieldsAttrs}

		FVSOLUTIONATTRS = {'solvers': solversAttrs, 
                           'SIMPLE': simpleAttrs, 
                           'relaxationFactors': relaxFAttrs}

		PBOUNDARYFIELD = {'inlet':
							{'type': 'zeroGradient'}, 'outlet':
							{'type': 'fixedValue', 'value': 'uniform 0'},
	                      'upperWall':
	                         {'type': 'zeroGradient'},
	                      'lowerWall':
	                         {'type': 'zeroGradient'},
	                      'frontAndBack':
	                         {'type': 'empty'}
	                     }

		UBOUNDARYFIELD = {'inlet':
	                         {'type': 'fixedValue', 'value': 'uniform (10 0 0)'},
	                      'outlet':
	                         {'type': 'zeroGradient'},
	                      'upperWall':
	                         {'type': 'zeroGradient'},
	                      'lowerWall':
	                         {'type': 'zeroGradient'},
	                      'frontAndBack':
	                         {'type': 'empty'}
	                     }

		pattrs = {'file_name': 'p',
	              'file_location': '0',
	              'file_class': 'volScalarField',
	              'dimensions': '[0 2 -2 0 0 0 0]',
	              'internalField': 'uniform 0',
	              'boundaryField': {'inlet':
	                         {'type': 'zeroGradient'},
	                      'outlet':
	                         {'type': 'fixedValue', 'value': 'uniform 0'},
	                      'upperWall':
	                         {'type': 'zeroGradient'},
	                      'lowerWall':
	                         {'type': 'zeroGradient'},
	                      'frontAndBack':
	                         {'type': 'empty'}
	                     }
	              }

		uattrs = {'file_name': 'U',
	              'file_location': '0',
	              'file_class': 'volVectorField',
	              'dimensions': '[0 1 -1 0 0 0 0]',
	              'internalField': 'uniform (0 0 0)',
	              'boundaryField': {'inlet':
	                                    {'type': 'fixedValue', 'value': 'uniform (10 0 0)'},
	                                'outlet':
	                                    {'type': 'zeroGradient'},
	                                'upperWall':
	                                    {'type': 'zeroGradient'},
	                                'lowerWall':
	                                    {'type': 'zeroGradient'},
	                                'frontAndBack':
	                                    {'type': 'empty'}
	                                }
	              }

		kattrs = {'file_name': 'k',
	              'file_location': '0',
	              'file_class': 'volScalarField',
	              'dimensions': '[0 2 -2 0 0 0 0]',
	              'internalField': 'uniform 0.375',
	              'boundaryField': {'inlet':
	                                    {'type': 'fixedValue', 'value': 'uniform 0.375'},
	                                'outlet':
	                                    {'type': 'zeroGradient'},
	                                'upperWall':
	                                    {'type': 'kqRWallFunction', 'value': 'uniform 0.375'},
	                                'lowerWall':
	                                    {'type': 'kqRWallFunction', 'value': 'uniform 0.375'},
	                                'frontAndBack':
	                                    {'type': 'empty'}
	                                }
	              }

		nutattrs = {'file_name': 'nut',
	                'file_location': '0',
	                'file_class': 'volScalarField',
	                'dimensions': '[0 2 -1 0 0 0 0]',
	                'internalField': 'uniform 0',
	                'boundaryField': {'inlet':
	                                      {'type': 'calculated', 'value': 'uniform 0'},
	                                  'outlet':
	                                      {'type': 'calculated', 'value': 'uniform 0'},
	                                  'upperWall':
	                                      {'type': 'nutkWallFunction', 'value': 'uniform 0'},
	                                  'lowerWall':
	                                      {'type': 'nutkWallFunction', 'value': 'uniform 0'},
	                                  'frontAndBack':
	                                      {'type': 'empty'}
	                                  }
	                }

		epattrs = {'file_name': 'epsilon',
	               'file_location': '0',
	               'file_class': 'volScalarField',
	               'dimensions': '[0 2 -3 0 0 0 0]',
	               'internalField': 'uniform 14.855',
	               'boundaryField': {'inlet':
	                                     {'type': 'fixedValue', 'value': 'uniform 14.855'},
	                                 'outlet':
	                                     {'type': 'zeroGradient'},
	                                 'upperWall':
	                                     {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
	                                 'lowerWall':
	                                     {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
	                                 'frontAndBack':
	                                     {'type': 'empty'}
	                                 }
	               }

		data = {'controldict': str(CONTROLDICTATTRS), 
			'transportproperties': str(TRANSPORTATTRS),
		    'turbulenceproperties': str(TURBULENCEATTRS),
		    'fvschemes': str(FVSCHEMESATTRS),
		    'fvsolution': str(FVSOLUTIONATTRS),
	        'p': str(pattrs),
	        'u': str(uattrs),
	        'k': str(kattrs),
	        'nut': str(nutattrs),
	        'epsilon': str(epattrs),
	        'workdir': 'workdir', 
	        'casename': 'pitzDaily', 
	        'show_output': 'true'}

		response = client.post('/api/simplefoam/create/', data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
