from django.test import TestCase, Client
from rest_framework import status
import shutil
import os

# Create your tests here.
# https://docs.djangoproject.com/en/2.1/topics/testing/overview/
# ./manage.py test youfoam.server.apps.controldict.tests.ControlDictTests.test_delete_controldict

class ControlDictTests(TestCase):
    def test_create_controldict(self):
        client = Client()
        workdir = 'workdir'
        casename = 'pitzDaily'
        file = 'rotatingCavity.cas'
        case_path = workdir + '/' + casename
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)
        data = {'workdir': workdir, 'casename': casename, 'createCase': ''}
        response = client.post('/api/createcase/create/', data)
        if response.status_code == status.HTTP_302_FOUND:
            data['application'] = 'simpleFoam'
            data['startFrom'] = 'startTime'
            data['startTime'] = 0
            data['stopAt'] = 'endTime'
            data['endTime'] = 2000
            data['deltaT'] = 1
            data['writeControl'] = 'timeStep'
            data['writeInterval'] = 100
            data['purgeWrite'] = 0
            data['writeFormat'] = 'ascii'
            data['writeCompression'] = 'off'
            data['writePrecision'] = 6
            data['timeFormat'] = 'general'
            data['timePrecision'] = 6
            data['runTimeModifiable'] = 'true'
            response = client.post('/api/controldict/create/', data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)

    def test_delete_controldict(self):
    	client = Client()
    	data = {'workdir': 'workdir', 'casename': 'pitzDaily'}
    	response = client.post('/api/controldict/delete/', data)
    	self.assertEqual(response.status_code, status.HTTP_200_OK)

