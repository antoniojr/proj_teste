"""
Documentation for executeSolver function
To call help, executeSolver --help

Call solver by solverName.
Test if case folder exists
Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 executeSolver.py -w workdir -c casedir -s solvername -l <1 ou 0>
"""

import os
import argparse
import subprocess


def execute_solver(workdir, case_name, solver_name, log, info):
    # Already Validated by selectFolder
    # solver_list = {'0': 'simpleFoam', '1': 'pimpleFoam', '2': 'rhoSimpleFoam', '3': 'rhoPimpleFoam'}

    """Checking arguments"""
    if workdir is None or case_name is None or solver_name is None or log is None:
        if info:
            print('Missing arguments: workdir or case_name or solver_name or log not found')
        return {'type': 0, 'desc': 'Missing arguments: workdir or case_name or solver_name or log not found'}

    if not isinstance(workdir, str):
        if info:
            print('Wrong type: workdir can only be a string')
        return {'type': 0, 'desc': 'Wrong type: workdir can only be a string'}

    if not isinstance(case_name, str):
        if info:
            print('Wrong type: case_name can only be a string')
        return {'type': 0, 'desc': 'Wrong type: case_name can only be a string'}

    if not isinstance(solver_name, str):
        if info:
            print('Wrong type: solver_name can only be a string')
        return {'type': 0, 'desc': 'Wrong type: solver_name can only be a string'}

    if str(log) not in ['0', '1']:
        if info:
            print('Wrong type: log can be 0 (do not create) or 1 (create)')
        return {'type': 0, 'desc': 'Wrong type: log can be 0 (do not create) or 1 (create)'}

    case_dir = workdir + '/' + case_name

    if not os.path.exists(case_dir):
        if info:
            print('Wrong case directory: this folder did not exist')
        return {'type': 0, 'desc': 'Wrong case_directory: this folder did not exist'}

    try:
        script_path = os.environ['YouFOAM_BIN_PATH']
        res = subprocess.call([script_path + '/call_solver.sh', solver_name, case_dir, log])
        if res == 0:
            if info:
                print(solver_name + ' executed for case_dir = ' + case_dir)
            return {'type': 1, 'desc': solver_name + ' executed for case_dir = ' + case_dir}
        else:
            if info:
                print(solver_name + ' error for case_dir = ' + case_dir)
            return {'type': 0, 'desc': solver_name + ' error for case_dir = ' + case_dir}
    except subprocess.CalledProcessError as e:
        if info:
            print(solver_name + ' error. Error: %s' % e.output)
        return {'type': 0, 'desc': solver_name + ' error. Error: %s' % e.output}


"""Main function to call execute_solver method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='execute solver help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    parser.add_argument("-s", "--solver", dest="solver", help="solver name")
    parser.add_argument("-l", "--log", dest="log", help="create log")
    args = parser.parse_args()
    execute_solver(args.workdir, args.case, args.solver, args.log, args.info)
