#!/bin/sh
# $0 is the script name, $1 solver_name, $2 case_dir, $3 log

logFileName="$1"Log

solver=$1

${solver} -case $2 >> $logFileName 2>&1

if [ $3 = 0 ]
then
    rm $logFileName
else
    mv $logFileName $2
fi
