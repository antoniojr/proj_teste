"""
Documentation for clone case function
To call help, cloneCase --help

Set info flag -i/--info for print output. You can check for error reasons

Return 1: Case folder Cloned
       0: Error - SolverName dir not exist or already exist case folder

To call from command line:
python cloneCase.py -s source_case -w workDir -c dest_case -a <1 ou 0> -t <1 ou 0>
"""

import argparse
import shutil
import os


def clone_case(source_case, workdir, dest_case, all_folders, first_time, info):
    """Checking arguments"""
    if source_case is None or workdir is None or dest_case is None:
        if info:
            print('Missing arguments: source_case and/or workdir and/or dest_case')
        return {'type': 0, 'desc': 'Missing arguments: source_case and/or workdir and/or dest_case'}

    try:
        dest_dir = workdir + "/" + dest_case

        if os.path.exists(dest_dir) and os.path.exists(dest_dir + '/' + 'constant') + os.path.exists(dest_dir + '/' + 'system'):
            if info:
                print('Pasta de Destino e subdiretórios constant/system já existem!')
            return {'type': 0, 'desc': 'Pasta de Destino e subdiretórios constant/system já existem!'}

        if all_folders == 1:
            shutil.copytree(source_case, dest_dir)
        else:
            if first_time == 0:
                directories = os.listdir(source_case)
                time_dir_list = [int(t) for t in directories if t.isdigit()]
                shutil.copytree(source_case + '/' + str(time_dir_list[-1]),
                                dest_dir + '/' + str(sorted(time_dir_list)[-1]))
                shutil.copytree(source_case + '/constant', dest_dir + '/constant')
                shutil.copytree(source_case + '/system', dest_dir + '/system')
            else:
                shutil.copytree(source_case + '/0', dest_dir + '/0')
                shutil.copytree(source_case + '/constant', dest_dir + '/constant')
                shutil.copytree(source_case + '/system', dest_dir + '/system')

        if info:
            print('Case Folder Created at ' + dest_dir)
        return {'type': 1, 'desc': 'Case Folder Cloned at ' + dest_dir}
    except OSError as e:
        if info:
            print('Case Folder not Cloned. Error: %s' % e)
        return {'type': 0, 'desc': 'Case Folder not Cloned. Error: %s' % e}


"""Main function to call clone solver method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='clone case help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-s", "--source_case", dest="source_case", help="source case folder path")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work folder")
    parser.add_argument("-c", "--dest_case", dest="dest_case", help="Destination case folder name")
    parser.add_argument("-a", "--all_folders", dest="all_folders", help="Copy all case folder")
    parser.add_argument("-t", "--first_time", dest="first_time", help="copy first or last time folder")
    args = parser.parse_args()
    clone_case(args.source_case, args.workdir, args.dest_case, int(args.all_folders), int(args.first_time), args.info)
