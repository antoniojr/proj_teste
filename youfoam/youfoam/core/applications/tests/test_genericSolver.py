from unittest import TestCase
from applications.utilities.chooseSolver import GenericSolver


class TestGenericSolver(TestCase):
    FLOW_TYPE = 0
    FLOW_REGIME = 1
    INFO = False

    def test_GetName_ok(self):
        print('********************GenericSolver: test_GetName get solver name************************************\n')
        solver_name = GenericSolver(self.FLOW_REGIME, self.FLOW_TYPE)
        self.assertEqual(solver_name.GetName(), 'rhoSimpleFoam')
        print('*************************************************************************')

    def test_GetName_nok(self):
        print('********************GenericSolver: test_GetName get solver name with wrong flow type*********************\n')
        solver_name = GenericSolver(self.FLOW_REGIME, 3)
        self.assertEqual(solver_name.GetName(), 'rhoSimpleFoam,rhoPimpleFoam')
        print('*************************************************************************')

