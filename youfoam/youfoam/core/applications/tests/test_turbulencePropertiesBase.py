from unittest import TestCase
from src.base.turbulencePropertiesBase import TurbulencePropertiesBase
from applications.utilities.generateMesh import generate_mesh
from applications.utilities.executeSolver import execute_solver
import os
import shutil


class TestTurbulencePropertiesBase(TestCase):
    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    SIMULATIONTYPE1 = 'laminar'
    SIMULATIONTYPE2 = 'LES'
    SIMULATIONTYPE3 = 'RAS'
    SIMULATIONTYPEATTRS = {'RASModel': 'kEpsilon',
                           'turbulence': 'on',
                           'printCoeffs': 'on',
                           'delta': 'cubeRootVol'}
    LOG = '1'
    SOLVERNAME = 'simpleFoam'

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASENAME
    source_dir = templates_path + '/' + CASENAME

    def test_write_dict_file_ok(self):
        print('******************turbulanceProperties: test_write_dict_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        turbulence_properties = TurbulencePropertiesBase(self.SIMULATIONTYPE2, None, self.WORKDIR,
                                                         self.CASENAME, self.INFO)
        r = turbulence_properties.write_turbulance_dict_file()
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')

    def test_call_solver_ok(self):
        print('*****************turbulanceProperties: test_call_solver_ok**************************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)
        turbulence_properties = TurbulencePropertiesBase(self.SIMULATIONTYPE3, self.SIMULATIONTYPEATTRS,
                                                         self.WORKDIR, self.CASENAME, self.INFO)
        tp = turbulence_properties.write_turbulance_dict_file()
        if tp['type'] == 1 and m['type'] == 1:
            print('executing solver!!!')
            r = execute_solver(self.WORKDIR, self.CASENAME, self.SOLVERNAME, self.LOG, self.INFO)
            self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('***************************************************************************** \n')
