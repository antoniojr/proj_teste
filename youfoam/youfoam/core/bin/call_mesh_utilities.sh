#!/bin/sh
# $0 is the script name, $1 mesh utility function, $2 case_dir, $3 log

logFileName="$2""/""$1"Log

if [ $1 = 'blockMesh' ]
then
    blockMesh -case $2 >> $logFileName 2>&1
else

    checkMesh -case $2 >> $logFileName 2>&1
fi

if [ $3 = 0 ]
then
    rm $logFileName
fi

