import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from turbulencePropertiesBase import TurbulencePropertiesBase
import shutil
import copy

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render, redirect


class TurbulencePropertiesConfView(views.APIView):

	def get(self, request, workdir, casename):

		tp_data = {}
		tp_data['workdir'] = workdir
		tp_data['casename'] = casename
		tp_data['simulationType'] = 'RAS'
		return render(request, 'turbulencepropertiesconf.html', tp_data)


	def post(self, request):
		
		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		simulation_type = request.data.get('simulationType')

		tp_data = {}
		tp = TurbulencePropertiesBase(simulation_type, None, workdir, casename, True)
		tp_data = copy.deepcopy(tp.get_dict_attrs(simulation_type))

		if simulation_type == 'laminar':
			tp_data = {}
			tp_data['simulationType'] = 'laminar'
		#elif simulation_type == 'RAS':
			#del tp_data['simulationTypeAttrs']['delta']
			
		tp_data['workdir'] = workdir
		tp_data['casename'] = casename

		return render(request, 'turbulenceproperties.html', tp_data)


class TurbulencePropertiesCreateView(views.APIView):

	def post(self, request):
		
		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		simulation_type = request.data.get('simulationType')

		simulationTypeAttrs = {}
		for k,v in request.data.items():
			if 'simulationTypeAttrs' in k:
				new_k = k.replace('simulationTypeAttrs', '')
				simulationTypeAttrs[new_k] = v

		#tp_data = {}
		tp = TurbulencePropertiesBase(simulation_type, simulationTypeAttrs, workdir, casename, True)
		tp_data = copy.deepcopy(tp.get_dict_attrs(simulation_type))

		tp.tuattrs = {}
		tp.tuattrs['simulationType'] = simulation_type
		if not simulation_type == 'laminar':
			tp.tuattrs['simulationTypeAttrs'] = simulationTypeAttrs
		
		#if simulation_type == 'RAS':
		#	del tp.tuattrs['simulationTypeAttrs']['delta']

		r = tp.write_turbulance_dict_file()

		tp_data['workdir'] = workdir
		tp_data['casename'] = casename

		simulation_type_model = tp.get_model_name(simulation_type)

		#return render(request, 'turbulenceproperties.html', tp_data)
		# FIXME: Refazendo Workflow. return redirect("/api/fvschemes/edit/{}/{}/{}/{}/".format(workdir, casename, simulation_type, simulation_type_model))
		return redirect("/api/transportproperties/modeledit/{}/{}/{}/{}/".format(workdir, casename, simulation_type, simulation_type_model))
