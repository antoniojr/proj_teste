from django.urls import path
from . import views

urlpatterns = [
    path('importmesh/convert/', views.ImportMeshView.as_view()),
    path('importmesh/delete/', views.ImportMeshDeleteView.as_view()),
    path('importmesh/index/<workdir>/<casename>/', views.ImportMesh.index)
]
