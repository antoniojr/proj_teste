"""
Documentation for epsilon base class
To call help, epsilonBase --help

Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 epsilonBase.py -w workdir -c casename -i
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class EpsilonBase(CommonPropertiesBase):

    ep_file_name = 'epsilon'
    ep_file_location = '0'
    ep_file_class = 'volScalarField'

    dimensions = '[0 2 -3 0 0 0 0]'

    internalField = 'uniform 14.855'

    boundaryField = {'inlet':
                         {'type': 'fixedValue', 'value': 'uniform 14.855'},
                     'outlet':
                         {'type': 'zeroGradient'},
                     'upperWall':
                         {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
                     'lowerWall':
                         {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
                     'frontAndBack':
                         {'type': 'empty'}
                     }

    directive = '#includeEtc "caseDicts/setConstraintTypes"'

    epattrs = {'dimensions': dimensions,
               'internalField': internalField,
               'boundaryField': boundaryField}

    def __init__(self, dim, intf, bf, wd, c, info):
        self.epattrs['dimensions'] = dim if dim else self.epattrs['dimensions']
        self.epattrs['internalField'] = intf if intf else self.epattrs['internalField']
        self.epattrs['boundaryField'] = bf if bf else self.epattrs['boundaryField']

        self.work_dir = wd
        self.case_name = c
        self.info = True if info else False

    def write_epsilon_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.ep_file_location + '/' + self.ep_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.ep_file_class, self.ep_file_location, self.ep_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            self.write_dict(fo, self.epattrs, '')
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}

        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def get_dict_attrs(self):
        return self.epattrs

    def updated_dict_attrs(self, new_attrs):
        self.epattrs = new_attrs

    def update_boundary_fields_attrs(self, new_attrs):
        self.boundaryField = new_attrs
        self.epattrs['boundaryField'] = self.boundaryField

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str:
                if v.find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                if k == 'boundaryField':
                    fo.write(space + self.directive + '\n\n')
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table


"""Main function to call epsilonBase method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='epsilon_base help')
    parser.add_argument("-d", "--dimensions", dest="dimensions", help="dimensions")
    parser.add_argument("-f", "--internalField", dest="internalField", help="internalField")
    parser.add_argument("-b", "--boundaryField", dest="boundaryField", help="boundaryField")
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    ep = EpsilonBase(args.dimensions, args.internalField, args.boundaryField, args.workdir, args.case, args.info)

    ep.write_epsilon_dict_file()
