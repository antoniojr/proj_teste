from django.urls import path
from . import views

urlpatterns = [
    path('fvsolution/create/', views.FvSolutionCreateView.as_view()),
    path('fvsolution/delete/', views.FvSolutionDeleteView.as_view()),
    path('fvsolution/index/<workdir>/<casename>/<simulation_type>/<simulation_type_model>/', views.FvSolution.index)
]
