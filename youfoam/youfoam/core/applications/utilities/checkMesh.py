"""
Documentation for importMesh function
To call help, generateMesh --help

Check Mesh utility.
Test if case folder exists
Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 generateMesh.py -w workdir -c casedir -l <1 ou 0>
"""

import os
import argparse
import subprocess


def check_mesh(workdir, case_name, log, info):

    """Checking arguments"""
    if workdir is None or case_name is None or log is None:
        if info:
            print('Missing arguments: workdir or case_name or log not found')
        return {'type': 0, 'desc': 'Missing arguments: workdir or case_name or log not found'}

    if str(log) not in ['0', '1']:
        if info:
            print('Wrong type: log can be 0 (do not create) or 1 (create)')
        return {'type': 0, 'desc': 'Wrong type: log can be 0 (do not create) or 1 (create)'}

    if not isinstance(workdir, str):
        if info:
            print('Wrong type: workdir can only be a string')
        return {'type': 0, 'desc': 'Wrong type: workdir can only be a string'}

    if not isinstance(case_name, str):
        if info:
            print('Wrong type: case_name can only be a string')
        return {'type': 0, 'desc': 'Wrong type: case_name can only be a string'}

    case_dir = workdir + '/' + case_name

    if not os.path.exists(case_dir):
        if info:
            print('Wrong case directory: this folder did not exist')
        return {'type': 0, 'desc': 'Wrong case_directory: this folder did not exist'}

    util_func = 'checkMesh'
    try:
        script_path = os.environ['YouFOAM_BIN_PATH']
        res = subprocess.call([script_path + '/call_mesh_utilities.sh', util_func, case_dir, log])
        if res == 0:
            if info:
                print(util_func + ' executed for case_dir = ' + case_dir)
            return {'type': 1, 'desc': util_func + ' executed for case_dir = ' + case_dir}
        else:
            if info:
                print(util_func + ' error for case_dir = ' + case_dir)
            return {'type': 0, 'desc': util_func + ' error for case_dir = ' + case_dir}
    except subprocess.CalledProcessError as e:
        if info:
            print(util_func + ' error. Error: %s' % e.output)
        return {'type': 0, 'desc': util_func + ' error. Error: %s' % e.output}


"""Main function to call check_mesh method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='check mesh help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    parser.add_argument("-l", "--log", dest="log", help="create log")
    args = parser.parse_args()
    check_mesh(args.workdir, args.case, args.log, args.info)
