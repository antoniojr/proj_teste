"""
Documentation for sompleFoam class
To call help, simpleFoam --help

Set info flag -i/--info for print output. You can check for error reasons

Return a value that can be:
         1: OK -> simpleFoam case created

         0: NOK -> simpleFoam create error!!

To call from command line:
python3 simpleFoam.py -w workdir -c casename -i "case creation success"
"""

import sys
import argparse
import os

sys.path.append(os.environ['YouFOAM_APPLICATIONS_PATH'] + '/utilities')
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')

from createCase import create_case
from controlDictBase import ControlDictBase
from transportPropertiesBase import TransportPropertiesBase
from turbulencePropertiesBase import TurbulencePropertiesBase
from fvSchemesBase import FvSchemesBase
from fvSolutionBase import FvSolutionBase

from fieldBase import FieldBase
from boundaryBase import BoundaryBase


class SimpleFoam(ControlDictBase, TransportPropertiesBase, TurbulencePropertiesBase, FvSchemesBase,
                 FvSolutionBase, FieldBase, BoundaryBase):

    pattrs = {'file_name': 'p',
              'file_location': '0',
              'file_class': 'volScalarField',
              'dimensions': '[0 2 -2 0 0 0 0]',
              'internalField': 'uniform 0',
              'boundaryField': {'inlet':
                         {'type': 'fixedValue', 'value': 'uniform 14.855'},
                     'outlet':
                         {'type': 'zeroGradient'},
                     'upperWall':
                         {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
                     'lowerWall':
                         {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
                     'frontAndBack':
                         {'type': 'empty'}
                                }
              }

    uattrs = {'file_name': 'U',
              'file_location': '0',
              'file_class': 'volVectorField',
              'dimensions': '[0 1 -1 0 0 0 0]',
              'internalField': 'uniform (0 0 0)',
              'boundaryField': {'inlet':
                         {'type': 'fixedValue', 'value': 'uniform (10 0 0)'},
                      'outlet':
                         {'type': 'zeroGradient'},
                      'upperWall':
                         {'type': 'zeroGradient'},
                      'lowerWall':
                         {'type': 'zeroGradient'},
                      'frontAndBack':
                         {'type': 'empty'}
                     }
              }

    fieldsAttr = {'p': pattrs,
                  'u': uattrs}

    def __init__(self, app, sf, st, sa, et, dt, wc, wi, pw, wf, wcp, wp, tf, tp, rt, tm, tmc, sim_type, sim_type_at,
                 ddt, grad, div, laplacian, inter, sngrad, walld, solvers, simple, relax, fields_attr, wd, c, info):

        ControlDictBase.__init__(self, app, sf, st, sa, et, dt, wc, wi, pw, wf, wcp, wp, tf, tp, rt, wd, c, info)
        TransportPropertiesBase.__init__(self, tm, tmc, wd, c, info)
        TurbulencePropertiesBase.__init__(self, sim_type, sim_type_at, wd, c, info)
        FvSchemesBase.__init__(self, ddt, grad, div, laplacian, inter, sngrad, walld, wd, c, info)
        FvSolutionBase.__init__(self, solvers, simple, relax, wd, c, info)

        BoundaryBase.__init__(self, wd, c, info)

        self.fieldsAttr = fields_attr if fields_attr else self.fieldsAttr
        self.work_dir = wd
        self.case_name = c
        self.info = True if info else False

    def write_case(self):
        try:

            # BoundaryBase.read_boundary_file(self)
            # self.validate_field_patches(self.fieldsAttr, BoundaryBase.get_patch_list(self))

            case_folders = create_case(self.work_dir, self.case_name, self.info)
            if case_folders['type'] == 1:
                ControlDictBase.write_control_dict_file(self)
                TransportPropertiesBase.write_transport_dict_file(self)
                TurbulencePropertiesBase.write_turbulance_dict_file(self)
                FvSchemesBase.write_fvschemes_dict_file(self)
                FvSolutionBase.write_fvsolution_dict_file(self)

                field = FieldBase()
                self.write_field_files(field, 'p')
                self.write_field_files(field, 'u')

                if TurbulencePropertiesBase.get_simulation_type(self) == 'RAS':
                    sim_ta = TurbulencePropertiesBase.get_model_name(self)
                    if sim_ta in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:

                        self.write_field_files(field, 'k')
                        self.write_field_files(field, 'nut')
                        self.write_field_files(field, 'epsilon')
                    elif sim_ta in ['kOmega', 'kOmegaSST', 'kOmegaSSTSAS']:

                        self.write_field_files(field, 'k')
                        self.write_field_files(field, 'nut')
                        self.write_field_files(field, 'omega')
                    elif sim_ta in ['LRR', 'SSG']:

                        self.write_field_files(field, 'nut')
                        self.write_field_files(field, 'epsilon')
                        self.write_field_files(field, 'R')

                    elif sim_ta in ['v2f']:

                        self.write_field_files(field, 'k')
                        self.write_field_files(field, 'nut')
                        self.write_field_files(field, 'epsilon')
                        self.write_field_files(field, 'f')
                        self.write_field_files(field, 'v2')

                    elif sim_ta in ['kOmegaSSTLM']:

                        self.write_field_files(field, 'k')
                        self.write_field_files(field, 'nut')
                        self.write_field_files(field, 'omega')
                        self.write_field_files(field, 'gammaInt')
                        self.write_field_files(field, 'ReThetat')

            if self.info:
                print('simpleFoam case creation success')
            return {'type': 1, 'desc': 'simpleFoam case creation success'}
        except IOError as e:
            if self.info:
                print('Error: %s' % e.output)
            return {'type': 0, 'desc': 'Error: %s' % e.output}

    def write_field_files(self, field, type):
        field.write_field_dict_file(self.fieldsAttr[type]['file_name'],
                                    self.fieldsAttr[type]['file_location'],
                                    self.fieldsAttr[type]['file_class'],
                                    self.fieldsAttr[type]['dimensions'],
                                    self.fieldsAttr[type]['internalField'],
                                    self.fieldsAttr[type]['boundaryField'],
                                    self.work_dir, self.case_name, self.info)

    def validate_field_patches(self, boundary_patch_list):
        for k, v in self.fieldsAttr.items():
            boundary_field = v['boundaryField']
            field_patch_list = list(boundary_field.keys())
            intersec = list(set(field_patch_list) & set(boundary_patch_list))
            if len(intersec) != len(boundary_patch_list):
                msg = 'Validate Field Patch Error in file ' + k
                if self.info:
                    print(msg)
                # raise Exception(msg)
                return {'type': 0, 'desc': msg}
        msg = 'Patches validated in all files!'
        if self.info:
            print(msg)
        return {'type': 1, 'desc': msg}

    def get_fields_attrs(self):
        return self.fieldsAttr


"""Main function to call simpleFoam method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='transport_dict_base help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")

    parser.add_argument("-app", "--application", dest="application", help="application")
    parser.add_argument("-sf", "--startFrom", dest="startFrom", help="startFrom")
    parser.add_argument("-st", "--startTime", dest="startTime", help="startTime")
    parser.add_argument("-sa", "--stopAt", dest="stopAt", help="stopAt")
    parser.add_argument("-et", "--endTime", dest="endTime", help="endTime")
    parser.add_argument("-dt", "--deltaT", dest="deltaT", help="deltaT")
    parser.add_argument("-wc", "--writeControl", dest="writeControl", help="writeControl")
    parser.add_argument("-wi", "--writeInterval", dest="writeInterval", help="writeInterval")
    parser.add_argument("-pw", "--purgeWrite", dest="purgeWrite", help="purgeWrite")
    parser.add_argument("-wf", "--writeFormat", dest="writeFormat", help="writeFormat")
    parser.add_argument("-wcp", "--writeCompression", dest="writeCompression", help="writeCompression")
    parser.add_argument("-wp", "--writePrecision", dest="writePrecision", help="writePrecision")
    parser.add_argument("-tf", "--timeFormat", dest="timeFormat", help="timeFormat")
    parser.add_argument("-tp", "--timePrecision", dest="timePrecision", help="timePrecision")
    parser.add_argument("-rt", "--runTimeModifiable", dest="runTimeModifiable", help="runTimeModifiable")

    parser.add_argument("-tm", "--transportModel", dest="transportModel", help="transportModel")
    parser.add_argument("-tmc", "--transportModelCoeffs", dest="transportModelCoeffs", help="transportModelCoeffs")

    parser.add_argument("-sim_type", "--simulationType", dest="simulationType", help="simulationType")
    parser.add_argument("-sim_type_at", "--simulationTypeAttrs", dest="simulationTypeAttrs", help="simulationTypeAttrs")

    parser.add_argument("-ddt", "--ddtSchemes", dest="ddtSchemes", help="ddtSchemes")
    parser.add_argument("-grad", "--gradSchemes", dest="gradSchemes", help="gradSchemes")
    parser.add_argument("-div", "--divSchemes", dest="divSchemes", help="divSchemes")
    parser.add_argument("-lap", "--laplacianSchemes", dest="laplacianSchemes", help="laplacianSchemes")
    parser.add_argument("-int", "--interpolationSchemes", dest="interpolationSchemes", help="interpolationSchemes")
    parser.add_argument("-sng", "--snGradSchemes", dest="snGradSchemes", help="snGradSchemes")
    parser.add_argument("-wall", "--wallDist", dest="wallDist", help="wallDist")

    parser.add_argument("-solvers", "--solvers", dest="solvers", help="solvers")
    parser.add_argument("-simple", "--simple", dest="simple", help="simple")
    parser.add_argument("-rel", "--relaxationFactors", dest="relaxationFactors", help="relaxationFactors")

    args = parser.parse_args()
    case = SimpleFoam(args.application, args.startFrom, args.startTime, args.stopAt, args.endTime, args.deltaT,
                      args.writeControl, args.writeInterval, args.purgeWrite, args.writeFormat,
                      args.writeCompression, args.writePrecision, args.timeFormat, args.timePrecision,
                      args.runTimeModifiable, args.transportModel, args.transportModelCoeffs, args.simulationType,
                      args.simulationTypeAttrs, args.ddtSchemes, args.gradSchemes, args.divSchemes,
                      args.laplacianSchemes, args.interpolationSchemes, args.snGradSchemes, args.wallDist,
                      args.solvers, args.simple, args.relaxationFactors, args.fieldsAttrs,
                      args.workdir, args.case, args.info)

    case.write_case()
