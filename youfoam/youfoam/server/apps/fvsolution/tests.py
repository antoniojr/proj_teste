from django.test import TestCase, Client
from rest_framework import status
import shutil
import os

# Create your tests here.
# ./manage.py test youfoam.server.apps.fvsolution.tests.FvSolutionTests 

class FvSolutionTests(TestCase):
    def test_create_fvsolution(self):
        client = Client()
        workdir = 'workdir'
        casename = 'pitzDaily'
        file = 'rotatingCavity.cas'
        case_path = workdir + '/' + casename
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)
        data = {'workdir': workdir, 'casename': casename, 'createCase': ''}
        response = client.post('/api/createcase/create/', data)
        if response.status_code == status.HTTP_302_FOUND:
            data['SIMPLEnNonOrthogonalCorrectors'] = '0'
            data['SIMPLEconsistent'] = 'yes'
            response = client.post('/api/fvsolution/create/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)

    def test_delete_fvsolution(self):
    	client = Client()
    	data = {'workdir': 'workdir', 'casename': 'pitzDaily'}
    	response = client.post('/api/fvsolution/delete/', data)
    	self.assertEqual(response.status_code, status.HTTP_200_OK)

