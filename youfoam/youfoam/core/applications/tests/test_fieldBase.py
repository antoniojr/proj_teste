from unittest import TestCase
from src.base.fieldBase import FieldBase
# from applications.utilities.generateMesh import generate_mesh
# from applications.utilities.executeSolver import execute_solver
import os
import shutil


class TestFieldBase(TestCase):

    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    LOG = '1'
    SOLVERNAME = 'simpleFoam'

    pattrs = {'file_name': 'p',
              'file_location': '0',
              'file_class': 'volScalarField',
              'dimensions': '[0 2 -2 0 0 0 0]',
              'internalField': 'uniform 0',
              'boundaryField': {'inlet':
                                    {'type': 'fixedValue', 'value': 'uniform 14.855'},
                                'outlet':
                                    {'type': 'zeroGradient'},
                                'upperWall':
                                    {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
                                'lowerWall':
                                    {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
                                'frontAndBack':
                                    {'type': 'empty'}
                                }
              }

    uattrs = {'file_name': 'U',
              'file_location': '0',
              'file_class': 'volVectorField',
              'dimensions': '[0 1 -1 0 0 0 0]',
              'internalField': 'uniform (0 0 0)',
              'boundaryField': {'inlet':
                                    {'type': 'fixedValue', 'value': 'uniform (10 0 0)'},
                                'outlet':
                                    {'type': 'zeroGradient'},
                                'upperWall':
                                    {'type': 'zeroGradient'},
                                'lowerWall':
                                    {'type': 'zeroGradient'},
                                'frontAndBack':
                                    {'type': 'empty'}
                                }
              }

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASENAME
    source_dir = templates_path + '/' + CASENAME

    def test_write_field_dict_file(self):
        print('******************FieldBase: test_write_dict_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)

        f = FieldBase()

        f.write_field_dict_file(self.pattrs['file_name'], self.pattrs['file_location'], self.pattrs['file_class'],
                                self.pattrs['dimensions'], self.pattrs['internalField'],
                                self.pattrs['boundaryField'], self.WORKDIR, self.CASENAME, self.INFO)

        r = f.write_field_dict_file(self.uattrs['file_name'], self.uattrs['file_location'], self.uattrs['file_class'],
                                    self.uattrs['dimensions'], self.uattrs['internalField'],
                                    self.uattrs['boundaryField'], self.WORKDIR, self.CASENAME, self.INFO)

        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')
