import sys
import os
sys.path.append(os.environ['YouFOAM_SOLVERS_PATH'])
sys.path.append(os.environ['YouFOAM_APPLICATIONS_PATH'] + '/utilities')
from simpleFoam import SimpleFoam
from importMesh import import_mesh
from executeSolver import execute_solver
import shutil
import json
from django.http import QueryDict

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render, redirect

class SimpleFoamCreateView(views.APIView):
	
	def get(self, request):

		""" 
		get:
		Return a list of all simpleFoam attributes.
		"""

		CONTROLDICTATTRS = {
			  'application': 'simpleFoam', 
			  'startFrom': 'startTime',
		          'startTime': '0',
              		  'stopAt': 'endTime',
		          'endTime': '2000',
                          'deltaT': '1',
                          'writeControl': 'timeStep',
                          'writeInterval': '100',
                          'purgeWrite': '0',
              		  'writeFormat': 'ascii',
                          'writeCompression': 'off',
                          'writePrecision': '6',
                          'timeFormat': 'general',
                          'timePrecision': '6',
                          'runTimeModifiable': 'true', 
			  'workdir': 'workdir', 
			  'casename': 'pitzDaily', 
                          'show_output': 'true'}

		crossPowerLawCoeffs = {'nu0 [0 2 -1 0 0 0 0]': '0.01',
                           'nuInf [0 2 -1 0 0 0 0]': '10',
                           'm [0 0 1 0 0 0 0]': '0.4',
                           'n [0 0 0 0 0 0 0]': '3'}

		newtonian = {'nu [0 2 -1 0 0 0 0]': '1e-05'}

		TRANSPORTATTRS = {'transportModel': 'Newtonian', 
    		              'coeffs': crossPowerLawCoeffs, 
    		              'newtonian': newtonian}


		SIMULATIONTYPEATTRS = {'RASModel': 'kEpsilon',
                           'turbulence': 'off',
                           'printCoeffs': 'off'}

		TURBULENCEATTRS = {'simulationType': 'RAS', 
                           'simulationTypeAttrs': SIMULATIONTYPEATTRS}

		ddtSchemesAttrs = {'default': 'steadyState'}
		gradSchemesAttrs = {'default': 'Gauss linear'}
		divSchemesAttrs = {'default': 'none',
                       'div(phi,U)': 'bounded Gauss linearUpwind grad(U)',
                       'div(phi,k)': 'bounded Gauss limitedLinear 1',
                       'div(phi,epsilon)': 'bounded Gauss limitedLinear 1',
                       'div(phi,omega)': 'bounded Gauss limitedLinear 1',
                       'div(phi,v2)': 'bounded Gauss limitedLinear 1',
                       'div((nuEff*dev2(T(grad(U)))))': 'Gauss linear',
                       'div(nonlinearStress)': 'Gauss linear'}
		laplacianSchemesAttrs = {'default': 'Gauss linear corrected'}
		interpolationSchemesAttrs = {'default': 'linear'}
		snGradSchemesAttrs = {'default': 'corrected'}
		wallDistAttrs = {'method': 'meshWave'}


		FVSCHEMESATTRS = {'ddtSchemes': ddtSchemesAttrs, 
                          'gradSchemes': gradSchemesAttrs, 
                          'divSchemes': divSchemesAttrs, 
                          'laplacianSchemes': laplacianSchemesAttrs, 
                          'interpolationSchemes': interpolationSchemesAttrs, 
                          'snGradSchemes': snGradSchemesAttrs, 
                          'wallDist': wallDistAttrs}

		pAttrs = {'solver': 'GAMG',
                  'tolerance': '1e-08',
                  'reltol': '0.1',
                  'smoother': 'GaussSeidel'}

		UAttrs = {'solver': 'smoothSolver',
                  'smoother': 'symGaussSeidel',
                  'tolerance': '1e-05',
                  'relTol': '0.1'}

		kAttrs = {'solver': 'smoothSolver',
                  'smoother': 'symGaussSeidel',
                  'tolerance': '1e-05',
                  'relTol': '0.1'}

		epsilonAttrs = {'solver': 'smoothSolver',
                        'smoother': 'symGaussSeidel',
                        'tolerance': '1e-05',
                        'relTol': '0.1'}

		omegaAttrs = {'solver': 'smoothSolver',
                      'smoother': 'symGaussSeidel',
                      'tolerance': '1e-05',
                      'relTol': '0.1'}

		fAttrs = {'solver': 'smoothSolver',
                  'smoother': 'symGaussSeidel',
                  'tolerance': '1e-05',
                  'relTol': '0.1'}

		v2Attrs = {'solver': 'smoothSolver',
                   'smoother': 'symGaussSeidel',
                   'tolerance': '1e-05',
                   'relTol': '0.1'}

		solversAttrs = {'p': pAttrs,
                        'U': UAttrs,
                        'k': kAttrs,
                        'epsilon': epsilonAttrs,
                        'omega': omegaAttrs,
                        'f': fAttrs,
                        'v2': v2Attrs}

		residualControlAttrs = {'p': '1e-2',
                                'U': '1e-3',
                                'k': '1e-3',
                                'epsilon': '1e-3',
                                'omega': '1e-3',
                                'f': '1e-3',
                                'v2': '1e-3'}

		simpleAttrs = {'nNonOrthogonalCorrectors': '0',
                       'consistent': 'yes',
                       'residualControl': residualControlAttrs}

		equationsAttrs = {'U': '0.9',
                          'k': '0.9',
                          'epsilon': '0.9',
                          'omega': '0.9',
                          'f': '0.9',
                          'v2': '0.9'}

		fieldsAttrs = {'p': '0.9'}

		relaxFAttrs = {'equations': equationsAttrs,
                   'fields': fieldsAttrs}

		FVSOLUTIONATTRS = {'solvers': solversAttrs, 
                           'SIMPLE': simpleAttrs, 
                           'relaxationFactors': relaxFAttrs}

		PBOUNDARYFIELD = {'inlet':
							{'type': 'zeroGradient'}, 'outlet':
							{'type': 'fixedValue', 'value': 'uniform 0'},
	                      'upperWall':
	                         {'type': 'zeroGradient'},
	                      'lowerWall':
	                         {'type': 'zeroGradient'},
	                      'frontAndBack':
	                         {'type': 'empty'}
	                     }

		UBOUNDARYFIELD = {'inlet':
	                         {'type': 'fixedValue', 'value': 'uniform (10 0 0)'},
	                      'outlet':
	                         {'type': 'zeroGradient'},
	                      'upperWall':
	                         {'type': 'zeroGradient'},
	                      'lowerWall':
	                         {'type': 'zeroGradient'},
	                      'frontAndBack':
	                         {'type': 'empty'}
	                     }

		pattrs = {'file_name': 'p',
	              'file_location': '0',
	              'file_class': 'volScalarField',
	              'dimensions': '[0 2 -2 0 0 0 0]',
	              'internalField': 'uniform 0',
	              'boundaryField': {'inlet':
	                         {'type': 'zeroGradient'},
	                      'outlet':
	                         {'type': 'fixedValue', 'value': 'uniform 0'},
	                      'upperWall':
	                         {'type': 'zeroGradient'},
	                      'lowerWall':
	                         {'type': 'zeroGradient'},
	                      'frontAndBack':
	                         {'type': 'empty'}
	                     }
	              }

		uattrs = {'file_name': 'U',
	              'file_location': '0',
	              'file_class': 'volVectorField',
	              'dimensions': '[0 1 -1 0 0 0 0]',
	              'internalField': 'uniform (0 0 0)',
	              'boundaryField': {'inlet':
	                                    {'type': 'fixedValue', 'value': 'uniform (10 0 0)'},
	                                'outlet':
	                                    {'type': 'zeroGradient'},
	                                'upperWall':
	                                    {'type': 'zeroGradient'},
	                                'lowerWall':
	                                    {'type': 'zeroGradient'},
	                                'frontAndBack':
	                                    {'type': 'empty'}
	                                }
	              }

		kattrs = {'file_name': 'k',
	              'file_location': '0',
	              'file_class': 'volScalarField',
	              'dimensions': '[0 2 -2 0 0 0 0]',
	              'internalField': 'uniform 0.375',
	              'boundaryField': {'inlet':
	                                    {'type': 'fixedValue', 'value': 'uniform 0.375'},
	                                'outlet':
	                                    {'type': 'zeroGradient'},
	                                'upperWall':
	                                    {'type': 'kqRWallFunction', 'value': 'uniform 0.375'},
	                                'lowerWall':
	                                    {'type': 'kqRWallFunction', 'value': 'uniform 0.375'},
	                                'frontAndBack':
	                                    {'type': 'empty'}
	                                }
	              }

		nutattrs = {'file_name': 'nut',
	                'file_location': '0',
	                'file_class': 'volScalarField',
	                'dimensions': '[0 2 -1 0 0 0 0]',
	                'internalField': 'uniform 0',
	                'boundaryField': {'inlet':
	                                      {'type': 'calculated', 'value': 'uniform 0'},
	                                  'outlet':
	                                      {'type': 'calculated', 'value': 'uniform 0'},
	                                  'upperWall':
	                                      {'type': 'nutkWallFunction', 'value': 'uniform 0'},
	                                  'lowerWall':
	                                      {'type': 'nutkWallFunction', 'value': 'uniform 0'},
	                                  'frontAndBack':
	                                      {'type': 'empty'}
	                                  }
	                }

		epattrs = {'file_name': 'epsilon',
	               'file_location': '0',
	               'file_class': 'volScalarField',
	               'dimensions': '[0 2 -3 0 0 0 0]',
	               'internalField': 'uniform 14.855',
	               'boundaryField': {'inlet':
	                                     {'type': 'fixedValue', 'value': 'uniform 14.855'},
	                                 'outlet':
	                                     {'type': 'zeroGradient'},
	                                 'upperWall':
	                                     {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
	                                 'lowerWall':
	                                     {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
	                                 'frontAndBack':
	                                     {'type': 'empty'}
	                                 }
	               }

		SIMPLEFOAMATTR = {'controldict': CONTROLDICTATTRS,
		              'transportproperties': TRANSPORTATTRS,
		              'turbulenceproperties': TURBULENCEATTRS,
		              'fvschemes': FVSCHEMESATTRS,
		              'fvsolution': FVSOLUTIONATTRS,
	                  'p': pattrs,
	                  'u': uattrs,
	                  'k': kattrs,
	                  'nut': nutattrs,
	                  'epsilon': epattrs,
	                  'workdir': 'workdir', 
	                  'casename': 'pitzDaily', 
	                  'show_output': 'true'}


		return Response(SIMPLEFOAMATTR)


	def post(self, request):

		"""
		post:
		Create simplefoam
		"""		

		workdir = request.data.get('workdir')
		casename = request.data.get('casename')

		app = eval(request.data.get('controldict'))['application'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['application']
		sf = eval(request.data.get('controldict'))['startFrom'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['startFrom']
		st = eval(request.data.get('controldict'))['startTime'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['startTime']
		sa = eval(request.data.get('controldict'))['stopAt'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['stopAt']
		et = eval(request.data.get('controldict'))['endTime'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['endTime']
		dt = eval(request.data.get('controldict'))['deltaT'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['deltaT']
		wc = eval(request.data.get('controldict'))['writeControl'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['writeControl']
		wi = eval(request.data.get('controldict'))['writeInterval'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['writeInterval']
		pw = eval(request.data.get('controldict'))['purgeWrite'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['purgeWrite']
		wf = eval(request.data.get('controldict'))['writeFormat'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['writeFormat']
		wcp = eval(request.data.get('controldict'))['writeCompression'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['writeCompression']
		wp = eval(request.data.get('controldict'))['writePrecision'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['writePrecision']
		tf = eval(request.data.get('controldict'))['timeFormat'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['timeFormat']
		tp = eval(request.data.get('controldict'))['timePrecision'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['timePrecision']
		rt = eval(request.data.get('controldict'))['runTimeModifiable'] if isinstance(request.data, QueryDict) else request.data.get('controldict')['runTimeModifiable']

		tm = eval(request.data.get('transportproperties'))['transportModel'] if isinstance(request.data, QueryDict) else request.data.get('transportproperties')['transportModel']
		tmc = eval(request.data.get('transportproperties'))['coeffs'] if isinstance(request.data, QueryDict) else request.data.get('transportproperties')['coeffs']

		sim_type = eval(request.data.get('turbulenceproperties'))['simulationType'] if isinstance(request.data, QueryDict) else request.data.get('turbulenceproperties')['simulationType']
		sim_type_at = eval(request.data.get('turbulenceproperties'))['simulationTypeAttrs'] if isinstance(request.data, QueryDict) else request.data.get('turbulenceproperties')['simulationTypeAttrs']

		ddt = eval(request.data.get('fvschemes'))['ddtSchemes'] if isinstance(request.data, QueryDict) else request.data.get('fvschemes')['ddtSchemes']
		grad = eval(request.data.get('fvschemes'))['gradSchemes'] if isinstance(request.data, QueryDict) else request.data.get('fvschemes')['gradSchemes']
		div = eval(request.data.get('fvschemes'))['divSchemes'] if isinstance(request.data, QueryDict) else request.data.get('fvschemes')['divSchemes']
		laplacian = eval(request.data.get('fvschemes'))['laplacianSchemes'] if isinstance(request.data, QueryDict) else request.data.get('fvschemes')['laplacianSchemes']
		inter = eval(request.data.get('fvschemes'))['interpolationSchemes'] if isinstance(request.data, QueryDict) else request.data.get('fvschemes')['interpolationSchemes']
		sngrad = eval(request.data.get('fvschemes'))['snGradSchemes'] if isinstance(request.data, QueryDict) else request.data.get('fvschemes')['snGradSchemes']
		walld = eval(request.data.get('fvschemes'))['wallDist'] if isinstance(request.data, QueryDict) else request.data.get('fvschemes')['wallDist']

		solvers = eval(request.data.get('fvsolution'))['solvers'] if isinstance(request.data, QueryDict) else request.data.get('fvsolution')['solvers']
		simple = eval(request.data.get('fvsolution'))['SIMPLE'] if isinstance(request.data, QueryDict) else request.data.get('fvsolution')['SIMPLE']
		relax = eval(request.data.get('fvsolution'))['relaxationFactors'] if isinstance(request.data, QueryDict) else request.data.get('fvsolution')['relaxationFactors']

		fieldsAttr = {'p': eval(request.data.get('p')) if isinstance(request.data, QueryDict) else request.data.get('p'), 
		              'u': eval(request.data.get('u')) if isinstance(request.data, QueryDict) else request.data.get('u'), 
		              'k': eval(request.data.get('k')) if isinstance(request.data, QueryDict) else request.data.get('k'), 
		              'nut': eval(request.data.get('nut')) if isinstance(request.data, QueryDict) else request.data.get('nut'), 
		              'epsilon': eval(request.data.get('epsilon')) if isinstance(request.data, QueryDict) else request.data.get('epsilon')}

		case = SimpleFoam(app, sf, st, sa, et, dt,
                          wc, wi, pw, wf, wcp, wp,
                          tf, tp, rt, tm, tmc, sim_type,
                          sim_type_at, ddt, grad,
                          div, laplacian, inter, sngrad, walld, solvers, simple,
                          relax, fieldsAttr,
                          workdir, casename, True)

		r = case.write_case()
		if r['type'] == 1:

			return Response({"Write simplefoam case Status": r['desc']}, status=status.HTTP_200_OK)

class SimpleFoamDeleteView(views.APIView):

	def get(self, request):

		""" 
		get:
		Return a json example to erase case folder.
		"""

		attrs = {
			'workdir': 'workdir', 
                        'casename': 'pitzDaily'}

		return Response(attrs)

	def post(self, request):

		"""
		post:
		Delete folder workdir/casename folder
		"""

		if not request.data:
			return Response({"Delete case Status": 'NOT Deleted', 'desc': 'empty request body'}, status=status.HTTP_200_OK)

		templates_path = os.environ['YouFOAM_TEMPLATES_PATH']
		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		case_dir = workdir + '/' + casename

		if not os.path.exists(workdir):
			return Response({"Delete case Status": 'NOT Deleted', 'desc': 'Work Directory does not exist!'}, status=status.HTTP_200_OK)
		
		shutil.rmtree(case_dir)
		return Response({"Delete case Status": 'Deleted'}, status=status.HTTP_200_OK)


class SimpleFoamExecuteSolverView(views.APIView):
	
	def get(self, request):

		""" 
		get:
		Return a json example to erase case folder.
		"""

		attrs = {'workdir': 'workdir', 
		         'casename': 'pitzDaily', 
		         'file': '/home/rodrigo/projeto_youfoam/youFoam/youfoam/youfoam/core/templates/mesh/rotatingCavity.cas', 
		         'solvername': 'simpleFoam'}

		return Response(attrs)


	def post(self, request):

		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		file = request.data.get('file')
		solvername = request.data.get('solvername')
		conv_type = 1

		r = import_mesh(conv_type, file, workdir, casename, True)

		if r['type'] == 1:
			e = execute_solver(workdir, casename, solvername, '1', True)
			return Response({"Execute Solver Status": e['desc']}, status=status.HTTP_200_OK)
		else:
			return Response({"Convert Mesh Status": r['desc']}, status=status.HTTP_200_OK)
