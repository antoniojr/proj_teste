"""
Documentation for boundary base class
To call help, boundaryBase --help

Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 boundaryBase.py -w workdir -c casename
"""

import argparse
import sys
import os
import re
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class BoundaryBase(CommonPropertiesBase):

    bou_file_name = 'boundary'
    bou_file_location = 'constant/polyMesh'
    bou_file_class = 'polyBoundaryMesh'

    num_attrs = 3
    patch_list = ['movingWall', 'fixedWalls', 'frontAndBack']
    bouattrs = [{'movingWall': {'type': 'wall',
                             'inGroups': '1(wall)',
                             'nFaces': 20,
                             'startFace': 760}
              },
             {'fixedWalls': {'type': 'wall',
                             'inGroups': '1(wall)',
                             'nFaces': 60,
                             'startFace': 780}
              },
             {'frontAndBack': {'type': 'empty',
                               'inGroups': '1(empty)',
                               'nFaces': 800,
                               'startFace': 840}
              }]

    def __init__(self, wd, c, info):
        self.work_dir = wd
        self.case_name = c
        self.info = True if info else False

    def read_boundary_file(self):
        fin = self.work_dir + '/' + self.case_name + '/' + self.bou_file_location + '/' + self.bou_file_name
        boundary_attributes_list = []
        line_counter = 0
        start_patch_list = False
        start_patch_attrs = False
        patches_counter = 0
        curr_patch = ''
        curr_attr = {}
        num_patches = 0
        patch_attrs = {}
        patch_list = []

        try:
            f = open(fin, "r")
            for ln in f:
                line_counter += 1

                if re.match("^\d+.*$", ln):
                    num_patches = int(ln)

                if ln[0] == '(':
                    start_patch_list = True

                if ln[0] == ')':
                    start_patch_list = False

                if start_patch_list:
                    if len(boundary_attributes_list) >= num_patches:
                        break

                    curr_line = ln.strip()

                    if curr_line[0] == '{':
                        start_patch_attrs = True

                    if curr_line[0] == '}':
                        start_patch_attrs = False
                        patch_attrs[curr_patch] = curr_attr
                        boundary_attributes_list.append(patch_attrs)
                        patch_attrs = {}
                        curr_attr = {}

                    if start_patch_attrs:
                        if len(curr_line) > 0 and curr_line not in ['{', '}']:
                            k, v = curr_line.split()
                            curr_attr[k] = v.split(';')[0]

                    else:
                        if len(curr_line) > 0 and curr_line not in ['{', '}', '(', ')']:
                            patches_counter += 1
                            curr_patch = curr_line
                            patch_list.append(curr_patch)

            f.close()
            self.bouattrs = boundary_attributes_list
            self.num_attrs = len(boundary_attributes_list)
            self.patch_list = patch_list

            if self.info:
                print('file read success ' + fin)
            return {'type': 1, 'desc': 'file read success ' + fin}

        except IOError as e:
            if self.info:
                print('cannot read file ' + fin + '. Error: %s' % e.output)
            return {'type': 0, 'desc': 'cannot read file ' + fin + '. Error: %s' % e.output}

    def write_boundary_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.bou_file_location + '/' + self.bou_file_name

        try:
            fo = open(fout, "w")
            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.bou_file_class, self.bou_file_location, self.bou_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            fo.write(str(self.num_attrs) + '\n')
            fo.write('(' + '\n')
            for attr in self.bouattrs:
                fo.write('    ' + list(attr.keys())[0] + '\n')
                fo.write('    {' + '\n')
                for k, v in list(attr.values())[0].items():
                    fo.write('        ' + str(k) + ' ' + str(v) + ';' + '\n')
                fo.write('    }' + '\n')
            fo.write(')' + '\n\n')
            fo.write("// ************************************************************************* //")
            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}

        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def get_dict_attrs(self):
        return self.bouattrs

    def get_patch_attr(self, patch_name):
        for attr in self.bouattrs:
            curr_patch_name = list(attr.keys())[0]
            if curr_patch_name == patch_name:
                return attr[patch_name]

    def get_patch_list(self):
        return self.patch_list

    def updated_dict_attrs(self, new_attrs):
        self.bouattrs = new_attrs

    def insert_new_patch(self, patch_attrs):
        patch_name = list(patch_attrs.keys())[0]
        self.patch_list.append(patch_name)
        self.bouattrs.append(patch_attrs)
        self.num_attrs += 1

    def update_patch_attr(self, patch_name, attr_key, attr_val):
        idx = 0
        for attr in self.bouattrs:
            curr_patch_name = list(attr.keys())[0]
            if curr_patch_name == patch_name:
                self.bouattrs[idx][patch_name][attr_key] = attr_val
                break
            idx += 1

    def update_patch_name(self, curr_name, new_name):
        idx = 0
        temp_attr = {new_name: {}}
        for attr in self.bouattrs:
            curr_patch_name = list(attr.keys())[0]
            if curr_patch_name == curr_name:
                temp_attr[new_name] = self.bouattrs[idx][curr_name]
                del self.bouattrs[idx]
                break
            idx += 1
        self.patch_list.remove(curr_name)
        self.patch_list.append(new_name)
        self.bouattrs.append(temp_attr)


"""Main function to call boundaryBase method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='boundary_base help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    boundary = BoundaryBase(args.workdir, args.case, args.info)

    boundary.read_boundary_file()
    boundary.write_boundary_dict_file()

