from unittest import TestCase
from src.base.fvSolutionBase import FvSolutionBase
from applications.utilities.generateMesh import generate_mesh
from applications.utilities.executeSolver import execute_solver
import os
import shutil


class TestFvSolutionBase(TestCase):

    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    LOG = '1'
    SOLVERNAME = 'simpleFoam'

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASENAME
    source_dir = templates_path + '/' + CASENAME

    def test_write_dict_file(self):
        print('******************fvSolution: test_write_dict_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        fvsolution = FvSolutionBase(None,
                                    None,
                                    None,
                                    self.WORKDIR,
                                    self.CASENAME,
                                    self.INFO)
        r = fvsolution.write_fvsolution_dict_file()
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')

    def test_call_solver_ok(self):
        print('*****************fvSolution: test_call_solver_ok**************************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)
        fvsolution = FvSolutionBase(None,
                                    None,
                                    None,
                                    self.WORKDIR,
                                    self.CASENAME,
                                    self.INFO)

        equationsAttrs = {'U': '0.9',
                          'k': '0.9',
                          'epsilon': '0.9',
                          'omega': '0.9',
                          'f': '0.9',
                          'v2': '0.9'}

        fieldsAttrs = {'p': '0.9'}

        relaxFAttrs = {'equations': equationsAttrs,
                       'fields': fieldsAttrs}

        fvsolution.update_relax_factors_attrs(relaxFAttrs)
        fv = fvsolution.write_fvsolution_dict_file()
        if fv['type'] == 1 and m['type'] == 1:
            print('executing solver!!!')
            r = execute_solver(self.WORKDIR, self.CASENAME, self.SOLVERNAME, self.LOG, self.INFO)
            self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('***************************************************************************** \n')
