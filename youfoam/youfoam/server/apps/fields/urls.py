from django.urls import path
from . import views

urlpatterns = [
    path('fields/create/', views.FieldsCreateView.as_view()),
    path('fields/edit/<workdir>/<casename>/<simulation_type>/<simulation_type_model>/', views.FieldsCreateView.as_view())
]