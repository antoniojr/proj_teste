"""
Documentation for transport properties base class
To call help, transportPropertiesBase --help

Set info flag -i/--info for print output. You can check for error reasons

Return a value that can be:
         1: OK -> transportProperties created

         0: NOK -> transportProperties creating error!!

To call from command line:
python3 transportPropertiesBase.py -w workdir -c casename -transportModel Newtonian -i "creation success"
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class TransportPropertiesBase(CommonPropertiesBase):

    tp_file_name = 'transportProperties'
    tp_file_location = 'constant'
    tp_file_class = 'dictionary'

    crossPowerLawCoeffs = {'nu0 [0 2 -1 0 0 0 0]': '0.01',
                           'nuInf [0 2 -1 0 0 0 0]': '10',
                           'm [0 0 1 0 0 0 0]': '0.4',
                           'n [0 0 0 0 0 0 0]': '3'}

    newtonian = {'nu [0 2 -1 0 0 0 0]': '1e-05'}

    tpattrs = {'transportModel': 'Newtonian'}

    def __init__(self, tm, tmc, wd, c, info):
        self.tpattrs['transportModel'] = tm if tm else self.tpattrs['transportModel']
        if self.tpattrs['transportModel'] == 'Newtonian':
            self.tpattrs[list(self.newtonian.keys())[0]] = tmc if tmc else list(self.newtonian.values())[0]
        else:
            self.tpattrs[self.tpattrs['transportModel'] + 'Coeffs'] = tmc if tmc else self.crossPowerLawCoeffs
        self.work_dir = wd
        self.case_name = c
        self.info = True if info else False

    def write_transport_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.tp_file_location + '/' + self.tp_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.file_class, self.tp_file_location, self.tp_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            self.write_dict(fo, self.tpattrs, '')
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}
        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def get_dict_attrs(self):
        return self.tpattrs

    def updated_dict_attrs(self, new_attrs):
        self.tpattrs = new_attrs

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str:
                if v.find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table


"""Main function to call transport_dict_base method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='transport_dict_base help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-tm", "--transportModel", dest="transportModel", help="transportModel")
    parser.add_argument("-tmc", "--transportModelCoeffs", dest="transportModelCoeffs", help="transportModelCoeffs")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    transportProperties = TransportPropertiesBase(args.transportModel, args.transportModelCoeffs, args.workdir,
                                                  args.case, args.info)

    transportProperties.write_transport_dict_file()

