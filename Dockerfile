FROM wikkibrasil/wbrdev_ubuntu18.04x64-of1812:v01
MAINTAINER wikkibrasil

EXPOSE 7000

RUN mkdir youFoam-web
COPY youFoam /home/root/youFoam

WORKDIR /home/root/youFoam

# Atualizar a distribuição ubuntu
RUN apt-get update
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get install -y --no-install-recommends apt-utils && \
    apt-get -y upgrade

# Installing Python3
RUN apt install -y python3 && \
    apt install -y python3-pip && \
    apt install -y doxygen && \
    apt install -y python3-pydot && \
    apt install -y python-pydot-ng && \
    apt install -y graphviz

# Installing MongoDB
RUN apt install -y libcurl4 && \
    apt install -y openssl && \
    apt install -y mongodb

# Set mongodb daemon
ENTRYPOINT service mongodb start && /bin/bash

# Installing virtualenv
RUN pip3 install virtualenv

SHELL ["/bin/bash", "-c"] 
RUN source /home/root/youFoam/youfoam/youfoam/core/etc/bashrc
RUN virtualenv -p /usr/bin/python3.6 venv
RUN source /home/root/youFoam/venv/bin/activate && \
    pip install -r requirements.txt

# Change folder owner permissions
# RUN sudo chown -R wikkibr:wikkibr /home/wikkibr/youFoam-web
