import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from controlDictBase import ControlDictBase
import shutil

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render, redirect
#from rest_framework.decorators import api_view

#from .serializers import ControlDictSerializer
class ControlDict():
	def index(request, workdir, casename):

		control_dict = ControlDictBase(None, None, None, None, None, None, None, None, 
			None, None, "on", None, None, None, None, workdir, casename, True)

		controldict_data = control_dict.read_controldict_file()

		controldict_data['workdir'] = workdir
		controldict_data['casename'] = casename		

		return render(request, 'controldict.html', controldict_data)


class ControlDictCreateView(views.APIView):

	def get(self, request):

		""" 
		get:
		Return a list of all controdlIdct attributes.
		"""
		
		control_dict_attrs = {
			  'application': 'simpleFoam', 
			  'startFrom': 'startTime',
		          'startTime': '0',
              		  'stopAt': 'endTime',
		          'endTime': '2000',
                          'deltaT': '1',
                          'writeControl': 'timeStep',
                          'writeInterval': '100',
                          'purgeWrite': '0',
              		  'writeFormat': 'ascii',
                          'writeCompression': 'off',
                          'writePrecision': '6',
                          'timeFormat': 'general',
                          'timePrecision': '6',
                          'runTimeModifiable': 'true', 
			  'workdir': 'workdir', 
			  'casename': 'pitzDaily', 
                          'show_output': 'true'}


		return Response(control_dict_attrs)

	def post(self, request):

		"""
		post:
		Create controldict file in workdir/casename/system folder
		"""
		templates_path = os.environ['YouFOAM_TEMPLATES_PATH']
		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		#case_dir = workdir + '/' + casename
		#source_dir = templates_path + '/' + 'blank_case'
		#shutil.copytree(source_dir, case_dir)

		application = request.data.get('application')
		startFrom = request.data.get('startFrom')
		startTime = str(request.data.get('startTime'))
		stopAt = request.data.get('stopAt')
		endTime = str(request.data.get('endTime'))
		deltaT = str(request.data.get('deltaT'))
		writeControl = request.data.get('writeControl')
		writeInterval = str(request.data.get('writeInterval'))
		purgeWrite = str(request.data.get('purgeWrite'))
		writeFormat = request.data.get('writeFormat')
		writeCompression = str(request.data.get('writeCompression'))
		writePrecision = str(request.data.get('writePrecision'))
		timeFormat = request.data.get('timeFormat')
		timePrecision = str(request.data.get('timePrecision'))
		runTimeModifiable = str(request.data.get('runTimeModifiable'))

		control_dict = ControlDictBase(application, startFrom, startTime, stopAt, endTime, deltaT, writeControl, writeInterval,
                                       purgeWrite, writeFormat, writeCompression, writePrecision, timeFormat, timePrecision, runTimeModifiable, workdir, casename, True)

		control_dict_attrs = control_dict.get_dict_attrs()

		if 'workdir' in control_dict_attrs or 'casename' in control_dict_attrs:
			del control_dict_attrs['workdir']
			del control_dict_attrs['casename']
			control_dict.updated_dict_attrs(control_dict_attrs)

		r = control_dict.write_control_dict_file()
		
		#return Response({"Write ControlDict File Status": r['desc']}, status=status.HTTP_200_OK)
		# FIXME: Refazendo Workflow. return redirect("/api/fvsolution/index/{}/{}/".format(workdir, casename))
		return redirect("/api/solver/edit/{}/{}/".format(workdir, casename))


class ControlDictDeleteView(views.APIView):

	def get(self, request):
		""" 
		get:
		Return a json example to erase case folder.
		"""

		attrs = {
			'workdir': 'workdir', 
                        'casename': 'pitzDaily'}

		return Response(attrs)

	def post(self, request):

		"""
		post:
		Delete folder workdir/casename folder
		"""

		if not request.data:
			return Response({"Delete case Status": 'NOT Deleted', 'desc': 'empty request body'}, status=status.HTTP_200_OK)

		templates_path = os.environ['YouFOAM_TEMPLATES_PATH']
		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		case_dir = workdir + '/' + casename

		if not os.path.exists(workdir):
			return Response({"Delete case Status": 'NOT Deleted', 'desc': 'Work Directory does not exist!'}, status=status.HTTP_200_OK)
		
		shutil.rmtree(case_dir)
		return Response({"Delete case Status": 'Deleted'}, status=status.HTTP_200_OK)
		

