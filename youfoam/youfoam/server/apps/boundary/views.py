import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from boundaryBase import BoundaryBase
import shutil

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render, redirect

class Boundary():
	def index(request, workdir, casename):

		bou = BoundaryBase(workdir, casename, True)
		bou.read_boundary_file()
		bou_data_list = bou.get_dict_attrs()

		bou_data_dicts = {}
		counter = 0
		for data in bou_data_list:
			data_key = list(data.keys())[0]
			data_val = list(data.values())[0]
			bou_data_dict = {}
			bou_data_dict['name'] = data_key
			bou_data_dict['type'] = data_val['type']
			bou_data_dicts['boundary' + str(counter)] = bou_data_dict
			counter += 1

		bou_data_dicts['workdir'] = workdir
		bou_data_dicts['casename'] = casename
		return render(request, 'boundary.html', {'boundaries': bou_data_dicts})


class BoundaryCreateView(views.APIView):

	def post(self, request):

		workdir = request.data.get('workdir')
		casename = request.data.get('casename')

		boundary = BoundaryBase(workdir, casename, True)
		boundary.read_boundary_file()

		attrs = boundary.get_dict_attrs()
		patchs = boundary.get_patch_list()		

		for k,v in request.data.items():
			if 'BoundaryNameOld' in k:
				idx = patchs.index(v)
				new_k = k.replace('Old', 'New')
				new_t = new_k.replace('Name', 'Type')
				attrs[idx][ request.data.get(new_k) ] = attrs[idx].pop(v)
				attrs[idx][ request.data.get(new_k) ]['type'] = request.data.get(new_t)

		boundary.updated_dict_attrs(attrs)
		r = boundary.write_boundary_dict_file()

		#return redirect("/api/controldict/index/{}/{}/".format(workdir, casename))
		return redirect("/api/turbulenceproperties/simtypeedit/{}/{}/".format(workdir, casename))
