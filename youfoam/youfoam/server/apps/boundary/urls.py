from django.urls import path
from . import views

urlpatterns = [
    path('boundary/index/<workdir>/<casename>/', views.Boundary.index),
    path('boundary/create/', views.BoundaryCreateView.as_view())
]