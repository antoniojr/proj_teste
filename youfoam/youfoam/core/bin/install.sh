#!/bin/sh

echo "Installing Python3, pip3 e virtualenv"
apt update
apt install python3
apt install python3-pip
pip3 install virtualenv


echo "Installing Doxygen"
apt install doxygen

echo "Installing mongoDB"
apt update
apt install mongodb libcurl4 openssl
systemctl start mongodb


echo "Installing pydot for doxygen diagram"
apt install python3-pydot python-pydot-ng graphviz
