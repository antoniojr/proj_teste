"""
Documentation for common properties base class
To call help, commonPropertiesBase --help
"""


class CommonPropertiesBase(object):

    of_version = '4.x'
    file_name = 'controlDict'
    file_location = 'system'
    file_class = 'dictionary'
    file_format = 'ascii'
    file_version = '2.0'
    work_dir = 'workdir'
    case_name = 'test'
    info = True

    file_header = """/*--------------------------------*- C++ -*----------------------------------*\\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  {}                                   |
|   \\  /    A nd           | Web:      www.OpenFOAM.org                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/"""

    foam_file = """
FoamFile
{{
    version     2.0;
    format      ascii;
    class       {};
    location    "{}";
    object      {};
}}"""

