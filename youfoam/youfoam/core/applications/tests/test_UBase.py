from unittest import TestCase
from src.base.uBase import UBase
from applications.utilities.generateMesh import generate_mesh
from applications.utilities.executeSolver import execute_solver
import os
import shutil


class TestUBase(TestCase):

    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    LOG = '1'
    SOLVERNAME = 'simpleFoam'

    BOUNDARYFIELD = {'rotor':
                         {'type': 'movingWallVelocity', 'value': 'uniform (0 0 0)'},
                     'stator':
                         {'type': 'movingWallVelocity', 'value': 'uniform (0 0 0)'},
                     'inlet':
                         {'type': 'flowRateInletVelocity', 'massFlowRate': 'table (0 0),(1e-1 7.300)'}
                     }

    BOUNDARYFIELD2 = {'inlet':
                         {'type': 'fixedValue', 'value': 'uniform (10 0 0)'},
                      'outlet':
                         {'type': 'zeroGradient'},
                      'upperWall':
                         {'type': 'noSlip'},
                      'lowerWall':
                          {'type': 'noSlip'},
                      'frontAndBack':
                          {'type': 'empty'}
                      }

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASENAME
    source_dir = templates_path + '/' + CASENAME

    def test_write_dict_file(self):
        print('******************UBase: test_write_dict_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)

        u = UBase(None,
                  None,
                  None,
                  self.WORKDIR,
                  self.CASENAME,
                  self.INFO)

        r = u.write_u_dict_file()
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')

    def test_call_solver(self):
        print('*****************UBase: test_call_solver_ok**************************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)

        u = UBase(None,
                  None,
                  self.BOUNDARYFIELD2,
                  self.WORKDIR,
                  self.CASENAME,
                  self.INFO)

        u.file_class = 'volVectorField'

        r = u.write_u_dict_file()

        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)

        if r['type'] == 1 and m['type'] == 1:
            print('executing solver!!!')
            r = execute_solver(self.WORKDIR, self.CASENAME, self.SOLVERNAME, self.LOG, self.INFO)
            self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('***************************************************************************** \n')
