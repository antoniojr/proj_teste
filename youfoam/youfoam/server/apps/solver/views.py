import sys
import os
sys.path.append(os.environ['YouFOAM_SOLVERS_PATH'])
from executeSolver import execute_solver
import shutil
import json
from django.http import QueryDict

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render, redirect

class SolverExecuteView(views.APIView):
	
	def get(self, request, workdir, casename):

		case_data = {}
		case_data['workdir'] = workdir
		case_data['casename'] = casename
		case_data['solvername'] = 'simpleFoam'
		
		return render(request, 'solver.html', case_data)


	def post(self, request):

		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		solvername = request.data.get('solvername')
		
		case_data={}
		case_data['workdir'] = workdir
		case_data['casename'] = casename
		case_data['solvername'] = solvername
		
		e = execute_solver(workdir, casename, solvername, '1', True)
		case_data['status'] = e['desc']

		return render(request, 'solver.html', case_data)
