from django.urls import path
from . import views

urlpatterns = [
    path('solver/execute/', views.SolverExecuteView.as_view()),
    path('solver/edit/<workdir>/<casename>/', views.SolverExecuteView.as_view()),
]
