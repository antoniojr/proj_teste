from django.test import TestCase, Client
from rest_framework import status
import shutil
import os

# Create your tests here.

class ImportMeshTests(TestCase):
	def test_import_mesh(self):
		client = Client()
		file = 'rotatingCavity.cas'
		workdir = 'workdir'
		casename = 'pitzDaily'
		data = {'workdir': workdir, 'casename': casename, 'createCase': ''}
		#data = {'type': 1, 'meshfile': file, 'workdir': 'workdir', 'casename': 'pitzDaily'}
		case_path = workdir + '/' + casename
		if os.path.exists(case_path):
			shutil.rmtree(case_path, ignore_errors=True)
		response = client.post('/api/createcase/create/', data)
		data['type'] = 1
		data['meshfile'] = file
		data['convertmesh'] = ''
		response = client.post('/api/importmesh/convert/', data)
		self.assertEqual(response.status_code, status.HTTP_302_FOUND)
		#if os.path.exists(case_path):
		#	shutil.rmtree(case_path, ignore_errors=True)

	def test_delete_case(self):
		client = Client()
		workdir = 'workdir'
		casename = 'pitzDaily'
		case_path = workdir + '/' + casename
		data = {'workdir': workdir, 'casename': casename, 'createCase': ''}
		if not os.path.exists(case_path):
			response = client.post('/api/createcase/create/', data)
		response = client.post('/api/importmesh/delete/', data)
		self.assertEqual(response.status_code, status.HTTP_200_OK)
		if os.path.exists(case_path):
			shutil.rmtree(case_path, ignore_errors=True)