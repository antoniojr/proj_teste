"""
Documentation for fvSolution base class
To call help, fvSolutionBase --help

Set info flag -i/--info for print output. You can check for error reasons

Return a value that can be:
         1: OK -> fvSolution created

         0: NOK -> fvSolution create error!!

To call from command line (Example):
python3 fvSolutionBase.py -w workdir -c casename -i "creation success"
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class FvSolutionBase(CommonPropertiesBase):

    fvs_file_name = 'fvSolution'
    fvs_file_location = 'system'
    fvs_file_class = 'dictionary'

    pAttrs = {'solver': 'GAMG',
              'tolerance': '1e-06',
              'relTol': '0.1',
              'smoother': 'GaussSeidel'}

    UAttrs = {'solver': 'smoothSolver',
              'smoother': 'symGaussSeidel',
              'tolerance': '1e-05',
              'relTol': '0.1'}

    kAttrs = {'solver': 'smoothSolver',
              'smoother': 'symGaussSeidel',
              'tolerance': '1e-05',
              'relTol': '0.1'}

    epsilonAttrs = {'solver': 'smoothSolver',
                    'smoother': 'symGaussSeidel',
                    'tolerance': '1e-05',
                    'relTol': '0.1'}

    omegaAttrs = {'solver': 'smoothSolver',
                  'smoother': 'symGaussSeidel',
                  'tolerance': '1e-05',
                  'relTol': '0.1'}

    fAttrs = {'solver': 'smoothSolver',
              'smoother': 'symGaussSeidel',
              'tolerance': '1e-05',
              'relTol': '0.1'}

    v2Attrs = {'solver': 'smoothSolver',
               'smoother': 'symGaussSeidel',
               'tolerance': '1e-05',
               'relTol': '0.1'}

    '''solversAttrs = {'p': pAttrs,
                    'U': UAttrs,
                    'k': kAttrs,
                    'epsilon': epsilonAttrs,
                    'omega': omegaAttrs}
                    'f': fAttrs,
                    'v2': v2Attrs}'''

    # Bye simulation_type_model kEpsilon
    solversAttrs_epsilon = {'p': pAttrs,
                            'U': UAttrs,
                            'k': kAttrs,
                            'epsilon': epsilonAttrs}      

    # Bye simulation_type_model kOmega e kOmegaSST
    solversAttrs_omegas = {'p': pAttrs,
                           'U': UAttrs,
                           'k': kAttrs,
                           'omega': omegaAttrs}                                          

    # Bye SimulationType
    solversAttrs_laminar = {'p': pAttrs,
                            'U': UAttrs}

    '''residualControlAttrs = {'p': '1e-2',
                            'U': '1e-3',
                            'k': '1e-3',
                            'epsilon': '1e-3',
                            'omega': '1e-3',
                            'f': '1e-3',
                            'v2': '1e-3'}'''

    residualControlAttrs_epsilon = {'p': '1e-2',
                                    'U': '1e-3',
                                    'k': '1e-3',
                                    'epsilon': '1e-3'}

    residualControlAttrs_omegas = {'p': '1e-2',
                                   'U': '1e-3',
                                   'k': '1e-3',
                                   'omega': '1e-3'}                         

    residualControlAttrs_laminar = {'p': '1e-2',
                                    'U': '1e-3'}

    '''simpleAttrs = {'nNonOrthogonalCorrectors': '0',
                   'consistent': 'yes',
                   'residualControl': residualControlAttrs}'''

    simpleAttrs_epsilon = {'nNonOrthogonalCorrectors': '0',
                           'consistent': 'yes',
                           'residualControl': residualControlAttrs_epsilon}      

    simpleAttrs_omegas = {'nNonOrthogonalCorrectors': '0',
                          'consistent': 'yes',
                          'residualControl': residualControlAttrs_omegas}                                            

    simpleAttrs_laminar = {'nNonOrthogonalCorrectors': '0',
                           'consistent': 'yes',
                           'residualControl': residualControlAttrs_laminar}

    '''equationsAttrs = {'U': '0.9',
                      'k': '0.9',
                      'epsilon': '0.9',
                      'omega': '0.9'}
                      'f': '0.9',
                      'v2': '0.9'}'''

    equationsAttrs_epsilon = {'U': '0.9',
                              'k': '0.9',
                              'epsilon': '0.9'}      

    equationsAttrs_omegas = {'U': '0.9',
                             'k': '0.9',
                             'omega': '0.9'}                                       

    equationsAttrs_laminar = {'U': '0.9'}

    fieldsAttrs = {'p': '0.9'}

    '''relaxFAttrs = {'equations': equationsAttrs,
                   'fields': fieldsAttrs}'''

    relaxFAttrs_epsilon = {'equations': equationsAttrs_epsilon,
                           'fields': fieldsAttrs}

    relaxFAttrs_omegas = {'equations': equationsAttrs_omegas,
                          'fields': fieldsAttrs}                                      

    relaxFAttrs_laminar = {'equations': equationsAttrs_laminar,
                           'fields': fieldsAttrs}

    fvs_attrs_epsilon = {'solvers': solversAttrs_epsilon,
                         'SIMPLE': simpleAttrs_epsilon,
                         'relaxationFactors': relaxFAttrs_epsilon}

    fvs_attrs_omegas = {'solvers': solversAttrs_omegas,
                        'SIMPLE': simpleAttrs_omegas,
                        'relaxationFactors': relaxFAttrs_omegas}                         

    fvs_attrs_laminar = {'solvers': solversAttrs_laminar,
                         'SIMPLE': simpleAttrs_laminar,
                         'relaxationFactors': relaxFAttrs_laminar}

    def __init__(self, solvers, simple, relax, wd, c, st, stm, info):
        if st.strip().upper() == 'RAS':
            if stm.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                self.fvs_attrs_epsilon['solvers'] = solvers if solvers else self.fvs_attrs_epsilon['solvers']
                self.fvs_attrs_epsilon['SIMPLE'] = simple if simple else self.fvs_attrs_epsilon['SIMPLE']
                self.fvs_attrs_epsilon['relaxationFactors'] = relax if relax else self.fvs_attrs_epsilon['relaxationFactors']
            # elif stm.strip() in ['kOmega', 'kOmegaSST', 'kOmegaSSTSAS']:
            else:
                self.fvs_attrs_omegas['solvers'] = solvers if solvers else self.fvs_attrs_omegas['solvers']
                self.fvs_attrs_omegas['SIMPLE'] = simple if simple else self.fvs_attrs_omegas['SIMPLE']
                self.fvs_attrs_omegas['relaxationFactors'] = relax if relax else self.fvs_attrs_omegas['relaxationFactors']
        else:
            self.fvs_attrs_laminar['solvers'] = solvers if solvers else self.fvs_attrs_laminar['solvers']
            self.fvs_attrs_laminar['SIMPLE'] = simple if simple else self.fvs_attrs_laminar['SIMPLE']
            self.fvs_attrs_laminar['relaxationFactors'] = relax if relax else self.fvs_attrs_laminar['relaxationFactors']
        self.work_dir = wd
        self.case_name = c
        self.simtype = st
        self.simtypemode = stm
        self.info = True if info else False

    def write_fvsolution_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.fvs_file_location + '/' + self.fvs_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.fvs_file_class, self.fvs_file_location, self.fvs_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            if self.simtype.strip().upper() == 'RAS':
                if self.simtypemode.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                    self.write_dict(fo, self.fvs_attrs_epsilon, '')
                else:
                    self.write_dict(fo, self.fvs_attrs_omegas, '')
            else:
                self.write_dict(fo, self.fvs_attrs_laminar, '')
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}
        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def get_dict_attrs(self, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                return self.fvs_attrs_epsilon
            else:
                return self.fvs_attrs_omegas
        else:
            return self.fvs_attrs_laminar

    def updated_dict_attrs(self, new_attrs, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                self.fvs_attrs_epsilon = new_attrs
            else:
                self.fvs_attrs_omegas = new_attrs
        else:
            self.fvs_attrs_laminar = new_attrs

    def get_solvers_attrs(self, simulation_type, simulation_type_model):        
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                return self.solversAttrs_epsilon
            else:
                return self.solversAttrs_omegas
        else:
            return self.solversAttrs_laminar        

    def get_simple_attrs(self, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                return self.simpleAttrs_epsilon
            else:
                return self.simpleAttrs_omegas
        else:
            return self.simpleAttrs_laminar         

    def get_relax_factors_attrs(self, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                return self.relaxFAttrs_epsilon
            else:
                return self.relaxFAttrs_omegas
        else:
            return self.relaxFAttrs_laminar          

    def update_solvers_attrs(self, new_attrs, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                self.solversAttrs_epsilon = new_attrs
                self.fvs_attrs_epsilon['solvers'] = self.solversAttrs_epsilon
            else:
                self.solversAttrs_omegas = new_attrs
                self.fvs_attrs_omegas['solvers'] = self.solversAttrs_omegas                
        else:
            self.solversAttrs_laminar = new_attrs
            self.fvs_attrs_laminar['solvers'] = self.solversAttrs_laminar


    def update_simple_attrs(self, new_attrs, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            self.simpleAttrs = new_attrs
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                self.fvs_attrs_epsilon['SIMPLE'] = self.simpleAttrs
            else:
                self.fvs_attrs_omegas['SIMPLE'] = self.simpleAttrs
        else:
            self.simpleAttrs_laminar = new_attrs
            self.fvs_attrs_laminar['SIMPLE'] = self.simpleAttrs_laminar

    def update_relax_factors_attrs(self, new_attrs, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            self.relaxFAttrs = new_attrs
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                self.fvs_attrs_epsilon['relaxationFactors'] = self.relaxFAttrs
            else:
                self.fvs_attrs_omegas['relaxationFactors'] = self.relaxFAttrs
        else:
            self.relaxFAttrs_laminar = new_attrs
            self.fvs_attrs_laminar['relaxationFactors'] = self.relaxFAttrs_laminar

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str:
                if v.find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table

"""Main function to call fvSolution base method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='turbulence_dict_base help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-solvers", "--solvers", dest="solvers", help="solvers")
    parser.add_argument("-simple", "--simple", dest="simple", help="simple")
    parser.add_argument("-rel", "--relaxationFactors", dest="relaxationFactors", help="relaxationFactors")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    parser.add_argument("-st", "--simulationType", dest="simulationType", help="simulationType")
    parser.add_argument("-stm", "--simulationTypeModel", dest="simulationTypeModel", help="simulationTypeModel")
    args = parser.parse_args()
    fvsolution = FvSolutionBase(args.solvers,
                                args.simple,
                                args.relaxationFactors,
                                args.workdir,
                                args.case,
                                args.simulationType,
                                args.simulationTypeModel,
                                args.info)

    fvsolution.write_fvsolution_dict_file()
