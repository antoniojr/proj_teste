"""
Documentation for fvSchemes base class
To call help, fvSchemesBase --help

Set info flag -i/--info for print output. You can check for error reasons

Return a value that can be:
         1: OK -> fvSchemes created

         0: NOK -> fvSchemes create error!!

To call from command line (Example):
python3 fvSchemesBase.py -w workdir -c casename -i "creation success"
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class FvSchemesBase(CommonPropertiesBase):

    fch_file_name = 'fvSchemes'
    fch_file_location = 'system'
    fch_file_class = 'dictionary'

    ddtSchemesAttrs = {'default': 'steadyState'}

    gradSchemesAttrs = {'default': 'Gauss linear'}

    '''divSchemesAttrs = {'default': 'none',
                       'div(phi,U)': 'bounded Gauss linearUpwind grad(U)',
                       'div(phi,k)': 'bounded Gauss limitedLinear 1',
                       'div(phi,epsilon)': 'bounded Gauss limitedLinear 1',
                       'div(phi,omega)': 'bounded Gauss limitedLinear 1',
                       'div(phi,v2)': 'bounded Gauss limitedLinear 1',
                       'div((nuEff*dev2(T(grad(U)))))': 'Gauss linear',
                       'div(nonlinearStress)': 'Gauss linear'}'''

    divSchemesAttrs_epsilon = {'div(phi,U)': 'bounded Gauss linearUpwind grad(U)',
                               'div(phi,k)': 'bounded Gauss limitedLinear 1',
                               'div(phi,epsilon)': 'bounded Gauss limitedLinear 1',
                               'div((nuEff*dev2(T(grad(U)))))': 'Gauss linear'}

    divSchemesAttrs_omegas = {'div(phi,U)': 'bounded Gauss linearUpwind grad(U)',
                              'div(phi,k)': 'bounded Gauss limitedLinear 1',
                              'div(phi,omega)': 'bounded Gauss limitedLinear 1',
                              'div((nuEff*dev2(T(grad(U)))))': 'Gauss linear'}                                     

    divSchemesAttrs_laminar = {'div(phi,U)': 'bounded Gauss linearUpwind grad(U)',
                               'div((nuEff*dev2(T(grad(U)))))': 'Gauss linear'}

    laplacianSchemesAttrs = {'default': 'Gauss linear corrected'}

    interpolationSchemesAttrs = {'default': 'linear'}

    snGradSchemesAttrs = {'default': 'corrected'}

    wallDistAttrs = {'method': 'meshWave'}

    '''fchattrs = {'ddtSchemes': ddtSchemesAttrs,
                'gradSchemes': gradSchemesAttrs,
                'divSchemes': divSchemesAttrs,
                'laplacianSchemes': laplacianSchemesAttrs,
                'interpolationSchemes': interpolationSchemesAttrs,
                'snGradSchemes': snGradSchemesAttrs,
                'wallDist': wallDistAttrs}'''

    fchattrs_epsilon = {'ddtSchemes': ddtSchemesAttrs,
                        'gradSchemes': gradSchemesAttrs,
                        'divSchemes': divSchemesAttrs_epsilon,
                        'laplacianSchemes': laplacianSchemesAttrs,
                        'interpolationSchemes': interpolationSchemesAttrs,
                        'snGradSchemes': snGradSchemesAttrs,
                        'wallDist': wallDistAttrs}    

    fchattrs_omegas = {'ddtSchemes': ddtSchemesAttrs,
                        'gradSchemes': gradSchemesAttrs,
                        'divSchemes': divSchemesAttrs_omegas,
                        'laplacianSchemes': laplacianSchemesAttrs,
                        'interpolationSchemes': interpolationSchemesAttrs,
                        'snGradSchemes': snGradSchemesAttrs,
                        'wallDist': wallDistAttrs}                                       

    fchattrs_laminar = {'ddtSchemes': ddtSchemesAttrs,
                         'gradSchemes': gradSchemesAttrs,
                         'divSchemes': divSchemesAttrs_laminar,
                         'laplacianSchemes': laplacianSchemesAttrs,
                         'interpolationSchemes': interpolationSchemesAttrs,
                         'snGradSchemes': snGradSchemesAttrs,
                         'wallDist': wallDistAttrs}

    def __init__(self, ddt, grad, div, laplacian, inter, sngrad, walld, wd, c, st, stm, info):
        if st.strip().upper() == 'RAS':
            if stm.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                self.fchattrs_epsilon['ddtSchemes'] = ddt if ddt else self.fchattrs_epsilon['ddtSchemes']
                self.fchattrs_epsilon['gradSchemes'] = grad if grad else self.fchattrs_epsilon['gradSchemes']
                self.fchattrs_epsilon['divSchemes'] = div if div else self.fchattrs_epsilon['divSchemes']
                self.fchattrs_epsilon['laplacianSchemes'] = laplacian if laplacian else self.fchattrs_epsilon['laplacianSchemes']
                self.fchattrs_epsilon['interpolationSchemes'] = inter if inter else self.fchattrs_epsilon['interpolationSchemes']
                self.fchattrs_epsilon['snGradSchemes'] = sngrad if sngrad else self.fchattrs_epsilon['snGradSchemes']
                self.fchattrs_epsilon['wallDist'] = walld if walld else self.fchattrs_epsilon['wallDist']
            #elif stm.strip() in ['kOmega', 'kOmegaSST', 'kOmegaSSTSAS']:
            else:
                self.fchattrs_omegas['ddtSchemes'] = ddt if ddt else self.fchattrs_omegas['ddtSchemes']
                self.fchattrs_omegas['gradSchemes'] = grad if grad else self.fchattrs_omegas['gradSchemes']
                self.fchattrs_omegas['divSchemes'] = div if div else self.fchattrs_omegas['divSchemes']
                self.fchattrs_omegas['laplacianSchemes'] = laplacian if laplacian else self.fchattrs_omegas['laplacianSchemes']
                self.fchattrs_omegas['interpolationSchemes'] = inter if inter else self.fchattrs_omegas['interpolationSchemes']
                self.fchattrs_omegas['snGradSchemes'] = sngrad if sngrad else self.fchattrs_omegas['snGradSchemes']
                self.fchattrs_omegas['wallDist'] = walld if walld else self.fchattrs_omegas['wallDist']                
        else:
            self.fchattrs_laminar['ddtSchemes'] = ddt if ddt else self.fchattrs_laminar['ddtSchemes']
            self.fchattrs_laminar['gradSchemes'] = grad if grad else self.fchattrs_laminar['gradSchemes']
            self.fchattrs_laminar['divSchemes'] = div if div else self.fchattrs_laminar['divSchemes']
            self.fchattrs_laminar['laplacianSchemes'] = laplacian if laplacian else self.fchattrs_laminar['laplacianSchemes']
            self.fchattrs_laminar['interpolationSchemes'] = inter if inter else self.fchattrs_laminar['interpolationSchemes']
            self.fchattrs_laminar['snGradSchemes'] = sngrad if sngrad else self.fchattrs_laminar['snGradSchemes']
            self.fchattrs_laminar['wallDist'] = walld if walld else self.fchattrs_laminar['wallDist']

        self.work_dir = wd
        self.case_name = c
        self.simtype = st
        self.simtypemode = stm
        self.info = True if info else False

    def write_fvschemes_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.fch_file_location + '/' + self.fch_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.fch_file_class, self.fch_file_location, self.fch_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            if self.simtype.strip().upper() == 'RAS':
                if self.simtypemode.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                    self.write_dict(fo, self.fchattrs_epsilon, '')
                else:
                    self.write_dict(fo, self.fchattrs_omegas, '')
            else:
                self.write_dict(fo, self.fchattrs_laminar, '')
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}
        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def get_dict_attrs(self, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                return self.fchattrs_epsilon
            else:
                return self.fchattrs_omegas
        else:
            return self.fchattrs_laminar

    def updated_dict_attrs(self, new_attrs, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                self.fchattrs_epsilon = new_attrs
            else:
                self.fchattrs_omegas = new_attrs
        else:
            self.fchattrs_laminar = new_attrs

    def get_ddt_schemes_attrs(self):
        return self.ddtSchemesAttrs

    def get_grad_schemes_attrs(self):
        return self.gradSchemesAttrs

    def get_div_schemes_attrs(self, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                return self.divSchemesAttrs_epsilon
            else:
                return self.divSchemesAttrs_omegas
        else:
            return self.divSchemesAttrs_laminar

    def get_laplacian_schemes_attrs(self):
        return self.laplacianSchemesAttrs

    def get_interpolation_schemes_attrs(self):
        return self.interpolationSchemesAttrs

    def get_sngrad_schemes_attrs(self):
        return self.snGradSchemesAttrs

    def get_wall_dist_attrs(self):
        return self.wallDistAttrs

    def update_ddt_dict_attrs(self, new_attrs, simulation_type, simulation_type_model):
        self.ddtSchemesAttrs = new_attrs
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                self.fchattrs_epsilon['ddtSchemes'] = new_attrs
            else:
                self.fchattrs_omegas['ddtSchemes'] = new_attrs
        else:
            self.fchattrs_laminar['ddtSchemes'] = new_attrs

    def update_grad_dict_attrs(self, new_attrs, simulation_type, simulation_type_model):
        self.gradSchemesAttrs = new_attrs
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                self.fchattrs_epsilon['gradSchemes'] = new_attrs
            else:
                self.fchattrs_omegas['gradSchemes'] = new_attrs
        else:
            self.fchattrs_laminar['gradSchemes'] = new_attrs

    def update_div_dict_attrs(self, new_attrs, simulation_type, simulation_type_model):
        if simulation_type.strip().upper() == 'RAS':
            if simulation_type_model.strip() in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
                self.divSchemesAttrs_epsilon = new_attrs
            else:
                self.divSchemesAttrs_omegas = new_attrs
        else:
            self.divSchemesAttrs_laminar = new_attrs

    def update_laplacian_dict_attrs(self, new_attrs):
        self.laplacianSchemesAttrs = new_attrs

    def update_interpolation_dict_attrs(self, new_attrs):
        self.interpolationSchemesAttrs = new_attrs

    def update_sngrad_dict_attrs(self, new_attrs):
        self.snGradSchemesAttrs = new_attrs

    def update_walldist_dict_attrs(self, new_attrs):
        self.wallDistAttrs = new_attrs

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str:
                if v.find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table


"""Main function to call fvschemes base method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='turbulence_dict_base help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-ddt", "--ddtSchemes", dest="ddtSchemes", help="ddtSchemes")
    parser.add_argument("-grad", "--gradSchemes", dest="gradSchemes", help="gradSchemes")
    parser.add_argument("-div", "--divSchemes", dest="divSchemes", help="divSchemes")
    parser.add_argument("-lap", "--laplacianSchemes", dest="interpolationSchemes", help="interpolationSchemes")
    parser.add_argument("-int", "--interpolationSchemes", dest="interpolationSchemes", help="interpolationSchemes")
    parser.add_argument("-sng", "--snGradSchemes", dest="snGradSchemes", help="snGradSchemes")
    parser.add_argument("-wall", "--wallDist", dest="wallDist", help="wallDist")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    parser.add_argument("-st", "--simulationType", dest="simulationType", help="simulationType")
    parser.add_argument("-stm", "--simulationTypeModel", dest="simulationTypeModel", help="simulationTypeModel")
    args = parser.parse_args()
    fvSchemes = FvSchemesBase(args.ddtSchemes,
                              args.gradSchemes,
                              args.divSchemes,
                              args.laplacianSchemes,
                              args.interpolationSchemes,
                              args.snGradSchemes,
                              args.wallDist,
                              args.workdir,
                              args.case,
                              args.simulationType,
                              args.simulationTypeModel,
                              args.info)

    fvSchemes.write_fvschemes_dict_file()
