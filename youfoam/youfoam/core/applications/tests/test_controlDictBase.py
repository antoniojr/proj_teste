from unittest import TestCase
from src.base.controlDictBase import ControlDictBase
import os
import shutil


class TestControlDictBase(TestCase):
    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    WRITEINTERVAL = '50'

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASENAME
    source_dir = templates_path + '/' + CASENAME

    if os.path.exists(WORKDIR):
        shutil.rmtree(WORKDIR, ignore_errors=True)

    def test_write_dict_file_ok(self):
        print('*****************controlDict: test_write_dict_file_ok********************************** \n')
        shutil.copytree(self.source_dir, self.case_dir)
        control_dict = ControlDictBase(None, None, None, None, None, None, None, None,
                                       None, None, None, None, None, None, None, self.WORKDIR, self.CASENAME, self.INFO)
        control_dict.of_version = '4.0'
        r = control_dict.write_control_dict_file()
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('**************************************************************************** \n')

