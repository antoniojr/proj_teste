from django.apps import AppConfig


class SimplefoamConfig(AppConfig):
    name = 'simplefoam'
