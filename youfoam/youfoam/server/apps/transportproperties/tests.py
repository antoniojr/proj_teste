from django.test import TestCase, Client
from rest_framework import status
import shutil
import os

class TransportPropertiesTests(TestCase):
    def test_create_transportproperties(self):
        client = Client()
        workdir = 'workdir'
        casename = 'pitzDaily'
        file = 'rotatingCavity.cas'
        case_path = workdir + '/' + casename
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)
        data = {'workdir': workdir, 'casename': casename, 'createCase': ''}
        response = client.post('/api/createcase/create/', data)
        if response.status_code == status.HTTP_302_FOUND:
        	data['transportModel'] = 'Newtonian'
        	data['nu'] = '[0 2 -1 0 0 0 0] 1e-05'
        	response = client.post('/api/transportproperties/create/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)
