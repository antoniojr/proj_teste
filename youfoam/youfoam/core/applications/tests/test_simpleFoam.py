from unittest import TestCase
from applications.solvers.simpleFoam import SimpleFoam
import os
import shutil
from applications.utilities.generateMesh import generate_mesh
from applications.utilities.executeSolver import execute_solver
from src.base.boundaryBase import BoundaryBase


class TestSimpleFoam(TestCase):
    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    TIMEPRECISION = '6'
    LOG = '1'
    SOLVERNAME = 'simpleFoam'
    SIMULATIONTYPE = 'RAS'

    SIMULATIONTYPEATTRS = {'RASModel': 'kEpsilon',
                           'turbulence': 'off',
                           'printCoeffs': 'off'}

    PBOUNDARYFIELD = {'inlet':
                         {'type': 'zeroGradient'},
                      'outlet':
                         {'type': 'fixedValue', 'value': 'uniform 0'},
                      'upperWall':
                         {'type': 'zeroGradient'},
                      'lowerWall':
                         {'type': 'zeroGradient'},
                      'frontAndBack':
                         {'type': 'empty'}
                     }

    UBOUNDARYFIELD = {'inlet':
                         {'type': 'fixedValue', 'value': 'uniform (10 0 0)'},
                      'outlet':
                         {'type': 'zeroGradient'},
                      'upperWall':
                         {'type': 'zeroGradient'},
                      'lowerWall':
                         {'type': 'zeroGradient'},
                      'frontAndBack':
                         {'type': 'empty'}
                     }

    pattrs = {'file_name': 'p',
              'file_location': '0',
              'file_class': 'volScalarField',
              'dimensions': '[0 2 -2 0 0 0 0]',
              'internalField': 'uniform 0',
              'boundaryField': {'inlet':
                         {'type': 'zeroGradient'},
                      'outlet':
                         {'type': 'fixedValue', 'value': 'uniform 0'},
                      'upperWall':
                         {'type': 'zeroGradient'},
                      'lowerWall':
                         {'type': 'zeroGradient'},
                      'frontAndBack':
                         {'type': 'empty'}
                     }
              }

    uattrs = {'file_name': 'U',
              'file_location': '0',
              'file_class': 'volVectorField',
              'dimensions': '[0 1 -1 0 0 0 0]',
              'internalField': 'uniform (0 0 0)',
              'boundaryField': {'inlet':
                                    {'type': 'fixedValue', 'value': 'uniform (10 0 0)'},
                                'outlet':
                                    {'type': 'zeroGradient'},
                                'upperWall':
                                    {'type': 'zeroGradient'},
                                'lowerWall':
                                    {'type': 'zeroGradient'},
                                'frontAndBack':
                                    {'type': 'empty'}
                                }
              }

    kattrs = {'file_name': 'k',
              'file_location': '0',
              'file_class': 'volScalarField',
              'dimensions': '[0 2 -2 0 0 0 0]',
              'internalField': 'uniform 0.375',
              'boundaryField': {'inlet':
                                    {'type': 'fixedValue', 'value': 'uniform 0.375'},
                                'outlet':
                                    {'type': 'zeroGradient'},
                                'upperWall':
                                    {'type': 'kqRWallFunction', 'value': 'uniform 0.375'},
                                'lowerWall':
                                    {'type': 'kqRWallFunction', 'value': 'uniform 0.375'},
                                'frontAndBack':
                                    {'type': 'empty'}
                                }
              }

    nutattrs = {'file_name': 'nut',
                'file_location': '0',
                'file_class': 'volScalarField',
                'dimensions': '[0 2 -1 0 0 0 0]',
                'internalField': 'uniform 0',
                'boundaryField': {'inlet':
                                      {'type': 'calculated', 'value': 'uniform 0'},
                                  'outlet':
                                      {'type': 'calculated', 'value': 'uniform 0'},
                                  'upperWall':
                                      {'type': 'nutkWallFunction', 'value': 'uniform 0'},
                                  'lowerWall':
                                      {'type': 'nutkWallFunction', 'value': 'uniform 0'},
                                  'frontAndBack':
                                      {'type': 'empty'}
                                  }
                }

    epattrs = {'file_name': 'epsilon',
               'file_location': '0',
               'file_class': 'volScalarField',
               'dimensions': '[0 2 -3 0 0 0 0]',
               'internalField': 'uniform 14.855',
               'boundaryField': {'inlet':
                                     {'type': 'fixedValue', 'value': 'uniform 14.855'},
                                 'outlet':
                                     {'type': 'zeroGradient'},
                                 'upperWall':
                                     {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
                                 'lowerWall':
                                     {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
                                 'frontAndBack':
                                     {'type': 'empty'}
                                 }
               }

    fieldsAttr = {'p': pattrs,
                  'u': uattrs,
                  'k': kattrs,
                  'nut': nutattrs,
                  'epsilon': epattrs}

    case_dir = WORKDIR + '/' + CASENAME
    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']
    source_dir = templates_path + '/' + CASENAME

    def test_write_case_ok(self):
        print('*****************simpleFoam: test_write_dict_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        case = SimpleFoam(None, None, None, None, None, None,
                          None, None, None, None, None, None,
                          None, self.TIMEPRECISION, None,
                          None, None, self.SIMULATIONTYPE,
                          self.SIMULATIONTYPEATTRS, None, None,
                          None, None, None, None, None, None, None,
                          None, self.fieldsAttr,
                          self.WORKDIR, self.CASENAME, self.INFO)
        r = case.write_case()
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')

    def test_write_case_with_solver_exec_ok(self):
        print('*****************simpleFoam: test_write_case_with_solver_exec_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        case = SimpleFoam(None, None, None, None, None, None,
                          None, None, None, None, None, None,
                          None, self.TIMEPRECISION, None,
                          None, None, self.SIMULATIONTYPE,
                          self.SIMULATIONTYPEATTRS, None, None,
                          None, None, None, None, None, None, None,
                          None, self.fieldsAttr,
                          self.WORKDIR, self.CASENAME, self.INFO)
        r = case.write_case()
        if r['type'] == 1:
            shutil.copyfile(self.source_dir + '/system/blockMeshDict', self.case_dir + '/system/blockMeshDict')
            m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)
            if m['type'] == 1:
                print('executing solver!!!')
                e = execute_solver(self.WORKDIR, self.CASENAME, self.SOLVERNAME, self.LOG, self.INFO)
                self.assertEqual(e['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')

    def test_validate_field_patches(self):
        print('*****************simpleFoam: test_validate_field_patches_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)

        case = SimpleFoam(None, None, None, None, None, None,
                          None, None, None, None, None, None,
                          None, self.TIMEPRECISION, None,
                          None, None, self.SIMULATIONTYPE,
                          self.SIMULATIONTYPEATTRS, None, None,
                          None, None, None, None, None, None, None,
                          None, self.fieldsAttr,
                          self.WORKDIR, self.CASENAME, self.INFO)

        r = case.write_case()
        if r['type'] == 1:
            shutil.copyfile(self.source_dir + '/system/blockMeshDict', self.case_dir + '/system/blockMeshDict')
            m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)
            if m['type'] == 1:
                b = BoundaryBase(self.WORKDIR, self.CASENAME, self.INFO)
                b.read_boundary_file()
                r = case.validate_field_patches(b.get_patch_list())
                self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')
