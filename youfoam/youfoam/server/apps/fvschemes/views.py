import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from fvSchemesBase import FvSchemesBase
import shutil
import copy

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render, redirect

class FvSchemesCreateView(views.APIView):

	def get(self, request, workdir, casename, simulation_type, simulation_type_model):

		fvs = FvSchemesBase(None, 
							None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            workdir,
                            casename,
							simulation_type,
							simulation_type_model,
                            True)

		fvs_data = fvs.get_dict_attrs(simulation_type, simulation_type_model)

		fvs_data['workdir'] = workdir
		fvs_data['casename'] = casename
		fvs_data['simulationType'] = simulation_type
		fvs_data['simulationTypeModel'] = simulation_type_model

		return render(request, 'fvschemes.html', fvs_data)

	def post(self, request):

		#print(request.data)

		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		ddt = {'default': request.data.get('ddt')}
		grad = {'default': request.data.get('grad')}
		simulation_type = request.data.get('simulationType')
		simulation_type_model = request.data.get('simulationTypeModel')

		div = {}
		for k,v in request.data.items():
			if 'divSchemes' in k:
				div[k.replace('divSchemes', '')] = v

		laplacian = {'default': request.data.get('laplacian')}
		inter = {'default': request.data.get('inter')}
		sngrad = {'default': request.data.get('sngrad')}
		walld = {'method': request.data.get('wallDist')}

		fvs = FvSchemesBase(None, 
							None,
                            None,
                            None,
                            None,
                            None,
                            None,
                            workdir,
                            casename,
							simulation_type,
							simulation_type_model,
                            True)

		fchattrs = {'ddtSchemes': ddt,
                'gradSchemes': grad,
                'divSchemes': div,
                'laplacianSchemes': laplacian,
                'interpolationSchemes': inter,
                'snGradSchemes': sngrad,
                'wallDist': walld}

		fvs.updated_dict_attrs(fchattrs,simulation_type, simulation_type_model)

		'''fvs = FvSchemesBase(ddt, 
							grad,
                            div,
                            laplacian,
                            inter,
                            sngrad,
                            walld,
                            workdir,
                            casename,
                            True)

		print(fvs.get_dict_attrs(simulation_type, simulation_type_model))'''

		r = fvs.write_fvschemes_dict_file()

		'''fvs_data = fvs.get_dict_attrs(simulation_type, simulation_type_model)
		fvs_data['workdir'] = workdir
		fvs_data['casename'] = casename'''

		#return render(request, 'fvschemes.html', fvs_data)
		return redirect("/api/fvsolution/index/{}/{}/{}/{}/".format(workdir, casename, simulation_type, simulation_type_model))
		