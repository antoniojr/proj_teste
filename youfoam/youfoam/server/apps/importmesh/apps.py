from django.apps import AppConfig


class ImportmeshConfig(AppConfig):
    name = 'importmesh'
