#!/bin/sh
# $0 is the script name, $1 mesh converter function, $2 file_name, $3 case_dir

logFileName="$3""/""$1"Log

if [ $1 = 'fluentMeshToFoam' ]
then
    fluentMeshToFoam $2 -case $3 >> $logFileName 2>&1
else

    fluent3DMeshToFoam $2 -case $3 >> $logFileName 2>&1
fi