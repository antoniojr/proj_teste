"""
Documentation for p base class
To call help, pBase --help

Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 pBase.py -w workdir -c casename -i
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class PBase(CommonPropertiesBase):

    p_file_name = 'p'
    p_file_location = '0'
    p_file_class = 'volScalarField'

    dimensions = '[0 2 -2 0 0 0 0]'

    internalField = 'uniform 0'

    boundaryField = {'rotor':
                         {'type': 'zeroGradient'},
                     'stator':
                         {'type': 'zeroGradient'},
                     'front':
                         {'type': 'empty'},
                     'back':
                         {'type': 'empty'},
                     'AMI1':
                         {'type': 'cyclicAMI', 'value': '$internalField'},
                     'AMI2':
                         {'type': 'cyclicAMI', 'value': '$internalField'},
                     'inlet':
                         {'type': 'flowRateInletVelocity', 'massFlowRate': 'table (0 0),(1e-1 7.362)'}
                     }

    directive = '#includeEtc "caseDicts/setConstraintTypes"'

    pattrs = {'dimensions': dimensions,
              'internalField': internalField,
              'boundaryField': boundaryField}

    def __init__(self, dim, intf, bf, wd, c, info):
        self.pattrs['dimensions'] = dim if dim else self.pattrs['dimensions']
        self.pattrs['internalField'] = intf if intf else self.pattrs['internalField']
        self.pattrs['boundaryField'] = bf if bf else self.pattrs['boundaryField']

        self.work_dir = wd
        self.case_name = c
        self.info = True if info else False

    def write_p_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.p_file_location + '/' + self.p_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.p_file_class, self.p_file_location, self.p_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            self.write_dict(fo, self.pattrs, '')
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}

        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def get_dict_attrs(self):
        return self.pattrs

    def updated_dict_attrs(self, new_attrs):
        self.pattrs = new_attrs

    def update_boundary_fields_attrs(self, new_attrs):
        self.boundaryField = new_attrs
        self.pattrs['boundaryField'] = self.boundaryField

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str:
                if v.find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                if k == 'boundaryField':
                    fo.write(space + self.directive + '\n\n')
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table


"""Main function to call pBase method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='p_base help')
    parser.add_argument("-d", "--dimensions", dest="dimensions", help="dimensions")
    parser.add_argument("-f", "--internalField", dest="internalField", help="internalField")
    parser.add_argument("-b", "--boundaryField", dest="boundaryField", help="boundaryField")
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    p = PBase(args.dimensions, args.internalField, args.boundaryField, args.workdir, args.case, args.info)

    p.write_p_dict_file()
