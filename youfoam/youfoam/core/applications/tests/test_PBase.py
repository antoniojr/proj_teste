from unittest import TestCase
from src.base.pBase import PBase
from applications.utilities.generateMesh import generate_mesh
from applications.utilities.executeSolver import execute_solver
import os
import shutil


class TestPBase(TestCase):

    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    LOG = '1'
    SOLVERNAME = 'simpleFoam'

    BOUNDARYFIELD = {'inlet':
                         {'type': 'zeroGradient'},
                     'outlet':
                         {'type': 'fixedValue', 'value': 'uniform 0'},
                     'upperWall':
                         {'type': 'zeroGradient'},
                     'lowerWall':
                         {'type': 'zeroGradient'},
                     'frontAndBack':
                         {'type': 'empty'}
                     }

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASENAME
    source_dir = templates_path + '/' + CASENAME

    def test_write_dict_file(self):
        print('******************PBase: test_write_dict_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)

        p = PBase(None,
                  None,
                  None,
                  self.WORKDIR,
                  self.CASENAME,
                  self.INFO)

        r = p.write_p_dict_file()
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')

    def test_call_solver(self):
        print('*****************PBase: test_call_solver_ok**************************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)

        p = PBase(None,
                  None,
                  self.BOUNDARYFIELD,
                  self.WORKDIR,
                  self.CASENAME,
                  self.INFO)

        r = p.write_p_dict_file()

        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)

        if r['type'] == 1 and m['type'] == 1:
            print('executing solver!!!')
            r = execute_solver(self.WORKDIR, self.CASENAME, self.SOLVERNAME, self.LOG, self.INFO)
            self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('***************************************************************************** \n')
