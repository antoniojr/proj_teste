from unittest import TestCase
from applications.utilities.generateMesh import generate_mesh
from applications.utilities.checkMesh import check_mesh
import shutil
import os


class TestGenerate_mesh(TestCase):
    WORKDIR = 'run_cases'
    CASE = 'pitzDaily'
    CASE2 = 'case01'
    LOG = '1'
    INFO = True

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASE
    source_dir = templates_path + '/' + CASE

    if os.path.exists(WORKDIR):
        shutil.rmtree(WORKDIR, ignore_errors=True)

    def test_check_mesh_ok(self):
        print('********************test_check_mesh OK************************************\n')
        shutil.copytree(self.source_dir, self.case_dir)
        r = check_mesh(self.WORKDIR, self.CASE, self.LOG, self.INFO)
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('**************************************************************************')

    def test_block_mesh_ok(self):
        print('********************test_block_mesh OK************************************\n')
        shutil.copytree(self.source_dir, self.case_dir)
        r = generate_mesh(self.WORKDIR, self.CASE, self.LOG, self.INFO)
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('**************************************************************************')

