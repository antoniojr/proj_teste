import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from transportPropertiesBase import TransportPropertiesBase
import shutil
import copy

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render, redirect


class TransportPropertiesConfView(views.APIView):

	def get(self, request, workdir, casename, simulation_type, simulation_type_model):
		transport_properties_data = {}
		transport_properties_data['workdir'] = workdir
		transport_properties_data['casename'] = casename
		transport_properties_data['transportModel'] = 'Newtonian'
		transport_properties_data['simulationType'] = simulation_type
		transport_properties_data['simulationTypeModel'] = simulation_type_model

		return render(request, 'transportpropertiesconf.html', transport_properties_data)

	def post(self, request):
		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		transport_model = request.data.get('transportModel')
		simulation_type = request.data.get('simulationType')  #50Sp0 to be used by FvSchemes
		simulation_type_model = request.data.get('simulationTypeModel')  #50Sp0 to be used by FvSchemes

		tp_data = {}
		tp = TransportPropertiesBase(transport_model, None, workdir, casename, True)
		tp_data = copy.deepcopy(tp.get_dict_attrs())

		if transport_model == 'Newtonian':
			tp_data['nu'] = tp_data['nu [0 2 -1 0 0 0 0]']
		else:
			tp_data['nu0'] = tp_data[transport_model + 'Coeffs']['nu0 [0 2 -1 0 0 0 0]']
			tp_data['nuInf'] = tp_data[transport_model + 'Coeffs']['nuInf [0 2 -1 0 0 0 0]']
			tp_data['m'] = tp_data[transport_model + 'Coeffs']['m [0 0 1 0 0 0 0]']
			tp_data['n'] = tp_data[transport_model + 'Coeffs']['n [0 0 0 0 0 0 0]']

		tp_data['workdir'] = workdir
		tp_data['casename'] = casename
		tp_data['transportModel'] = transport_model
		tp_data['simulationType'] = simulation_type
		tp_data['simulationTypeModel'] = simulation_type_model

		return render(request, 'transportproperties.html', tp_data)


class TransportPropertiesCreateView(views.APIView):

	def post(self, request):

		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		transport_model = request.data.get('transportModel')
		simulation_type = request.data.get('simulationType')   #50Sp0 to be used by FvSchemes
		simulation_type_model = request.data.get('simulationTypeModel')   #50Sp0 to be used by FvSchemes


		if transport_model == 'Newtonian':
			coeffs = request.data.get('nu')
		else:
			coeffs = {}
			coeffs['nu0 [0 2 -1 0 0 0 0]'] = request.data.get('nu0')
			coeffs['nuInf [0 2 -1 0 0 0 0]'] = request.data.get('nuInf')
			coeffs['m [0 0 1 0 0 0 0]'] = request.data.get('m')
			coeffs['n [0 0 0 0 0 0 0]'] = request.data.get('n')
		
		tp_data = {}
		tp = TransportPropertiesBase(transport_model, coeffs, workdir, casename, True)
		tp_data = copy.deepcopy(tp.get_dict_attrs())

		tp.tpattrs = {}
		tp.tpattrs['transportModel'] = transport_model
		if transport_model == 'Newtonian':
			tp.tpattrs['nu [0 2 -1 0 0 0 0]'] = coeffs
		else:
			tp.tpattrs[transport_model + 'Coeffs'] = coeffs

		r = tp.write_transport_dict_file()

		if transport_model == 'Newtonian':
			tp_data['nu'] = tp_data['nu [0 2 -1 0 0 0 0]']
		else:
			tp_data['nu0'] = tp_data[transport_model + 'Coeffs']['nu0 [0 2 -1 0 0 0 0]']
			tp_data['nuInf'] = tp_data[transport_model + 'Coeffs']['nuInf [0 2 -1 0 0 0 0]']
			tp_data['m'] = tp_data[transport_model + 'Coeffs']['m [0 0 1 0 0 0 0]']
			tp_data['n'] = tp_data[transport_model + 'Coeffs']['n [0 0 0 0 0 0 0]']

		tp_data['workdir'] = workdir
		tp_data['casename'] = casename

		#return render(request, 'transportproperties.html', tp_data)
		# FIXME: Refazendo Workflow. return redirect("/api/turbulenceproperties/simtypeedit/{}/{}/".format(workdir, casename))
		return redirect("/api/fields/edit/{}/{}/{}/{}/".format(workdir, casename, simulation_type, simulation_type_model))
