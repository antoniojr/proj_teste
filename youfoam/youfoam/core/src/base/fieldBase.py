"""
Documentation for field base class
To call help, fieldBase --help

Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 fieldBase.py -w workdir -c casename -i
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class FieldBase(CommonPropertiesBase):

    field_file_name = 'epsilon'
    field_file_location = '0'
    field_file_class = 'volScalarField'

    dimensions = '[0 2 -3 0 0 0 0]'

    internalField = 'uniform 14.855'

    boundaryField = {'inlet':
                         {'type': 'fixedValue', 'value': 'uniform 14.855'},
                     'outlet':
                         {'type': 'zeroGradient'},
                     'upperWall':
                         {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
                     'lowerWall':
                         {'type': 'epsilonWallFunction', 'value': 'uniform 14.855'},
                     'frontAndBack':
                         {'type': 'empty'}
                     }

    directive = '#includeEtc "caseDicts/setConstraintTypes"'

    field_attrs = {'dimensions': dimensions,
                   'internalField': internalField,
                   'boundaryField': boundaryField}

    def __init__(self):
        pass

    def write_field_dict_file(self, name, location, dict_class, dim, intf, bf, wd, c, info):

        self.field_file_name = name if name else self.field_file_name
        self.field_file_location = location if location else self.field_file_location
        self.field_file_class = dict_class if dict_class else self.field_file_class

        self.field_attrs['dimensions'] = dim if dim else self.field_attrs['dimensions']
        self.field_attrs['internalField'] = intf if intf else self.field_attrs['internalField']
        self.field_attrs['boundaryField'] = bf if bf else self.field_attrs['boundaryField']

        self.work_dir = wd
        self.case_name = c
        self.info = True if info else False

        fout = self.work_dir + '/' + self.case_name + '/' + self.field_file_location + '/' + self.field_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.field_file_class, self.field_file_location, self.field_file_name)
                     + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            self.write_dict(fo, self.field_attrs, '')
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}

        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str:
                if v.find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                if k == 'boundaryField':
                    fo.write(space + self.directive + '\n\n')
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table


"""Main function to call fieldBase method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='field_base help')
    parser.add_argument("-d", "--dimensions", dest="dimensions", help="dimensions")
    parser.add_argument("-f", "--internalField", dest="internalField", help="internalField")
    parser.add_argument("-b", "--boundaryField", dest="boundaryField", help="boundaryField")
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    f = FieldBase()

    f.write_field_dict_file(args.fileName, args.fileLocation, args.fileClass, args.dimensions, args.internalField,
                            args.boundaryField, args.workdir, args.case, args.info)
