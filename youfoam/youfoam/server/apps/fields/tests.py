from django.test import TestCase, Client
from rest_framework import status
import shutil
import os

class FieldsTests(TestCase):
    def test_create_fields(self):
        client = Client()
        workdir = 'workdir'
        casename = 'pitzDaily'
        file = 'rotatingCavity.cas'
        case_path = workdir + '/' + casename
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)
        data = {'workdir': workdir, 'casename': casename, 'createCase': ''}
        response = client.post('/api/createcase/create/', data)
        if response.status_code == status.HTTP_302_FOUND:
        	data['simulationType'] = 'laminar'
        	data['simulationTypeModel'] = ''
        	data['simulationTypeModelList'] = 'p,U,k,nut,epsilon,R,f,v2,gammaInt,ReThetat'
        	data['boundary_names_list'] = 'bottom-4'
        	data['field_list'] = 'p'
        	data['p_dict_class'] = 'volScalarField'
        	data['p_internalField'] = 'uniform 0'
        	data['p__dimensions'] = '[0 2 -2 0 0 0 0]'
        	data['p_boundaryField_bottom-4_type'] = 'fixedValue'
        	data['p_boundaryField_bottom-4_value'] = 'uniform 0'
        	response = client.post('/api/fields/create/', data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)