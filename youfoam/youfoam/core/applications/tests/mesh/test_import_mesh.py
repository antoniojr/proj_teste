from unittest import TestCase
from applications.utilities.importMesh import import_mesh
import shutil
import os


class TestImport_mesh(TestCase):
    TYPE = 1
    FILE = 'rotatingCavity.cas'
    FILE2 = 'rotatingCavity.case'
    WORKDIR = 'run_cases'
    CASE = 'pitzDaily'
    CASE2= 'case01'
    INFO = True

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    source_dir = templates_path + '/' + CASE
    dest_dir = WORKDIR + '/' + CASE
    FILE = templates_path + '/mesh/' + FILE
    FILE2 = templates_path + '/mesh/' + FILE2

    if os.path.exists(WORKDIR):
        shutil.rmtree(WORKDIR, ignore_errors=True)

    def test_import_mesh_ok(self):
        print('*****************test_import_mesh OK************************\n')
        shutil.copytree(self.source_dir, self.dest_dir)
        r = import_mesh(self.TYPE, self.FILE, self.WORKDIR, self.CASE, self.INFO)
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('************************************************************')

    def test_import_mesh_wrong_folder(self):
        print('*****************test_import_mesh_wrong_folder************************\n')
        r = import_mesh(self.TYPE, self.FILE, self.WORKDIR, self.CASE2, self.INFO)
        self.assertNotEqual(r['type'], 1)
        print('**************************************************************************')

    def test_import_mesh_wrong_file(self):
        print('*****************test_import_mesh_wrong_file************************\n')
        r = import_mesh(self.TYPE, self.FILE2, self.WORKDIR, self.CASE, self.INFO)
        self.assertNotEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('************************************************************************')

