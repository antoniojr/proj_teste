from django.urls import path
from . import views

urlpatterns = [
    path('fvschemes/create/', views.FvSchemesCreateView.as_view()),
    path('fvschemes/edit/<workdir>/<casename>/<simulation_type>/<simulation_type_model>/', views.FvSchemesCreateView.as_view())
]