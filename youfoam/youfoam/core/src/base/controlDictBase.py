"""
Documentation for control dict base class
To call help, controlDictBase --help

Set info flag -i/--info for print output. You can check for error reasons

Return a value that can be:
         1: OK -> controlDict file created

         0: NOK -> controlDict creating error!!

To call from command line:
python controlDictBase.py -w workdir -c casename -app simpleFoam -i "creation success"
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class ControlDictBase(CommonPropertiesBase):

    cd_file_name = 'controlDict'
    cd_file_location = 'system'
    cd_file_class = 'dictionary'

    cattrs = {'application': 'simpleFoam',
              'startFrom': 'startTime',
              'startTime': '0',
              'stopAt': 'endTime',
              'endTime': '2000',
              'deltaT': '1',
              'writeControl': 'timeStep',
              'writeInterval': '100',
              'purgeWrite': '0',
              'writeFormat': 'ascii',
              'writeCompression': 'off',
              'writePrecision': '6',
              'timeFormat': 'general',
              'timePrecision': '6',
              'runTimeModifiable': 'true'}

    def __init__(self, app, sf, st, sa, et, dt, wc, wi, pw, wf, wcp, wp, tf, tp, rt, wd, c, info):
        self.cattrs['application'] = app if app else self.cattrs['application']
        self.cattrs['startFrom'] = sf if sf else self.cattrs['startFrom']
        self.cattrs['startTime'] = int(st) if st else self.cattrs['startTime']
        self.cattrs['stopAt'] = sa if sa else self.cattrs['stopAt']
        self.cattrs['endTime'] = int(et) if et else self.cattrs['endTime']
        self.cattrs['deltaT'] = int(dt) if dt else self.cattrs['deltaT']
        self.cattrs['writeControl'] = wc if wc else self.cattrs['writeControl']
        self.cattrs['writeInterval'] = int(wi) if wi else self.cattrs['writeInterval']
        self.cattrs['purgeWrite'] = int(pw) if pw else self.cattrs['purgeWrite']
        self.cattrs['writeFormat'] = wf if wf else self.cattrs['writeFormat']
        self.cattrs['writeCompression'] = wcp if wcp else self.cattrs['writeCompression']
        self.cattrs['writePrecision'] = int(wp) if wp else self.cattrs['writePrecision']
        self.cattrs['timeFormat'] = tf if tf else self.cattrs['timeFormat']
        self.cattrs['timePrecision'] = tp if tp else self.cattrs['timePrecision']
        self.cattrs['runTimeModifiable'] = rt if rt else self.cattrs['runTimeModifiable']
        self.work_dir = wd
        self.case_name = c
        self.info = True if info else False

    def write_control_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.cd_file_location + '/' + self.cd_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.cd_file_class, self.cd_file_location, self.cd_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            self.write_dict(fo, self.cattrs, '')
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}
        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def get_dict_attrs(self):
        return self.cattrs

    def updated_dict_attrs(self, new_attrs):
        self.cattrs = new_attrs

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str or type(v) == int or type(v) == float:
                if str(v).find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                # print('next_params: ', v)
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table

    def read_controldict_file(self):
        fin = self.work_dir + '/' + self.case_name + '/' + self.cd_file_location + '/' + self.cd_file_name
        return self.cattrs


"""Main function to call control_dict_base method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='control_dict_base help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-app", "--application", dest="application", help="application")
    parser.add_argument("-sf", "--startFrom", dest="startFrom", help="startFrom")
    parser.add_argument("-st", "--startTime", dest="startTime", help="startTime")
    parser.add_argument("-sa", "--stopAt", dest="stopAt", help="stopAt")
    parser.add_argument("-et", "--endTime", dest="endTime", help="endTime")
    parser.add_argument("-dt", "--deltaT", dest="deltaT", help="deltaT")
    parser.add_argument("-wc", "--writeControl", dest="writeControl", help="writeControl")
    parser.add_argument("-wi", "--writeInterval", dest="writeInterval", help="writeInterval")
    parser.add_argument("-pw", "--purgeWrite", dest="purgeWrite", help="purgeWrite")
    parser.add_argument("-wf", "--writeFormat", dest="writeFormat", help="writeFormat")
    parser.add_argument("-wcp", "--writeCompression", dest="writeCompression", help="writeCompression")
    parser.add_argument("-wp", "--writePrecision", dest="writePrecision", help="writePrecision")
    parser.add_argument("-tf", "--timeFormat", dest="timeFormat", help="timeFormat")
    parser.add_argument("-tp", "--timePrecision", dest="timePrecision", help="timePrecision")
    parser.add_argument("-rt", "--runTimeModifiable", dest="runTimeModifiable", help="runTimeModifiable")

    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    controlDict = ControlDictBase(args.application, args.startFrom, args.startTime, args.stopAt, args.endTime, args.deltaT,
                             args.writeControl, args.writeInterval, args.purgeWrite, args.writeFormat,
                             args.writeCompression, args.writePrecision, args.timeFormat, args.timePrecision,
                             args.runTimeModifiable, args.workdir, args.case, args.info)

    controlDict.write_control_dict_file()

