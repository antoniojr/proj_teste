"""
Documentation for select_solver function
To call help, selectSolver --help

Set info flag -i/--info for print output. You can check for error reasons

Return a solver name based on 2 arguments:
         FLOW_TYPE: 0 - Stationary
                    1 - Transient

         FLOW_REGIME: 0 - Incompressive
                      1 - Compressive

To call from command line:
python selectSolver.py -t 0 -r 1 return pimpleFoam
"""

import argparse

#solvers = ['simpleFoam', 'pimpleFoam', 'rhoSimpleFoam', 'rhoPimpleFoam']


def select_solver(flow_type, flow_regime, info, **kwargs):
    """Checking arguments"""
    if flow_type is None or flow_regime is None:
        if info:
            print('Missing arguments: flow_type and/or flow_regime')
        return {'type': 0, 'desc': 'Missing arguments: flow_type and/or flow_regime'}

    if flow_type not in [0, 1]:
        if info:
            print('Wrong flow_type argument type: expected number between 0 - 1 but receive ' + str(flow_type))
        return {'type': 0, 'desc': 'Wrong flow_type argument type: expected number between 0 - 1 but receive ' + str(flow_type)}

    if flow_regime not in [0, 1]:
        if info:
            print('Wrong flow_regime argument type: expected number between 0 - 1 but receive ' + str(flow_regime))
        return {'type': 0, 'desc': 'Wrong flow_regime argument type: expected number between 0 - 1 but receive ' + str(flow_regime)}

    solvers_list = ['simpleFoam', 'pimpleFoam', 'rhoSimpleFoam', 'rhoPimpleFoam']

    if flow_type == 0:
        to_delete_solvers = ['rhoSimpleFoam', 'rhoPimpleFoam']
        for delete_solver in to_delete_solvers:
            solvers_list.remove(delete_solver)
    else:
        to_delete_solvers = ['simpleFoam', 'pimpleFoam']
        for delete_solver in to_delete_solvers:
            solvers_list.remove(delete_solver)

    if flow_regime == 0:
        to_delete_solvers = 'pimple'
        for delete_solver in solvers_list:
            if to_delete_solvers in delete_solver.lower():
                solvers_list.remove(delete_solver)
    else:
        to_delete_solvers = 'simple'
        for delete_solver in solvers_list:
            if to_delete_solvers in delete_solver.lower():
                solvers_list.remove(delete_solver)

    if len(solvers_list) > 0:
        if info:
            print(solvers_list[0])
        return solvers_list[0]
    else:
        return {'type': 0, 'desc': 'None solver found for these arguments: '+ str(flow_type) + ' and ' + str(flow_regime)}


"""Main function to call select solver method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='select solver help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-t", "--type", dest="type", help="flow_type")
    parser.add_argument("-r", "--regime", dest="regime", help="flow_regime")
    args = parser.parse_args()
    select_solver(int(args.type), int(args.regime), args.info)