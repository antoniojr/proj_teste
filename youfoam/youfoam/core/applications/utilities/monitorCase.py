"""
Documentation for monitorCase function
To call help, monitorCase --help

Monitor log to check for inProgress, Error, Finished
Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 monitorCase.py -l logFileName -wtime 1 -i
"""

import os
import time
import argparse


def monitor_case(log_file, waiting_time, info):
    """Checking arguments"""
    if log_file is None:
        if info:
            print('Missing argument: log file argument not found')
        return {'type': 0, 'desc': 'Missing argument: log file argument not found'}

    if not isinstance(log_file, str):
        if info:
            print('Wrong type: log_file can only be a string')
        return {'type': 0, 'desc': 'Wrong type: log_file can only be a string'}

    try:
        """Check for Fatal Error!"""
        error_msg_list = ['FOAM FATAL ERROR', 'Foam::error']
        with open(log_file, 'r') as logF:
            for line in logF:
                # if 'FATAL ERROR' in line:
                if any(error in line for error in error_msg_list):
                    if info:
                        print('FATAL ERROR')
                    return {'type': 1, 'desc': 'FATAL ERROR'}

        """Check if process is Done!"""
        file1 = os.stat(log_file)
        file1_size = file1.st_size

        time.sleep(float(waiting_time))

        file2 = os.stat(log_file)
        file2_size = file2.st_size

        comp = file2_size - file1_size

        if comp == 0:
            if info:
                print('PROCESS FINISHED')
            return {'type': 1, 'desc': 'PROCESS FINISHED'}
        else:
            if info:
                print('IN PROCESS')
            return {'type': 1, 'desc': 'IN PROCESS'}

    except IOError as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))


"""Main function to call monitor_case"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='monitor case help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-l", "--log", dest="log", help="log file")
    parser.add_argument("-t", "--wtime", dest="wtime", help="waiting time")
    args = parser.parse_args()
    monitor_case(args.log, args.wtime, args.info)
