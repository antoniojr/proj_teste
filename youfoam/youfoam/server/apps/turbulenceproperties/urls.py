from django.urls import path
from . import views

urlpatterns = [
    path('turbulenceproperties/create/', views.TurbulencePropertiesCreateView.as_view()),
    path('turbulenceproperties/simtypeedit/<workdir>/<casename>/', views.TurbulencePropertiesConfView.as_view()),
    path('turbulenceproperties/coeffconfig/', views.TurbulencePropertiesConfView.as_view())
]