from unittest import TestCase
from applications.utilities.executeSolver import execute_solver
import shutil
import os


class TestExecute_solver(TestCase):
    WORKDIR = 'run_cases'
    CASE = 'pitzDaily'
    CASE2 = 'simpleCase'
    SOLVERNAME = 'simpleFoam'
    LOG = '1'
    INFO = True

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASE
    source_dir = templates_path + '/' + CASE

    if os.path.exists(WORKDIR):
        shutil.rmtree(WORKDIR, ignore_errors=True)

    def test_execute_solver_ok(self):
        print('********************test_execute_solver OK************************************\n')
        shutil.copytree(self.source_dir, self.case_dir)
        r = execute_solver(self.WORKDIR, self.CASE, self.SOLVERNAME, self.LOG, self.INFO)
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*****************************************************************************')

    def test_execute_solver_nok(self):
        print('********************test_execute_solver Wrong case folder************************************\n')
        shutil.copytree(self.source_dir, self.case_dir)
        r = execute_solver(self.WORKDIR, self.CASE2, self.SOLVERNAME, self.LOG, self.INFO)
        self.assertEqual(r['type'], 0)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*******************************************************************************')
