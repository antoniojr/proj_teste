from unittest import TestCase
from src.base.kBase import KBase
from applications.utilities.generateMesh import generate_mesh
from applications.utilities.executeSolver import execute_solver
import os
import shutil


class TestKBase(TestCase):

    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    LOG = '1'
    SOLVERNAME = 'simpleFoam'

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASENAME
    source_dir = templates_path + '/' + CASENAME

    def test_write_dict_file(self):
        print('******************KBase: test_write_dict_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)

        k = KBase(None,
                  None,
                  None,
                  self.WORKDIR,
                  self.CASENAME,
                  self.INFO)

        r = k.write_k_dict_file()
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')

    def test_call_solver(self):
        print('*****************KBase: test_call_solver_ok**************************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)

        k = KBase(None,
                  None,
                  None,
                  self.WORKDIR,
                  self.CASENAME,
                  self.INFO)

        r = k.write_k_dict_file()

        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)

        if r['type'] == 1 and m['type'] == 1:
            print('executing solver!!!')
            r = execute_solver(self.WORKDIR, self.CASENAME, self.SOLVERNAME, self.LOG, self.INFO)
            self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('***************************************************************************** \n')

