from django.apps import AppConfig


class TurbulencepropertiesConfig(AppConfig):
    name = 'turbulenceproperties'
