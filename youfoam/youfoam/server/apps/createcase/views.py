from django.shortcuts import render, redirect
from rest_framework import views
from django.urls import reverse
from django.http import HttpResponseRedirect

import sys
import os
import shutil

sys.path.append(os.environ['YouFOAM_APPLICATIONS_PATH'] + '/utilities')

from createCase import create_case

# Create your views here.

class CreateCaseCreateView():

	def index(request):
		return render(request, 'createcase.html', {'workdir': 'rodrigolp', 'casename': 'pitzDaily'})

	def create(request):
		workdir = request.POST['workdir']
		casename = request.POST['casename']
		case_dir = workdir + '/' + casename

		if 'createCase' in request.POST:
			case_folders = create_case(workdir, casename, True)
			return redirect("/api/importmesh/index/{}/{}/".format(workdir, casename))
		else:
			try:
				shutil.rmtree(case_dir)
				return render(request, 'createcase.html', {'workdir': workdir, 'casename': casename, 'status': 'Case Directory Deleted'})
			except shutil.Error as err:
				return render(request, 'createcase.html', {'workdir': workdir, 'casename': casename, 'status': err})	