from django.urls import path
from . import views

urlpatterns = [
    path('transportproperties/create/', views.TransportPropertiesCreateView.as_view()),
    path('transportproperties/modeledit/<workdir>/<casename>/<simulation_type>/<simulation_type_model>/', views.TransportPropertiesConfView.as_view()),
    path('transportproperties/coeffconfig/', views.TransportPropertiesConfView.as_view()),
    #path('transportproperties/index/<workdir>/<casename>/', views.TransportPropertiesCreateView.as_view())
]