"""
Documentation for create_case function
To call help, createCase --help

Create a folder structure with workdir and casename.
This function test if folder exists and create the folder.
Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python createCase.py -w /home/rodrigo/wordir -c case9
"""

import os
import argparse


def create_case(workdir, casename, info):
    """Checking arguments"""
    if workdir is None or casename is None:
        if info:
            print("missing working dir and/or case dir argument!!")
        return {'type': 0, 'desc': 'missing working dir and/or case dir argument!!'}

    """Test if its possible to create folder by workdir and casename args"""
    try:
        dir = workdir + '/' + casename
        """Check if folder already exists"""
        if not os.path.exists(dir):
            os.makedirs(dir)
            os.makedirs(dir + '/0')
            os.makedirs(dir + '/constant')
            os.makedirs(dir + '/system')
            """check for info flag"""
            if info:
                print('directory created: ' + dir)
            return {'type': 1, 'desc': 'OK'}
        else:
            if info:
                print('directory already exists: ' + dir)
            return {'type': 0, 'desc': 'directory already exists: ' + dir}

    except OSError as e:
        print('Error Creating directory')
        err = '{0}'.format(e)
        if info:
            print(err)
        return {'type': 0, 'desc': err}


"""Main function to call create_case method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='create case help')
    parser.add_argument("-i", "--info", dest="info",action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work folder")
    parser.add_argument("-c", "--casename", dest="casename", help="case folder")
    args = parser.parse_args()
    create_case(args.workdir, args.casename, args.info)
