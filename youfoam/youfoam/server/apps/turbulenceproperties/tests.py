from django.test import TestCase, Client
from rest_framework import status
import shutil
import os

class TurbulencePropertiesTests(TestCase):
    def test_create_turbulenceproperties(self):
        client = Client()
        workdir = 'workdir'
        casename = 'pitzDaily'
        file = 'rotatingCavity.cas'
        case_path = workdir + '/' + casename
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)
        data = {'workdir': workdir, 'casename': casename, 'createCase': ''}
        response = client.post('/api/createcase/create/', data)
        if response.status_code == status.HTTP_302_FOUND:
        	data['simulationType'] = 'laminar'
        	response = client.post('/api/turbulenceproperties/create/', data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)
