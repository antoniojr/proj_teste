# youFoam-web
Projeto de desenvolvimento de uma plataforma web para aplicacões de engenharia usando CFD.

### Descricao do projeto:
- Desenvolvimento de uma plataforma web para simplificar a criação/edição de casos do OpenFOAM.

### Referencias
- https://wikkibox.wikki.com.br/wikkiBox/index.php/f/51476

- https://wikkibox.wikki.com.br/wikkiBox/index.php/f/55618

- https://wikkibox.wikki.com.br/wikkiBox/index.php/f/55614

### Para configurar o projeto manualmente:
- Instalar o python3 e o pip3.
- Instalar o openfoam 4.x ou superior.
- Instalação do Doxygen para edição do manual do código.
- Instalação do pydot e graphviz.
- Instalação do virtualenv:
  - sudo pip3 install virtualenv
- Ativação do ambiente virtual:
  - virtualenv -p /url/bin/python3.6 venv (cria ambiente virtual venv)
  - source venv/bin/activate (ativa ambiente virtual)
    - obs: Para desativar ambiente basta executar o comando "deactivate".

- Instalar as dependencias do python, existentes no arquivo ~/youFoam-web/requirements.txt, através do comando
   - pip install -r requirements.txt
- Iniciar/parar/status do mongoDb: 
   - sudo systemctl start mongodb
   - sudo systemctl stop mongodb
   - sudo systemctl status mongodb
- Verificar versão instalada do mongodb:
   - sudo apt-cache policy mongodb   
- Inserir as variáveis de ambiente do projeto:

  - export YouFOAM_PATH="${HOME}/youFoam-web"
  - export YouFOAM_SOURCE_PATH="${YouFOAM_PATH}/src"
  - export YouFOAM_TEMPLATES_PATH="${YouFOAM_PATH}/templates"
  - export YouFOAM_ETC_PATH="${YouFOAM_PATH}/etc"
  - export YouFOAM_DOC_PATH="${YouFOAM_PATH}/doc"
  - export YouFOAM_BIN_PATH="${YouFOAM_PATH}/bin"
  - export YouFOAM_APPLICATIONS_PATH="${YouFOAM_PATH}/applications"
  - export YouFOAM_SOLVERS_PATH="${YouFOAM_APPLICATIONS_PATH}/solvers"
  - export YouFOAM_TESTS_PATH="${YouFOAM_APPLICATIONS_PATH}/tests"
  - export FLASK_APP=app.py
  - export FLASK_ENV=development
  - export FLASK_DEBUG=0

  
- **Atualização do path**
  - O path é obtido de forma automática


- **O usuário deve garantir que as variáveis de ambiente do OpenFoam estejam configuradas**  

<!--### Para compilacao/instalacao:
- instrucoes para compilacao/instalacao -->

### Para configurar o projeto por scripts:
- Editar e Executar o script etc/install.sh com sudo:
  - sudo sh bin/install.sh
    - Instala o python3 e o pip3;
    - Instala o doxygen;
    - Instala o pydot e graphviz;
    - Instala o virtualenv;
- Instalar as dependencias do python, existentes no arquivo ~/youFoam-web/requirements.txt, através do comando
   - pip install -r requirements.txt    
- Execute o comando $source etc/bashrc para atualizar o shell com as variáveis de ambiente do projeto.
- O openfoam continua a ter que ser instalado de forma manual.
  - Não esqueça de configurar as variaveis de ambiente do OpenFoam.

### Branchs do projeto:
- Master: Branch principal do Projeto.
- Development: **Branch com as features da Sprint para validação do cliente.**
- Release: Branch com a Release do projeto.
- Feature: Branch com uma feature da Sprint. Padrão "AnoMêsDia_IniciaisDoNome_NúmeroSprint_feature_Descrição"
  - Ex: 181018_rp_sp001_feature_develop_create_case
        181109_rp_sp002_fix_config_file_and_update_readme
  
<!--### Configuração pelo GitFlow:
- .... -->

### Teste de ambiente após configuração:
Execute o script runTests.sh
    - sh bin/runTests.sh  
para executar todos os testes unitários. 
Verifique se todos passaram:
  - Exemplo: 
  
    
    Ran 18 tests in 0.343s
    
    OK
    
    
 Acima vemos que 18 testes foram executados em ~300ms e todos passaram (OK).
 
 Para executar um unico modulo de testes
   - python3 -m unittest applications/tests/test_boundaryBase.py
   
 Para executar um único teste do módulo 
   - python3 -m unittest -v applications.tests.test_boundaryBase.TestBoundaryBase.test_update_patch_name

### Sugestões para extensão do código:
- texto

Antonio -- 18-10-2018
