from unittest import TestCase
from src.base.boundaryBase import BoundaryBase
import os
import shutil
from applications.utilities.generateMesh import generate_mesh
from applications.utilities.executeSolver import execute_solver


class TestBoundaryBase(TestCase):
    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    LOG = '1'
    SOLVERNAME = 'simpleFoam'
    PATCH_NEW_NAME = 'frontWall'
    PATCH_NAME = 'inlet'
    TYPE_NAME = 'patches'

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASENAME
    source_dir = templates_path + '/' + CASENAME

    # python3 -m unittest -v applications.tests.test_boundaryBase.TestBoundaryBase.test_read_boundary_file
    def test_read_boundary_file(self):
        print('******************test_read_boundary_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)
        if m['type'] == 1:
            boundary = BoundaryBase(self.WORKDIR, self.CASENAME, self.INFO)
            boundary.read_boundary_file()
            self.assertEqual(boundary.num_attrs, 5)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')

    # python3 -m unittest -v applications.tests.test_boundaryBase.TestBoundaryBase.test_write_dict_file
    def test_write_dict_file(self):
        print('******************test_write_boundary_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)
        if m['type'] == 1:
            boundary = BoundaryBase(self.WORKDIR, self.CASENAME, self.INFO)
            boundary.read_boundary_file()
            r = boundary.write_boundary_dict_file()
            if r['type'] == 1:
                print('executing solver!!!')
                r = execute_solver(self.WORKDIR, self.CASENAME, self.SOLVERNAME, self.LOG, self.INFO)
                self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('***************************************************************************** \n')

    # python3 -m unittest -v applications.tests.test_boundaryBase.TestBoundaryBase.test_update_patch_name
    def test_update_patch_name(self):
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)
        if m['type'] == 1:
            boundary = BoundaryBase(self.WORKDIR, self.CASENAME, self.INFO)
            boundary.read_boundary_file()
            patchs = boundary.patch_list
            boundary.update_patch_name(self.PATCH_NAME, self.PATCH_NEW_NAME)
            r = boundary.write_boundary_dict_file()
            if r['type'] == 1:
                boundary.read_boundary_file()
                self.assertEqual(boundary.patch_list, patchs)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)

    # python3 -m unittest -v applications.tests.test_boundaryBase.TestBoundaryBase.test_update_patch_type
    def test_update_patch_type(self):
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)
        if m['type'] == 1:
            boundary = BoundaryBase(self.WORKDIR, self.CASENAME, self.INFO)
            boundary.read_boundary_file()
            patch2change = 'upperWall'
            boundary.update_patch_attr(patch2change, 'type', self.PATCH_NEW_NAME)
            r = boundary.write_boundary_dict_file()
            if r['type'] == 1:
                boundary.read_boundary_file()
                attr = boundary.get_patch_attr(patch2change)
                self.assertEqual(attr['type'], self.PATCH_NEW_NAME)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
