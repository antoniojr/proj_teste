from unittest import TestCase
from src.base.fvSchemesBase import FvSchemesBase
from applications.utilities.generateMesh import generate_mesh
from applications.utilities.executeSolver import execute_solver
import os
import shutil


class TestFvSchemesBase(TestCase):

    WORKDIR = 'workdir'
    CASENAME = 'pitzDaily'
    INFO = True
    LOG = '1'
    SOLVERNAME = 'simpleFoam'
    DDTSCHEMES = 'Euler'
    SNGRADSCHEMES = 'corrected'
    DDTSCHEMES2 = 'steadyState'

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    case_dir = WORKDIR + '/' + CASENAME
    source_dir = templates_path + '/' + CASENAME

    def test_write_dict_file(self):
        print('******************fvSchemes: test_write_dict_file_ok********************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        ddtschemesAttrs = {'default': self.DDTSCHEMES}
        fvschemes = FvSchemesBase(ddtschemesAttrs,
                                  None,
                                  None,
                                  None,
                                  None,
                                  None,
                                  None,
                                  self.WORKDIR,
                                  self.CASENAME,
                                  self.INFO)
        r = fvschemes.write_fvschemes_dict_file()
        self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('*************************************************************************** \n')

    def test_call_solver_ok(self):
        print('*****************fvSchemes: test_call_solver_ok**************************************** \n')
        if os.path.exists(self.WORKDIR):
            shutil.rmtree(self.WORKDIR, ignore_errors=True)
        shutil.copytree(self.source_dir, self.case_dir)
        m = generate_mesh(self.WORKDIR, self.CASENAME, self.LOG, self.INFO)

        sngradschemesAttrs = {'default': self.SNGRADSCHEMES}
        fvschemes = FvSchemesBase(None,
                                  None,
                                  None,
                                  None,
                                  None,
                                  sngradschemesAttrs,
                                  None,
                                  self.WORKDIR,
                                  self.CASENAME,
                                  self.INFO)

        fv = fvschemes.write_fvschemes_dict_file()
        if fv['type'] == 1 and m['type'] == 1:
            print('executing solver!!!')
            r = execute_solver(self.WORKDIR, self.CASENAME, self.SOLVERNAME, self.LOG, self.INFO)
            self.assertEqual(r['type'], 1)
        shutil.rmtree(self.WORKDIR, ignore_errors=True)
        print('***************************************************************************** \n')
