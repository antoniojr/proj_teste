from django.urls import path
from . import views

urlpatterns = [
    path('createcase/index/', views.CreateCaseCreateView.index),
    path('createcase/create/', views.CreateCaseCreateView.create),
]