from django.test import TestCase, Client
from rest_framework import status
import shutil
import os

class SolverTests(TestCase):
    def test_execute_solver(self):
        client = Client()
        workdir = 'workdir'
        casename = 'pitzDaily'
        case_path = workdir + '/' + casename
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)
        templates_path = os.environ['YouFOAM_TEMPLATES_PATH']
        source_path = templates_path + '/' + casename
        shutil.copytree(source_path, case_path)
        data = {'workdir': workdir, 'casename': casename, 'solvername': 'simpleFoam'}
        
        response = client.post('/api/solver/execute/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        if os.path.exists(case_path):
            shutil.rmtree(case_path, ignore_errors=True)