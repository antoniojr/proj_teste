import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from fieldBase import FieldBase
from boundaryBase import BoundaryBase
from turbulencePropertiesBase import TurbulencePropertiesBase
import shutil
import copy

from rest_framework import views
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import render, redirect

class FieldsCreateView(views.APIView):

	def get(self, request, workdir, casename, simulation_type, simulation_type_model):

		fs_data = {}
		boundaries = {}
		vectorBoundaries = {}
		bou = BoundaryBase(workdir, casename, True)
		bou.read_boundary_file()
		boundary_names_list = bou.get_patch_list()
		#boundary_names_list_str = ','.join(boundary_names_list)

		patch_wall_boundaries = []
		for patch in boundary_names_list:
			bou_attrs = bou.get_patch_attr(patch)
			if bou_attrs['type'] in ['wall', 'patch']:
				boundaries[patch] = {'type': 'fixedValue', 'value': 'uniform 0'}
				vectorBoundaries[patch] = {'type': 'fixedValue', 'value': 'uniform (0 0 0)'}
				patch_wall_boundaries.append(patch)

		boundary_names_list_str = ','.join(patch_wall_boundaries)

		p_data = {}
		p_data['dict_class'] = 'volScalarField'
		p_data['dimensions'] = '[0 2 -2 0 0 0 0]'
		p_data['internalField'] = 'uniform 0'
		p_data['boundaryField'] = boundaries

		u_data = {}
		u_data['dict_class'] = 'volVectorField'
		u_data['dimensions'] = '[0 1 -1 0 0 0 0]'
		u_data['internalField'] = 'uniform (0 0 0)'
		u_data['boundaryField'] = vectorBoundaries

		field_list = ['p', 'U']

		fs_data['p'] = p_data
		fs_data['U'] = u_data

		if simulation_type == 'RAS':
			if simulation_type_model in ['kEpsilon', 'RNGkEpsilon', 'realizableKE', 'LaunderSharmaKE']:
				for f in ['k', 'nut', 'epsilon']:
					field_list.extend(['k', 'nut', 'epsilon'])
					field_data = {}
					field_data['dict_class'] = 'volScalarField'
					if f == 'k':
						field_data['dimensions'] = '[0 2 -2 0 0 0 0]'
					elif f == 'nut':
						field_data['dimensions'] = '[0 2 -1 0 0 0 0]'
					else:
						field_data['dimensions'] = '[0 2 -3 0 0 0 0]'
					field_data['internalField'] = 'uniform 0'
					field_data['boundaryField'] = boundaries
					fs_data[f] = field_data
			elif simulation_type_model in ['kOmega', 'kOmegaSST', 'kOmegaSSTSAS']:
				for f in ['k', 'nut', 'omega']:
					field_data = {}
					field_list.extend(['k', 'nut', 'omega'])
					field_data['dict_class'] = 'volScalarField'
					if f == 'k':
						field_data['dimensions'] = '[0 2 -2 0 0 0 0]'
					elif f == 'nut':
						field_data['dimensions'] = '[0 2 -1 0 0 0 0]'
					else:
						field_data['dimensions'] = '[0 0 -1 0 0 0 0]'
					field_data['internalField'] = 'uniform 0'
					field_data['boundaryField'] = boundaries
					fs_data[f] = field_data
			elif simulation_type_model in ['LRR', 'SSG']:
				for f in ['k', 'epsilon', 'R']:
					field_data = {}
					field_list.extend(['k', 'epsilon', 'R'])
					field_data['dict_class'] = 'volScalarField'
					if f == 'k':
						field_data['dimensions'] = '[0 2 -2 0 0 0 0]'
					elif f == 'epsilon':
						field_data['dimensions'] = '[0 2 -3 0 0 0 0]'
					else:
						field_data['dimensions'] = '[0 2 -2 0 0 0 0]'
					field_data['internalField'] = 'uniform 0'
					field_data['boundaryField'] = boundaries
					fs_data[f] = field_data

		fs_data['workdir'] = workdir
		fs_data['casename'] = casename
		fs_data['simulationType'] = simulation_type
		fs_data['simulationTypeModel'] = simulation_type_model

		fs_data['simulationTypeModelList'] = 'p,U,k,nut,epsilon,omega,R,f,v2,gammaInt,ReThetat'
		fs_data['boundary_names_list'] = boundary_names_list_str
		fs_data['field_list'] = ','.join(field_list)
		
		return render(request, 'fields.html', {'fields': fs_data})

	def post(self, request):
		
		workdir = request.data.get('workdir')
		casename = request.data.get('casename')
		simulation_type = request.data.get('simulationType')
		simulation_type_model = request.data.get('simulationTypeModel')
		simulation_type_model_list = request.data.get('simulationTypeModelList').split(',')
		boundary_names_list = request.data.get('boundary_names_list').split(',')
		field_list = request.data.get('field_list').split(',')

		fieldfile = FieldBase()
		for field in field_list:
			
			name = field
			location = '0'
			field_class = request.data.get(name + '_dict_class')
			dimensions = request.data.get(name + '_dimensions')
			intf = request.data.get(name + '_internalField')

			bf = {}
			for patch in boundary_names_list:
				patch_type = request.data.get(name + '_boundaryField_' + patch + '_type')
				patch_value = request.data.get(name + '_boundaryField_' + patch + '_value')
				bf[patch] = {'type': patch_type, 'value': patch_value}

			fieldfile.write_field_dict_file(name, location, field_class, dimensions, intf, bf, workdir, casename, True)

		#return redirect("/api/solver/edit/{}/{}/".format(workdir, casename))
		return redirect("/api/fvschemes/edit/{}/{}/{}/{}/".format(workdir, casename, simulation_type, simulation_type_model))
