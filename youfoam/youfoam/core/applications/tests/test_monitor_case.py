from unittest import TestCase
from applications.utilities.monitorCase import monitor_case
import os
# from executeSolver import execute_solver
# import shutil
# import os
# from multiprocessing import Process, Manager, Queue, Pool


class TestMonitor_case(TestCase):
    SOLVERNAME = 'simpleFoam'
    CASE = 'pitzDaily'
    WORKDIR = 'run_cases'
    LOG = '1'
    WAITING_TIME = 0.1
    INFO = True

    templates_path = os.environ['YouFOAM_TEMPLATES_PATH']

    LOG_FILE1 = templates_path + '/pitzDaily/errorLog'
    LOG_FILE3 = templates_path + '/pitzDaily/finishedLog'

    def test_monitor_case_foam_error(self):
        print('********************test_monitor_case foam_error************************************\n')
        r = monitor_case(self.LOG_FILE1, self.WAITING_TIME, self.INFO)
        self.assertEqual(r['desc'], 'FATAL ERROR')

    # def test_monitor_case_in_progress(self):
    #     case_dir = self.WORKDIR + '/' + self.CASE
    #     source_dir = '../templates/' + self.CASE
    #
    #     if not os.path.exists(self.WORKDIR):
    #         shutil.copytree(source_dir, case_dir)
    #
    #     p1 = Process(target=execute_solver, args=(self.WORKDIR, self.CASE, self.SOLVERNAME, self.LOG, self.INFO))
    #     p1.start()
    #
    #     log_file = case_dir + '/' + self.SOLVERNAME + 'Log'
    #     p2 = Process(target=monitor_case, args=(log_file, self.WAITING_TIME, self.INFO))
    #     p2.start()
    #
    #     p1.join()
    #     p2.join()

    #    self.assertEqual(p2, 'IN PROCESS')

    # def test_monitor_case_in_progress(self):
    #     # Call solver and check status with monitor case!!
    #
    #     case_dir = self.WORKDIR + '/' + self.CASE
    #     source_dir = '../templates/' + self.CASE
    #
    #     if not os.path.exists(self.WORKDIR):
    #         shutil.copytree(source_dir, case_dir)
    #
    #     execute_solver(self.WORKDIR, self.CASE, self.SOLVERNAME, self.LOG, self.INFO)
    #
    #     log_file = case_dir + '/' + self.SOLVERNAME + 'Log'
    #
    #     r = monitor_case(log_file, self.WAITING_TIME, self.INFO)
    #
    #     self.assertEqual(r['desc'], 'IN PROCESS')

    def test_monitor_case_finished(self):
        print('********************test_monitor_case_ finished process************************************\n')
        r = monitor_case(self.LOG_FILE3, self.WAITING_TIME, self.INFO)
        self.assertEqual(r['desc'], 'PROCESS FINISHED')
        print('***************************************************************************************')
