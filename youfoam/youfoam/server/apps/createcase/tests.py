from django.test import TestCase, Client
from rest_framework import status
import shutil
import os

class CreateCaseTests(TestCase):
    def test_create_case(self):
        client = Client()
        workdir = 'workdir'
        casename = 'pitzDaily'
        data = {'workdir': workdir, 'casename': casename, 'createCase': ''}
        case_path = workdir + '/' + casename
        if os.path.exists(case_path):
        	shutil.rmtree(case_path, ignore_errors=True)
        response = client.post('/api/createcase/create/', data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)