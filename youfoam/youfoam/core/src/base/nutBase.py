"""
Documentation for nut base class
To call help, nutBase --help

Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 nutBase.py -w workdir -c casename -i
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class NutBase(CommonPropertiesBase):

    nut_file_name = 'nut'
    nut_file_location = '0'
    nut_file_class = 'volScalarField'

    dimensions = '[0 2 -1 0 0 0 0]'

    internalField = 'uniform 0'

    boundaryField = {'inlet':
                         {'type': 'calculated', 'value': 'uniform 0'},
                     'outlet':
                         {'type': 'calculated', 'value': 'uniform 0'},
                     'upperWall':
                         {'type': 'nutkWallFunction', 'value': 'uniform 0'},
                     'lowerWall':
                         {'type': 'nutkWallFunction', 'value': 'uniform 0'},
                     'frontAndBack':
                         {'type': 'empty'}
                     }

    directive = '#includeEtc "caseDicts/setConstraintTypes"'

    nutattrs = {'dimensions': dimensions,
                'internalField': internalField,
                'boundaryField': boundaryField}

    def __init__(self, dim, intf, bf, wd, c, info):
        self.nutattrs['dimensions'] = dim if dim else self.nutattrs['dimensions']
        self.nutattrs['internalField'] = intf if intf else self.nutattrs['internalField']
        self.nutattrs['boundaryField'] = bf if bf else self.nutattrs['boundaryField']

        self.work_dir = wd
        self.case_name = c
        self.info = True if info else False

    def write_nut_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.nut_file_location + '/' + self.nut_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.nut_file_class, self.nut_file_location, self.nut_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            self.write_dict(fo, self.nutattrs, '')
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}

        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. error: %s' % e.output}

    def get_dict_attrs(self):
        return self.nutattrs

    def updated_dict_attrs(self, new_attrs):
        self.nutattrs = new_attrs

    def update_boundary_fields_attrs(self, new_attrs):
        self.boundaryField = new_attrs
        self.nutattrs['boundaryField'] = self.boundaryField

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str:
                if v.find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                if k == 'boundaryField':
                    fo.write(space + self.directive + '\n\n')
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table


"""Main function to call nutBase method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='nut_base help')
    parser.add_argument("-d", "--dimensions", dest="dimensions", help="dimensions")
    parser.add_argument("-f", "--internalField", dest="internalField", help="internalField")
    parser.add_argument("-b", "--boundaryField", dest="boundaryField", help="boundaryField")
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    n = NutBase(args.dimensions, args.internalField, args.boundaryField, args.workdir, args.case, args.info)

    n.write_nut_dict_file()
