"""
Documentation for importMesh function
To call help, importMesh --help

Convert Fluent Mesh file to Foam inside case folder.
Test if file and case folder exists
Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 importMesh.py -t <1 ou 0> -f file.cas -w workdir -c casedir
"""

import os
import argparse
import subprocess


def import_mesh(mesh_type, file_name, workdir, case_name, info):

    to_mesh_list = {'0': 'fluentMeshToFoam', '1': 'fluent3DMeshToFoam'}
    mesh_exts = ['.cas', '.msh']

    """Checking arguments"""
    if mesh_type is None or file_name is None or case_name is None:
        if info:
            print('Missing arguments: mesh_type or file_name or case_name not found')
        return {'type': 0, 'desc': 'Missing arguments: mesh_type or file_name or case_name not found'}

    if str(mesh_type) not in list(to_mesh_list):
        if info:
            print('Wrong type: mesh_type can be 0 (fluentMeshToFoam) or 1 (fluent3DMeshToFoam)')
        return {'type': 0, 'desc': 'Wrong type: mesh_type can be 0 (fluentMeshToFoam) or 1 (fluent3DMeshToFoam)'}

    if not isinstance(file_name, str):
        if info:
            print('Wrong type: file_name can only be a string')
        return {'type': 0, 'desc': 'Wrong type: file_name can only be a string'}

    if not isinstance(case_name, str):
        if info:
            print('Wrong type: case_name can only be a string')
        return {'type': 0, 'desc': 'Wrong type: case_name can only be a string'}

    if not os.path.isfile(file_name) and not file_name.lower().endswith(tuple(mesh_exts)):
        if info:
            print('Wrong file_name: this is not a valid file. Only accept .cas or .msh files')
        return {'type': 0, 'desc': 'Wrong file_name: this is not a valid file. Only accept .cas or .msh files'}

    case_dir = workdir + '/' + case_name

    if not os.path.exists(case_dir):
        if info:
            print('Wrong case directory: this folder did not exist')
        return {'type': 0, 'desc': 'Wrong case_directory: this folder did not exist'}

    try:
        to_mesh_func = to_mesh_list[str(mesh_type)]
        script_path = os.environ['YouFOAM_BIN_PATH']
        res = subprocess.call([script_path + '/call_mesh_converter.sh', to_mesh_func, file_name, case_dir])
        if res == 0:
            if info:
                print('mesh ' + file_name + ' converted')
            return {'type': 1, 'desc': 'mesh ' + file_name + ' converted'}
        else:
            if info:
                print('mesh converter error for file_name = ' + file_name + ' and case_dir = ' + case_dir)
            return {'type': 0, 'desc': 'mesh converter error for file_name = ' + file_name + ' and case_dir = ' + case_dir}
    except subprocess.CalledProcessError as e:
        if info:
            print('mesh not converted. Error: %s' % e.output)
        return {'type': 0, 'desc': 'mesh not converted. Error: %s' % e.output}


"""Main function to call import_mesh method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='import mesh help')
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-t", "--type", dest="type", help="mesh type")
    parser.add_argument("-f", "--file", dest="file", help="mesh file name")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    import_mesh(int(args.type), args.file, args.workdir, args.case, args.info)