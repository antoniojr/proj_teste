from unittest import TestCase
from applications.utilities.chooseSolverByDatabase import choose_solver_by_database, insert_new_solver


class TestChoose_solver_by_database(TestCase):
    FLOW_TYPE = 0
    FLOW_REGIME = 1
    INFO = True
    DB_NAME = 'foam_database'

    def test_choose_solver_by_database_ok(self):
        print('***************** test_choose_solver_by_database OK ************************ \n')
        insert_new_solver(self.DB_NAME, self.FLOW_TYPE, self.FLOW_REGIME, 'pimpleFoam')
        solver_name = choose_solver_by_database(self.DB_NAME, self.FLOW_TYPE, self.FLOW_REGIME, self.INFO)
        self.assertEqual(solver_name, 'pimpleFoam')
        print('*************************************************************************')
