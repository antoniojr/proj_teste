"""
Documentation for u base class
To call help, uBase --help

Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 uBase.py -w workdir -c casename -i
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class UBase(CommonPropertiesBase):

    u_file_name = 'U'
    u_file_location = '0'
    u_file_class = 'volVectorField'

    dimensions = '[0 1 -1 0 0 0 0]'

    internalField = 'uniform (0 0 0)'

    boundaryField = {'rotor':
                         {'type': 'movingWallVelocity', 'value': 'uniform (0 0 0)'},
                     'stator':
                         {'type': 'movingWallVelocity', 'value': 'uniform (0 0 0)'},
                     'inlet':
                         {'type': 'flowRateInletVelocity', 'massFlowRate': 'table (0 0),(1e-1 7.362)'}
                     }

    directive = '#includeEtc "caseDicts/setConstraintTypes"'

    uattrs = {'dimensions': dimensions,
              'internalField': internalField,
              'boundaryField': boundaryField}

    def __init__(self, dim, intf, bf, wd, c, info):
        self.uattrs['dimensions'] = dim if dim else self.uattrs['dimensions']
        self.uattrs['internalField'] = intf if intf else self.uattrs['internalField']
        self.uattrs['boundaryField'] = bf if bf else self.uattrs['boundaryField']

        self.work_dir = wd
        self.case_name = c
        self.info = True if info else False

    def write_u_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.u_file_location + '/' + self.u_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.u_file_class, self.u_file_location, self.u_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            self.write_dict(fo, self.uattrs, '')
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}

        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def get_dict_attrs(self):
        return self.uattrs

    def updated_dict_attrs(self, new_attrs):
        self.uattrs = new_attrs

    def update_boundary_fields_attrs(self, new_attrs):
        self.boundaryField = new_attrs
        self.uattrs['boundaryField'] = self.boundaryField

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str:
                if v.find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                if k == 'boundaryField':
                    fo.write(space + self.directive + '\n\n')
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table


"""Main function to call uBase method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='u_base help')
    parser.add_argument("-d", "--dimensions", dest="dimensions", help="dimensions")
    parser.add_argument("-f", "--internalField", dest="internalField", help="internalField")
    parser.add_argument("-b", "--boundaryField", dest="boundaryField", help="boundaryField")
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    u = UBase(args.dimensions, args.internalField, args.boundaryField, args.workdir, args.case, args.info)

    u.write_u_dict_file()
