from django.apps import AppConfig


class TransportpropertiesConfig(AppConfig):
    name = 'transportproperties'
