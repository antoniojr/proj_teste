from django.test import TestCase, Client
from rest_framework import status
import shutil
import os

class CreateBoundaryTests(TestCase):
	def test_create_boundary(self):
		client = Client()
		workdir = 'workdir'
		casename = 'pitzDaily'
		file = 'rotatingCavity.cas'
		case_path = workdir + '/' + casename
		if os.path.exists(case_path):
			shutil.rmtree(case_path, ignore_errors=True)
		data = {'workdir': workdir, 'casename': casename, 'createCase': ''}
		response = client.post('/api/createcase/create/', data)
		data['type'] = 1
		data['meshfile'] = file
		data['convertmesh'] = ''
		response = client.post('/api/importmesh/convert/', data)
		print(response.status_code)
		if response.status_code == status.HTTP_302_FOUND:
			print('polymesh created')
			response = client.post('/api/boundary/create/', data)
		self.assertEqual(response.status_code, status.HTTP_302_FOUND)
		if os.path.exists(case_path):
			shutil.rmtree(case_path, ignore_errors=True)