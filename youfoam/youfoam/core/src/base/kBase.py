"""
Documentation for k base class
To call help, kBase --help

Set info flag -i/--info for print output. You can check for error reasons

To call from command line:
python3 kBase.py -w workdir -c casename -i
"""

import argparse
import sys
import os
sys.path.append(os.environ['YouFOAM_SOURCE_PATH'] + '/base')
from commonPropertiesBase import CommonPropertiesBase


class KBase(CommonPropertiesBase):

    k_file_name = 'k'
    k_file_location = '0'
    k_file_class = 'volScalarField'

    dimensions = '[0 2 -2 0 0 0 0]'

    internalField = 'uniform 0.375'

    boundaryField = {'inlet':
                         {'type': 'fixedValue', 'value': 'uniform 0.375'},
                     'outlet':
                         {'type': 'zeroGradient'},
                     'upperWall':
                         {'type': 'kqRWallFunction', 'value': 'uniform 0.375'},
                     'lowerWall':
                         {'type': 'kqRWallFunction', 'value': 'uniform 0.375'},
                     'frontAndBack':
                         {'type': 'empty'}
                     }

    directive = '#includeEtc "caseDicts/setConstraintTypes"'

    kattrs = {'dimensions': dimensions,
              'internalField': internalField,
              'boundaryField': boundaryField}

    def __init__(self, dim, intf, bf, wd, c, info):
        self.kattrs['dimensions'] = dim if dim else self.kattrs['dimensions']
        self.kattrs['internalField'] = intf if intf else self.kattrs['internalField']
        self.kattrs['boundaryField'] = bf if bf else self.kattrs['boundaryField']

        self.work_dir = wd
        self.case_name = c
        self.info = True if info else False

    def write_k_dict_file(self):
        fout = self.work_dir + '/' + self.case_name + '/' + self.k_file_location + '/' + self.k_file_name

        try:
            fo = open(fout, "w")

            fo.write(self.file_header.format(self.of_version))
            fo.write(self.foam_file.format(self.file_class, self.k_file_location, self.k_file_name) + '\n')
            fo.write("// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //" + '\n\n')
            self.write_dict(fo, self.kattrs, '')
            fo.write("// ************************************************************************* //")

            fo.close()
            if self.info:
                print(fout + ' creation success')
            return {'type': 1, 'desc': fout + ' creation success'}

        except IOError as e:
            if self.info:
                print(fout + ' creation' + ' error. Error: %s' % e.output)
            return {'type': 0, 'desc': fout + ' creation' + ' error. Error: %s' % e.output}

    def get_dict_attrs(self):
        return self.kattrs

    def updated_dict_attrs(self, new_attrs):
        self.kattrs = new_attrs

    def update_boundary_fields_attrs(self, new_attrs):
        self.boundaryField = new_attrs
        self.kattrs['boundaryField'] = self.boundaryField

    def write_dict(self, fo, dict_attr, space):
        for k, v in dict_attr.items():
            if type(v) == str:
                if v.find('table') is not -1:
                    fo.write(space + str(k) + ' ' + 'table\n')
                    fo.write(self.write_table(v[6:], '    '))
                else:
                    fo.write(space + str(k) + ' ' + str(v) + ';' + '\n\n')
            else:
                fo.write(space + str(k) + '\n')
                fo.write(space + '{' + '\n')
                space += '    '
                if k == 'boundaryField':
                    fo.write(space + self.directive + '\n\n')
                self.write_dict(fo, v, space)
                space = space[4:]
                fo.write(space + '}' + '\n\n')

    def write_table(self, table, space):
        str_table = space + '(\n'
        splited_tbl = table.split(',')
        for row in splited_tbl:
            str_table = str_table + space + '    ' + row + '\n'

        str_table = str_table + space + ');\n'
        return str_table


"""Main function to call kBase method"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='k_base help')
    parser.add_argument("-d", "--dimensions", dest="dimensions", help="dimensions")
    parser.add_argument("-f", "--internalField", dest="internalField", help="internalField")
    parser.add_argument("-b", "--boundaryField", dest="boundaryField", help="boundaryField")
    parser.add_argument("-i", "--info", dest="info", action="store_true")
    parser.add_argument("-w", "--workdir", dest="workdir", help="work directory")
    parser.add_argument("-c", "--case", dest="case", help="case folder")
    args = parser.parse_args()
    k = KBase(args.dimensions, args.internalField, args.boundaryField, args.workdir, args.case, args.info)

    k.write_k_dict_file()
